<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersBikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_bikes', function (Blueprint $table) {
            $table->id();
            $table->integer('status');
            $table->integer('confirmed');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
            $table->integer('bikes_id')->unsigned();
            $table->foreign('bikes_id')->references('id')->on('bikes');
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->integer('privacy');
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_bikes');
    }
}
