<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bikes', function (Blueprint $table) {
            $table->dropColumn('component_front_derailleur');
            $table->dropColumn('component_rear_derailleur');
            $table->dropColumn('component_shifters');
            $table->dropColumn('component_system');
            $table->dropColumn('component_carriage');
            $table->dropColumn('component_connecting_rods');
            $table->dropColumn('component_cassette');
            $table->dropColumn('component_chain');
            $table->dropColumn('component_brakes');
            $table->dropColumn('component_wheel_brand');
            $table->dropColumn('component_tire_brand');
            $table->dropColumn('component_tire_size');
            $table->dropColumn('component_stem');
            $table->dropColumn('component_steering_wheel');
            $table->dropColumn('component_saddle');
            $table->dropColumn('component_seat_post');
            $table->dropColumn('component_pedals');
            $table->foreignId('equipment_group')->on('components')->nullable()->default(null)->after('size');
            $table->foreignId('pedals')->on('components')->nullable()->default(null)->after('equipment_group');
            $table->foreignId('carriage')->on('components')->nullable()->default(null)->after('pedals');
            $table->foreignId('cassette')->on('components')->nullable()->default(null)->after('carriage');
            $table->foreignId('planetary_hub')->on('components')->nullable()->default(null)->after('cassette');
            $table->foreignId('singspeed')->on('components')->nullable()->default(null)->after('planetary_hub');
            $table->foreignId('connecting_rods')->on('components')->nullable()->default(null)->after('singspeed');
            $table->foreignId('chainring')->on('components')->nullable()->default(null)->after('connecting_rods');
            $table->foreignId('chain')->on('components')->nullable()->default(null)->after('chainring');
            $table->foreignId('shifters')->on('components')->nullable()->default(null)->after('chain');
            $table->foreignId('front_switch')->on('components')->nullable()->default(null)->after('shifters');
            $table->foreignId('rear_switch')->on('components')->nullable()->default(null)->after('front_switch');
            $table->foreignId('roller_skates')->on('components')->nullable()->default(null)->after('rear_switch');
            $table->foreignId('switch_rollers')->on('components')->nullable()->default(null)->after('roller_skates');
            $table->foreignId('aerobar')->on('components')->nullable()->default(null)->after('switch_rollers');
            $table->foreignId('steering_wheel')->on('components')->nullable()->default(null)->after('aerobar');
            $table->foreignId('grips')->on('components')->nullable()->default(null)->after('steering_wheel');
            $table->foreignId('handlebar_winding')->on('components')->nullable()->default(null)->after('grips');
            $table->foreignId('steering_column')->on('components')->nullable()->default(null)->after('handlebar_winding');
            $table->foreignId('stem')->on('components')->nullable()->default(null)->after('steering_column');
            $table->foreignId('rigid_fork')->on('components')->nullable()->default(null)->after('stem');
            $table->foreignId('shock_absorber_26')->on('components')->nullable()->default(null)->after('rigid_fork');
            $table->foreignId('shock_absorber_27_5')->on('components')->nullable()->default(null)->after('shock_absorber_26');
            $table->foreignId('shock_absorber_29')->on('components')->nullable()->default(null)->after('shock_absorber_27_5');
            $table->foreignId('shock_absorber_700с')->on('components')->nullable()->default(null)->after('shock_absorber_29');
            $table->foreignId('spring')->on('components')->nullable()->default(null)->after('shock_absorber_700с');
            $table->foreignId('rear_shock_absorber')->on('components')->nullable()->default(null)->after('spring');
            $table->foreignId('rim_brakes')->on('components')->nullable()->default(null)->after('rear_shock_absorber');
            $table->foreignId('disc_brake')->on('components')->nullable()->default(null)->after('rim_brakes');
            $table->foreignId('brake_levers')->on('components')->nullable()->default(null)->after('disc_brake');
            $table->foreignId('saddle')->on('components')->nullable()->default(null)->after('brake_levers');
            $table->foreignId('seatpost')->on('components')->nullable()->default(null)->after('saddle');
            $table->foreignId('cushioned_seatpost')->on('components')->nullable()->default(null)->after('seatpost');
            $table->foreignId('telescopic_seatpost')->on('components')->nullable()->default(null)->after('cushioned_seatpost');
            $table->foreignId('wheels_26')->on('components')->nullable()->default(null)->after('telescopic_seatpost');
            $table->foreignId('wheels_27')->on('components')->nullable()->default(null)->after('wheels_26');
            $table->foreignId('wheels_29')->on('components')->nullable()->default(null)->after('wheels_27');
            $table->foreignId('wheels_700C')->on('components')->nullable()->default(null)->after('wheels_29');
            $table->foreignId('rims')->on('components')->nullable()->default(null)->after('wheels_700C');
            $table->foreignId('tires')->on('components')->nullable()->default(null)->after('rims');
            $table->foreignId('inner_tire')->on('components')->nullable()->default(null)->after('tires');
            $table->foreignId('sleeve')->on('components')->nullable()->default(null)->after('inner_tire');
            $table->foreignId('drum_bushing')->on('components')->nullable()->default(null)->after('sleeve');
            $table->foreignId('power_connecting_rods')->on('components')->nullable()->default(null)->after('drum_bushing');
            $table->foreignId('power_pedals')->on('components')->nullable()->default(null)->after('power_connecting_rods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
