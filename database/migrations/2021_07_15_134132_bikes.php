<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes', function (Blueprint $table) {
            $table->id();
            $table->string('serial');
            $table->integer('status')->default(0);
            $table->integer('confirmed')->default(0);
            $table->integer('bikes_categories_id')->nullable();
            $table->string('brand')->nullable();
            $table->string('model')->nullable();
            $table->string('year')->nullable();
            $table->string('color')->nullable();
            $table->string('material')->nullable();
            $table->string('size')->nullable();
            $table->string('component_front_derailleur')->nullable();
            $table->string('component_rear_derailleur')->nullable();
            $table->string('component_shifters')->nullable();
            $table->string('component_system')->nullable();
            $table->string('component_carriage')->nullable();
            $table->string('component_cassette')->nullable();
            $table->string('component_connecting_rods')->nullable();
            $table->string('component_chain')->nullable();
            $table->string('component_brakes')->nullable();
            $table->string('component_wheel_brand')->nullable();
            $table->string('component_tire_brand')->nullable();
            $table->string('component_tire_size')->nullable();
            $table->string('component_steering_wheel')->nullable();
            $table->string('component_stem')->nullable();
            $table->string('component_saddle')->nullable();
            $table->string('component_seat_post')->nullable();
            $table->string('component_pedals')->nullable();
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrent();
            $table->dateTime('deleted_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bikes');
    }
}
