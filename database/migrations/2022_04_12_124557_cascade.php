<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_bikes', function (Blueprint $table) {
            $table->foreign('bikes_id')->references('id')->on('bikes')->onDelete('cascade')->change();
        });
        Schema::table('bike_files', function (Blueprint $table) {
            $table->foreign('files_id')->references('id')->on('files')->onDelete('cascade')->change();
        });
        Schema::table('image_sizes', function (Blueprint $table) {
            $table->foreign('images_id')->references('id')->on('files')->onDelete('cascade')->change();
        });
        Schema::table('bookmarks', function (Blueprint $table) {
            $table->removeColumn('bikes_id');
            $table->foreignId('bikes_id')->on('bikes')->onDelete('cascade')->change();
        });
        Schema::table('users_bikes_likes', function (Blueprint $table) {
            $table->foreign('bikes_id')->references('id')->on('bikes')->onDelete('cascade')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
