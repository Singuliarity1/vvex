<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BikeFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bike_files', function (Blueprint $table) {
            $table->id();
            $table->integer('bikes_id')->unsigned();
            $table->foreign('bikes_id')->references('id')->on('bikes');
            $table->integer('files_id')->unsigned();
            $table->foreign('files_id')->references('id')->on('files');
            $table->integer("cover");
            $table->integer("order");
            $table->dateTime('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bike_files');
    }
}
