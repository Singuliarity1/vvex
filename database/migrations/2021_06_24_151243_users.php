<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('page_id')->nullable();
            $table->string('page_title')->nullable();
            $table->integer('status')->nullable();
            $table->string('email',100)->unique();
            $table->string('email_secondary')->nullable();
            $table->string('phone',15)->unique();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->integer('photo_id')->nullable();
            $table->integer('bg_id')->nullable();
            $table->string('password')->nullable();
            $table->string('reset_token')->nullable();
            $table->string('fb',100)->unique()->nullable();
            $table->string('google',100)->unique()->nullable();
            $table->string('vk',100)->unique()->nullable();
            $table->string('apple',100)->unique()->nullable();
            $table->string('about')->nullable();
            $table->string('strava_link')->nullable();
            $table->string('insta_link')->nullable();
            $table->string('fb_link')->nullable();
            $table->string('yt_link')->nullable();
            $table->string('tw_link')->nullable();
            $table->integer('agreement')->nullable();
            $table->dateTime('login_at')->useCurrent();
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrent();
            $table->dateTime('deleted_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
