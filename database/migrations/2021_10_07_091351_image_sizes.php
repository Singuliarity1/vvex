<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ImageSizes extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create( 'image_sizes', function ( Blueprint $table ) {
            $table->id();
            $table->integer( 'images_id' )->unsigned();
            $table->foreign( 'images_id' )->references( 'id' )->on( 'files' );
            $table->string( "path" );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }
}
