<?php


namespace App\Repositories;


use App\Entities\Files;
use Doctrine\ORM\EntityManager;

class FileRepository {
    /**
     * @var string
     */
    private $class = 'App\Entity\Files';
    /**
     * @var EntityManager
     */
    private $em;


    public function __construct( EntityManager $em ) {
        $this->em = $em;

    }


    public function create( Files $file ) {
        $this->em->persist( $file );
        $this->em->flush();
    }

    public function update( Files $file, $data ) {
        $file->setPath( $data['path'] );
        $file->setCreatedAt(new \DateTime('now') );
        $this->em->persist( $file );
        $this->em->flush();
    }

    public function PostOfId( $id ) {
        return $this->em->getRepository( $this->class )->findOneBy( [
            'id' => $id
        ] );
    }

    public function delete( Files $file ) {
        $this->em->remove( $file );
        $this->em->flush();
    }

    public function perpare_data( array $array ) {
        return new Files( $array );
    }

    /**
     * create Post
     * @return Files
     */
    private function prepareData( $data ) {
        return new Files( $data );
    }
}
