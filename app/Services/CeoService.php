<?php


namespace App\Services;


use App\Models\Ceo;

class CeoService {
    private $site_name;
    private $site_description;
    private $site_description_short;

    function __construct() {
        $ceo                          = Ceo::where( "id", ">", 0 )->first();
        $this->site_name              = $ceo["site_name"];
        $this->site_description       = $ceo["site_description"];
        $this->site_description_short = $ceo["site_description_short"];
    }

    private function getBikeMeta( $bike ) {
        $brand = $bike["bike"]["brand"]['brand_list'] ?? '';
        $img   = $bike["bike"]["bike_files"] ? ( new ImageService() )->getImageSize( $bike["bike"]["bike_files"][0]["file"],'full' ) : '';
        $title = "{$brand} " . ( $bike ? $bike["bike"]["model"] : '' );
        $desc  = "{$brand} " . ( $bike ? $bike["bike"]["model"] : '' ) . " - страница велосипеда.";

        return [
            "title"       => $title,
            "description" => $desc,
            "property"    => [
                "og:image"            => $img ?? '',
                "twitter:image"       => $img ?? '',
                "twitter:card"        => "summary_large_image",
                "twitter:description" => $desc,
                "twitter:title"       => $title,
                "og:image:secure_url" => $img ?? '',
                "og:type"             => "website",
                "og:title"            => $title,
                "og:site_name"        => "VELOVEX",
                "og:description"      => $desc,
                "og:locale"           => "ru_RU",
            ]
        ];
    }

    private function getUserMeta( $user ) {
        return [
            "title"       => "{$user["page_title"]} - {$this->site_name}",
            "description" => "{$user["page_title"]} - страница участника. {$this->site_name}"
        ];
    }

    private function getAbout() {
        return [
            "title"       => "{$this->site_name} - {$this->site_description_short}",
            "description" => "{$this->site_name} - это лучшая площадка для регистрации и поиска подробной информации о велосипедах."
        ];
    }

    private function getBenefits() {
        return [
            "title"       => "{$this->site_name} - проверка номера велосипеда по базе. Объявление и помощь в розыске велосипедов.",
            "description" => "{$this->site_name} - это лучшая площадка для регистрации и поиска подробной информации о велосипедах."
        ];
    }

    private function getTeam() {
        return [
            "title"       => "Команда VELOVEX",
            "description" => "{$this->site_name} - создан командой увлеченных и опытных единомышленников. Велосипеды часть нашей жизни."
        ];
    }

    private function getRegister() {
        return [
            "title"       => "Регистрация - {$this->site_name}",
            "description" => "{$this->site_name} - проверка номера велосипеда по базе. Объявление и помощь в розыске велосипедов."
        ];
    }

    private function getLogin() {
        return [
            "title"       => "Вход - {$this->site_name}",
            "description" => ""
        ];
    }

    private function getRecover() {
        return [
            "title"       => "Восстановление - {$this->site_name}",
            "description" => ""
        ];
    }

    private function getWanted() {
        return [
            "title"       => "Розыск велосипедов - {$this->site_name}",
            "description" => "{$this->site_name} - База велосипедов в розыске и проверка номеров на угон."
        ];
    }

    private function getWantedBike( $bike ) {
        $model = $bike["bike"]["model"] ?? null;
        $brand = $bike["bike"]["brand"]['brand_list'] ?? '';
        $img   = $bike["bike"]["bike_files"] ? ( new ImageService() )->getImageSize( $bike["bike"]["bike_files"][0]["file"],'full' ) : '';
        $title = "Розыскивается {$brand} {$model}";
        $desc  = "{$brand} {$model} - это велосипед в розыске. Помогите владельцу его найти!";

        return [
            "title"       => $title,
            "description" => $desc,
            "property"    => [
                "og:image"            => $img ?? '',
                "twitter:image"       => $img ?? '',
                "twitter:card"        => "summary_large_image",
                "twitter:description" => $desc,
                "twitter:title"       => $title,
                "og:image:secure_url" => $img ?? '',
                "og:type"             => "website",
                "og:title"            => $title,
                "og:site_name"        => "VELOVEX",
                "og:description"      => $desc,
                "og:locale"           => "ru_RU",
            ]
        ];
    }


    private function getCheck() {
        return [
            "title"       => "Проверка номера - {$this->site_name}",
            "description" => 'Хотите купить велосипед? Проверьте его номер по базе VELOVEX и узнать о нем'
        ];
    }


    private function getCheckNotFound() {
        return [
            "title"       => "Номер не найден - {$this->site_name}",
            "description" => ''
        ];
    }

    private function getCheckResult() {
        return [
            "title"       => "Номер найден - {$this->site_name}",
            "description" => ''
        ];
    }

    private function getTerms() {
        return [
            "title"       => 'Пользовательское соглашение',
            "description" => ''
        ];
    }

    public function showMeta( $type, $data ) {
        switch ( $type ) {
            case "login":
                return $this->getLogin();
            case "register":
                return $this->getRegister();
            case "team":
                return $this->getTeam();
            case "benefits":
                return $this->getBenefits();
            case "about":
                return $this->getAbout();
            case "user":
                return $this->getUserMeta( $data );
            case "bike":
                return $this->getBikeMeta( $data );
            case "recover":
                return $this->getRecover();
            case "wanted":
                return $this->getWanted();
            case "wanted_bike":
                return $this->getWantedBike( $data );
            case "check":
                return $this->getCheck();
            case "check_not_found":
                return $this->getCheckNotFound();
            case "check_result":
                return $this->getCheckResult();
            case "terms":
                return $this->getTerms();
            default:
                return [
                    "title"       => $this->site_name,
                    "description" => $this->site_description
                ];
        }
    }
}
