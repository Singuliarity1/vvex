<?php


namespace App\Services;


use App\Models\Components;
use Illuminate\Support\Facades\Log;

class BikeViewService {
    private $block_checks = [
        'group_set'       => [
            "equipment_group",
            "pedals",
            "carriage",
            "cassette",
            "planetary_hub",
            "singspeed",
            "connecting_rods",
            "chainring",
            "chain",
        ],
        'shifters_switch' => [
            'shifters',
            'front_switch',
            'rear_switch',
            'switch_rollers',
        ],
        'control'         => [
            'aerobar',
            'steering_wheel',
            'grips',
            'handlebar_winding',
            'steering_column',
            'stem',
        ],
        'absorber_fork'   => [
            'rigid_fork',
            'shock_absorber_26',
            'shock_absorber_27_5',
            'shock_absorber_29',
            'shock_absorber_700с',
            'rear_shock_absorber',
            'spring',
        ],
        'breakers'        => [
            'rim_brakes',
            'disc_brake',
            'brake_levers',
        ],
        'saddle_seatpost' => [
            'saddle',
            'seatpost',
            'cushioned_seatpost',
            'telescopic_seatpost',
        ],
        'wheels_tires'    => [
            'wheels_26',
            'wheels_27',
            'wheels_29',
            'wheels_700C',
            'rims',
            'tires',
            'inner_tire',
            'sleeve',
            'drum_bushing',
        ],
        'power'           => [
            'power_connecting_rods',
            'power_pedals',
        ]
    ];

    public function showComponent( $component ) {

        $part_component = explode( " ", $component );
        $html_component = "";
        foreach ( $part_component as $val ) {
            $html_component .= $val . " ";
        }

        return $html_component;
    }


    public function showSlide( $slides, $class,$size='big_slider' ) {
        $image_service = new ImageService();
        $html_slides   = "";
        foreach ( $slides as $val ) {
            if ( $val["file"] == null ) {
                continue;
            }
            $html_slides .= "<div class=\"swiper-slide $class\">";
            $html_slides .= "<div class=\"bike-swiper__img bike-thumb__img\"><span></span>";
            $html_slides .= "<img src=\"" . $image_service->getImageSize( $val["file"], $size ) . "\" alt=\"photo\">";
            $html_slides .= "</div>";
            $html_slides .= "</div>";
        }

        return $html_slides;
    }


    public function getFieldFromData( $data, $field ) {
        return $data[ $field ] != null ? $data[ $field ] : "";
    }

    public function allImages( $images = null ) {

        $images_html = "<input class=\"visually-hidden pick-image\" accept=\"image/*\" type=\"file\" name=\"files[]\">";
        $images_html .= $this->galleryConstructor( $images );

        return $images_html;
    }

    public function allMobileImages( $images = null ) {

        return $this->galleryConstructor( $images, "edit-form__gallery-mobile-img" );
    }

    private function galleryConstructor( $images = null, $class_item = "", $attrib = "" ) {
        $images_html = "";

        for ( $i = 0; $i < 10; $i ++ ) {
            $url         = $this->addImage( $images, $i, isset($images[ $i ]["file"])?($images[ $i ]["file"]["image_sizes"]["big_slider"] ?? $images[$i]["file"][ 'path' ]):'' );
            $class       = $this->addImage( $images, $i, "active" );
            $data        = $this->addImage( $images, $i, isset( $images[ $i ] ) ? $i : 11 );
            $class_fill  = $url ? "fill" : "";
            $images_html .= "<label class=\"{$class_item} {$class_fill} {$class} clearable\" data-key=\"{$data}\"><svg width='42' height='42' viewBox='0 0 42 42' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <circle cx='21' cy='21' r='20.5' fill='white' stroke='#B6BCC3'/>
                            <path d='M29.7812 13.6762L25.3408 10.1182C25.2829 10.0718 25.217 10.038 25.1468 10.0187C25.0766 9.99941 25.0035 9.99499 24.9317 10.0057C24.8598 10.0164 24.7907 10.042 24.7283 10.0811C24.6658 10.1202 24.6112 10.1719 24.5677 10.2334L13.7937 25.442C13.7364 25.5228 13.7 25.6179 13.6878 25.7182L13.0045 31.3411C12.9923 31.4412 13.0049 31.543 13.0408 31.6365C13.0768 31.73 13.1351 31.812 13.2098 31.8745C13.2846 31.937 13.3734 31.9779 13.4675 31.9932C13.5617 32.0086 13.658 31.9977 13.747 31.9619L18.8707 29.897C18.9681 29.8577 19.053 29.79 19.1155 29.7017L29.8896 14.4931C29.9772 14.3695 30.0149 14.214 29.9946 14.0609C29.9743 13.9078 29.8976 13.7694 29.7812 13.6762ZM14.6599 26.866L17.5479 29.1801L14.2155 30.523L14.6599 26.866ZM18.5663 28.5314L15.005 25.6778L22.9816 14.4182L26.5427 17.2716L18.5663 28.5314ZM27.2047 16.3372L23.6434 13.4838L25.117 11.4037L28.6781 14.2571L27.2047 16.3372Z' fill='black'/>
                            </svg>
                            ";
            $images_html .= "<div class=\"edit-form__gallery-btn-delete\"></div>";
            $images_html .= "<span class=\"{$class}\" src=\"{$url}\" style=\"background-image:url({$url})\" data-key=\"{$data}\"></span>";
            $images_html .= "<input class=\"visually-hidden pick-image-hide\"  {$attrib} accept=\"image/*\" data-key=\"{$data}\" type=\"file\">";
            $images_html .= "<input type='hidden' class='db_orders' name='db_orders[]' value=\"{$i}\">";
            $images_html .= "<div class=\"edit-form__gallery-btn-update\"></div>";
            $images_html .= "</label>";
        }
        $images_html .= "<span class='last_item'></span>";

        return $images_html;
    }


    public function getTitleColors( $edit_colors ) {
        return explode( "-", $edit_colors );
    }

    public function allColors( $edit_colors = null ) {
        $edit_colors     = $this->getTitleColors( $edit_colors );
        $colors          = [
            [
                "color" => "white",
                "check" => "dark"
            ],
            [
                "color" => "grey",
                "check" => "dark"
            ],
            [
                "color" => "satin_sheen_gold",
                "check" => "dark"
            ],
            [
                "color" => "yellow",
                "check" => "dark"
            ],
            [
                "color" => "orange",
                "check" => "dark"
            ],
            [
                "color" => "lightcoral",
                "check" => "dark"
            ],
            [
                "color" => "pink",
                "check" => "dark"
            ],
            [
                "color" => "red",
                "check" => "white"
            ],
            [
                "color" => "magenta",
                "check" => "white"
            ],
            [
                "color" => "brown",
                "check" => "white"
            ],
            [
                "color" => "dodgerblue",
                "check" => "white"
            ],
            [
                "color" => "blue",
                "check" => "white"
            ],
            [
                "color" => "purple",
                "check" => "white"
            ],
            [
                "color" => "green",
                "check" => "white"
            ],
            [
                "color" => "darkgrey",
                "check" => "white"
            ],
            [
                "color" => "black",
                "check" => "white"
            ],
            [
                "color" => "several",
                "check" => "white"
            ],

        ];
        $all_colors      = "";
        $colors_selected = $edit_colors != null ? $edit_colors : null;
        foreach ( $colors as $key => $color ) {
            $selected   = $colors_selected != null && in_array( $color["color"], $colors_selected ) ? "checked" : "";
            $all_colors .= "<label>";
            $all_colors .= "<input type=\"checkbox\" class=\"color-gradient {$color["color"]}\" name=\"color[]\" value=\"{$color["color"]}\" {$selected}>";
            $all_colors .= "<span class=\"edit-form__color-box color-" . ( $key + 1 ) . " {$color["check"]}-check\"></span>";
            $all_colors .= "<span class=\"edit-form__color-border\"></span>";
            $all_colors .= "</label>";
        }

        return $all_colors;
    }

    private function addImage( $images, $i, $result ) {
        return $images != null && isset( $images[ $i ] ) && $images[ $i ]["file"] != null ? $result : "";
    }


    public function findBrand( $type, $title ) {

        $component = Components::where( 'type', $type )->where( 'model_list', $title )->first();

        return $component == null ? $title : $component->getAttributes()["brand_list"];
    }

    public function isEmpty( $bike, $block ) {

        foreach ( $this->block_checks[ $block ] as $block_item ) {
            if ( $bike[ $block_item ] != null ) {
                return false;
            };
        }

        return true;
    }
    public function isShowComponent( $bike ) {
        foreach ( $this->block_checks as $block ) {
            foreach ( $block as $block_item ) {
                if ( $bike[ $block_item ] != null ) {
                    return true;
                }
            }
        }

        return false;
    }
    public function isPro( $bike ) {

//        foreach ( $this->block_checks as $block ) {
//            $check = false;
//            foreach ( $block as $block_item ) {
//                if ( $bike[ $block_item ] != null ) {
//                    $check = true;
//                    break;
//                };
//            }
//
//            if ( ! $check ) {
//                return false;
//            }
//        }
//
//        return true;
    return $this->isShowComponent($bike);
    }

    public function sortArray(&$array){
        usort($array, function ($item1, $item2) {
            return $item1['items'] <=> $item2['items'];
        });
        return $array;

    }


}
