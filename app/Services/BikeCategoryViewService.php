<?php


namespace App\Services;


use App\Http\Resources\BikeCategory\BikeCategoryResource;
use App\Models\BikeCategory;

class BikeCategoryViewService {
    private $images;

    public function __construct() {
        $this->images = [
            "Шоссейный"    => asset( "layouts/build/img/pictures/road_bike.svg" ),
            "Трековый"     => asset( "layouts/build/img/pictures/track_bike2.svg" ),
            "Триатлон"     => asset( "layouts/build/img/pictures/triathlon_bike.svg" ),
            "Гравийный"    => asset( "layouts/build/img/pictures/gravel_bike.svg" ),
            "Складной"     => asset( "layouts/build/img/pictures/folding_bike.svg" ),
            "Фикс"         => asset( "layouts/build/img/pictures/fix_bike.svg" ),
            "Туринговый"   => asset( "layouts/build/img/pictures/touring_bike.svg" ),
            "Городской"    => asset( "layouts/build/img/pictures/city_bike.svg" ),
            "Тандем"       => asset( "layouts/build/img/pictures/bike-tandem.svg" ),
            "Кастомный"    => asset( "layouts/build/img/pictures/custom_bike.svg" ),
            "Ретро"        => asset( "layouts/build/img/pictures/retro_bike.svg" ),
            "Кросс-кантри" => asset( "layouts/build/img/pictures/cross_country_bike.svg" ),
            "Даунхилл"     => asset( "layouts/build/img/pictures/downhill_bike.svg" ),
            "Фетбайк"      => asset( "layouts/build/img/pictures/fat_bike.svg" ),
            "Трейловый"    => asset( "layouts/build/img/pictures/trail_bike.svg" ),
            "Стрит/дерт"    => asset( "layouts/build/img/pictures/dirtstreet_bike.svg" ),
            "BMX гоночный" => asset( "layouts/build/img/pictures/bmx_race.svg" ),
            "BMX фристайл" => asset( "layouts/build/img/pictures/bmx_freestyle.svg" ),
            "Электро"      => asset( "layouts/build/img/pictures/e-bike.svg" )
        ];
    }


    public function allCategories( $category_id = null ) {
        $categories = BikeCategoryResource::collection( BikeCategory::orderBy( 'order' )->get() )->toArray( null );

        $bikes_catgory_img = [
            [
                "count"  => 11,
                "images" => array_slice( $this->images, 0, 11 )
            ],
            [
                "count"  => 5,
                "images" => array_slice( $this->images, 11, 5 )
            ],
            [
                "count"  => 2,
                "images" => array_slice( $this->images, 16, 2 )
            ],
            [
                "count"  => 1,
                "images" => array_slice( $this->images, 18, 1 )
            ]
        ];

        $images_html  = "";
        $key_fieldset = 1;
        $key          = 1;
        foreach ( $categories as $category ) {
            if ( $key == 1 ) {
                $images_html .= '<div class="page-category__form-fieldset page-category__form-fieldset-' . $key_fieldset . '">';
            }
            $images_html .= ' <label>
                            <input type="radio" name="bikes_categories_id" value="' . $category["id"] . '"  ' .
                            ( $category_id == null ? '' : ( $category_id == $category["id"] ? "checked" : ' ' ) ) .
                            '/>
                            <div class="page-category__item">
                                <div class="page-category__item-img">
                                <img src="'. $this->images[ $category["title"]].'" alt="Шоссейный">
                                </div>
                                <span class="page-category__item-name">' . $category["title"] . '</span>
                                <span class="page-category__item-check"></span>
                            </div>
                        </label>';
            if ( $key == $bikes_catgory_img[ $key_fieldset - 1 ]["count"] ) {
                $images_html .= "</div>";
                $key_fieldset ++;
                $key = 1;
            } else {
                $key ++;
            }

        }

        return $images_html;
    }

    public function getCategory( $category = null ) {

        $model_data = $category != null ? BikeCategory::find( $category["id"] ) : BikeCategory::where( "order", 1 )->first();

        $category = ( new BikeCategoryResource( $model_data ) )->toArray();

        return '<div class="edit-menu__category">
                <div class="edit-menu__category-img">
                    <img src="' . $this->images[$category["title"] ] . '" alt="' . $category["title"] . '">
                </div>
                <span class="edit-menu__category-btn">' . $category["title"] . '</span>
            </div>';
    }
}
