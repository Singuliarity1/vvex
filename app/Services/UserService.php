<?php


namespace App\Services;


use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class UserService {

    public function isLogged() {
        return Auth::check() ? "isLogin" : "";
    }


    public function userImg( $id = null, $type = "photo", $size = "medium" ) {
        $image_service = new ImageService();
        $userResource  = ( new UserResource( User::find( $id != null ? $id : ( Auth::check() ? auth()->user()->id : $id ) ) ) )->toArray();
        if ( $userResource != [] ) {
            return isset( $userResource[ $type ] ) ? $image_service->getImageSize( $userResource[ $type ], $size ) : asset( "/layouts/build/img/pictures/" . ( $type == "bg" ? "profile-bg-new.png" : "def_theme.svg" ) );
        }

        return null;
    }

    public function getOriginalPass() {
        try {
            return decrypt( Auth::user()->getAuthPassword() );
        } catch ( \Exception $e ) {
            return "********";
        }
    }

    public function socialButton( $user ) {
        $socialField = [
            "strava_link" => [
                "alt" => "V",
                "src" => asset( "layouts/build/img/icons/profile-social-1.svg" )
            ],
            "fb_link"     => [
                "alt" => "facebook",
                "src" => asset( "layouts/build/img/icons/profile-social-2.svg" )
            ],
            "vk_link"     => [
                "alt" => "vk",
                "src" => asset( "layouts/build/img/icons/profile-social-3.svg" )
            ],
            "tw_link"     => [
                "alt" => "twitter",
                "src" => asset( "layouts/build/img/icons/profile-social-4.svg" )
            ],
            "insta_link"  => [
                "alt" => "instagram",
                "src" => asset( "layouts/build/img/icons/profile-social-5.svg" )
            ],
            "yt_link"     => [
                "alt" => "youtube",
                "src" => asset( "layouts/build/img/icons/profile-social-6.svg" )
            ],
        ];
        $html        = "";

        foreach ( $socialField as $key => $social ) {
            if ( $user[ $key ] != null ) {
                $html .= "<a href='{$user[$key]}' target='_blank' class='page-profile__social-item'>
                        <img src='{$social["src"]}' alt='{$social["alt"]}'>
                    </a>";
            }

        }

        return $html;
    }

    public function getAllUsersBikesAndCategories( $user ) {
        $all_categories = [];
        $all_bikes      = [];
        foreach ( $user["bikes"] as $bike ) {
            if ( ! in_array( $bike["bike"]["bike_category"]["title"], $all_categories ) ) {
                $all_categories[] = $bike["bike"]["bike_category"]["title"];
            }
            if ( ! in_array( ($bike["bike"]["brand"]['brand_list']??'') . " " . $bike["bike"]["model"], $all_categories ) ) {
                $all_bikes[] = ($bike["bike"]["brand"]['brand_list']??'') . " " . $bike["bike"]["model"];
            }
        }

        return [ implode( " ,", $all_bikes ), implode( " ,", $all_categories ) ];
    }

    public function getSocialAuthNow( $type ) {

        return session( 'social_name', 'none' ) == $type;

    }

}
