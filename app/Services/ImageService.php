<?php


namespace App\Services;


class ImageService {

    public function getImageSize( $image_part, $size = "medium" ) {
        return $image_part!=null?($image_part["image_sizes"]!=null?( $image_part["image_sizes"][ $size ] ?? $image_part["path"] ):$image_part["path"]):null;
    }

}
