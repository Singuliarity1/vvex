<?php


namespace App\Handlers;


class ImageHandler {
    public static function resize( $image_path, $size = "medium" ) {
        $pixel_size = null;
        switch ( $size ) {
            case "small":
                $pixel_size = [ 250, 250 ];
                break;
            case "small_slider":
                $pixel_size = [ 115, 115 ];
                break;
            case "medium":
                $pixel_size = [ 500, 500 ];
                break;
            case "cover":
                $pixel_size = [ 711, 124 ];
                break;
            case "big_slider":
                $pixel_size = [ 6000, 6000 ];
                break;
        }
        return self::cropImage( $image_path, $pixel_size );

    }

    private static function cropImage( $image_path, $size = null ) {
        try {
            list( $imageWidth, $imageHeight ) = getimagesize( $image_path );
            $pixel_size = getimagesize( $image_path );
            $centreX    = round( $pixel_size[0] / 2 );
            $centreY    = round( $pixel_size[1] / 2 );

            $new_size = $size ?? $pixel_size;
            list( $width, $height ) = $new_size;
            if ( $imageWidth < $width ) {
                $width = $imageWidth;
            }
            if ( $imageHeight < $height ) {
                $height = $imageHeight;
            }

            $cropWidthHalf  = round( $width / 2 );
            $cropHeightHalf = round( $height / 2 );

            $x1 = max( 0, $centreX - $cropWidthHalf );
            $y1 = max( 0, $centreY - $cropHeightHalf );

            $x2 = min( $width, $centreX + $cropWidthHalf );
            $y2 = min( $height, $centreY + $cropHeightHalf );


            $ext             = pathinfo( $image_path )["extension"];
            $func_end        = ( $ext == "jpg" ? "jpeg" : $ext );
            if($func_end==='jpeg' && $func_end==='png') {
                $func_image_work = "imagecreatefrom" . $func_end;
                $source          = $func_image_work( $image_path );
                $image_cropped   = imagecrop( $source, [ 'x' => $x1, 'y' => $y1, 'width' => $x2, 'height' => $y2 ] );

                $exif = is_callable( "exif_read_data" ) ? exif_read_data( $image_path ) : [];
                if ( ! empty( $exif['Orientation'] ) ) {
                    switch ( $exif['Orientation'] ) {
                        case 3:
                            $image_cropped = imagerotate( $image_cropped, 180, 0 );
                            break;
                        case 6:
                            $image_cropped = imagerotate( $image_cropped, - 90, 0 );
                            break;
                        case 8:
                            $image_cropped = imagerotate( $image_cropped, 90, 0 );
                            break;
                    }
                }

                $save_image_func = "image{$func_end}";
                $save_path       = str_replace( ".{$ext}", "", $image_path ) . "{$width}_{$height}.{$ext}";
                $save_image_func( $image_cropped, $save_path );
                imagedestroy( $image_cropped );
                imagedestroy( $source );
            }else{
                return null;
            }
            return $save_path;
        }catch(\Exception $e){
            return null;
        }
    }
}
