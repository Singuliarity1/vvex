<?php

namespace App\Handlers;

use App\Http\Resources\UserBike\UserBikeResource;
use App\Models\UserBike;

class SearchHandler {
    public static function search() {
        $bike_filter = UserBike::searchFilter();
        $bikes       = UserBikeResource::collection( $bike_filter["bikes"] )->toArray( null );

        return [
            "html"      => view( 'ajax/bikes_list', [
                "data"       => $bikes,
                "hide_white" => ! $bike_filter["white"]
            ] )->render(),
            "count"     => count( $bikes ),
            "page"      => $bike_filter["page"],
            "all_count" => $bike_filter["all_count"],
            "status"    => true
        ];
    }
}
