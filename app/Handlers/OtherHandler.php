<?php


namespace App\Handlers;


use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;

class OtherHandler {

    public static function getLocation( $location ) {
        $full_location = explode( ",", $location );

        return [
            "country" => $full_location[0] ?? "",
            "city"    => $full_location[1] ?? "",
            "address" => $full_location[2] ?? ""
        ];
    }

    public static function setUrlReturn() {
        global $request;
        Session::put('link_back', $request->path());
    }

    public static function urlBack() {
        $url_back = Session::get( 'url_back' );
        if ( $url_back != null ) {
            Session::remove( 'url_back' );

            return redirect( Crypt::decrypt( $url_back[1]??$url_back[0] ) );
        }

        return redirect( "/" );
    }

    public static function getUrlBack() {
        return Session::get( 'link_back' );
    }

}
