<?php


namespace App\Models;


class BikeFile extends BaseModel{

    protected $table = "bike_files";
    public $timestamps = false;
    protected $fillable = [
        'bikes_id',
        'files_id',
        'cover',
        'order'
    ];
    public function getFiles(){

        return $this->belongsTo(UserFile::class,"files_id","id");
    }

    public function delete() {

        UserFile::where("id",$this->files_id)->delete();
        return parent::delete();
    }

}
