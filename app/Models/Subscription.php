<?php


namespace App\Models;


class Subscription extends BaseModel {

    protected $table = "subscriptions";
    public $timestamps = false;
    protected $fillable = [
        'email'
    ];

}
