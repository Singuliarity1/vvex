<?php


namespace App\Models;


class ImageSize extends BaseModel {
    protected $table = "image_sizes";
    public $timestamps = false;

    public function delete() {
        $this->deleteFileByPath();
        return parent::delete();
    }

    public static function regenerateImage() {
        self::where('size_type','<>','original')->delete();
        $allImages=self::all();
        if($allImages){
            foreach($allImages as $image){
                $attr=$image->toArray();
                $file_name=basename($attr['path']);
                UserFile::saveImagesAllSizes($file_name,$attr['images_id']);
            }
        }
    }

}
