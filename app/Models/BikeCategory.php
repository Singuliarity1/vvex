<?php


namespace App\Models;


class BikeCategory extends BaseModel {

    protected $table = "bikes_categories";
    public $timestamps = false;
}
