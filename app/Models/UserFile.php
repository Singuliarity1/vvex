<?php


namespace App\Models;


use App\Handlers\ImageHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserFile extends BaseModel {

    protected $table = "files";
    public $timestamps = false;


    public static function saveFiles() {

        return self::saveFileFromRequest( "files" );
    }

    public static function saveBg() {

        return self::saveFileFromRequest( "bg" );
    }

    public static function savePhoto() {

        return self::saveFileFromRequest( "photo" );
    }

    public static function saveFileFromRequest( $key ) {
        global $request;

        $files = $request->file( $key );
        if ( $request->hasFile( $key ) ) {
            $all_ids = [];
            if ( is_array( $files ) ) {
                foreach ( $files as $file ) {
                    $all_ids[] = self::saveSingleFile( $file );
                }
            } else {
                $all_ids[] = self::saveSingleFile( $files );
            }

            return $all_ids;
        }

        return null;
    }

    public static function removeFile( $id ) {
        $file = UserFile::find( $id );
        if ( $file != null ) {
            $file->delete();
        }
    }

    public static function saveRemoteFile( $url ) {
        $file_model       = new UserFile();
        $file_model->path = $url;

        return self::saveFileGetId( $file_model );
    }

    public static function saveDefaultBackground() {
        $file_model       = new UserFile();
        $file_model->path = "default";
        $id_image         = self::saveFileGetId( $file_model );

        return $id_image;
    }

    public static function saveSingleFile( $file ) {

        $file_model = new UserFile();
        $file->store( 'public/files' );
        $file_model->path = "/files/{$file->hashName()}";
        $id_image         = self::saveFileGetId( $file_model );
        self::save_images_with_different_sizes( $file, $id_image );

        return $id_image;
    }

    public static function saveFileGetId( $file_model ) {
        $file_model->save();

        return $file_model->id;
    }

    private static function save_images_with_different_sizes( $file, $id_image ) {
        self::saveImagesAllSizes( $file->hashName(), $id_image );
    }

    public static function saveImagesAllSizes( $file_name_original, $id_image ) {
        $sizes = [ "small", "small_slider", "medium", "big_slider", "original", "cover" ];

        foreach ( $sizes as $size ) {
            $path = ImageHandler::resize( public_path( '/storage/files/' . $file_name_original ), $size );

            if ( $path != null ) {
                $file_name        = pathinfo( $path );
                $image            = new ImageSize();
                $image->images_id = $id_image;
                $image->size_type = $size;
                $image->path = "/files/{$file_name['filename']}.{$file_name['extension']}";
                $image->makeSave();
            }
        }
    }

    public function getImageSizes() {

        return $this->hasMany( "App\Models\ImageSize", "images_id" );

    }


    public function delete() {
        $this->deleteFileByPath();
        $image_sizes = ImageSize::where( "images_id", $this->id )->get();
        if ( $image_sizes != null ) {
            foreach ( $image_sizes as $image ) {
                $image->delete();
            }
        }

        return parent::delete();
    }
}


