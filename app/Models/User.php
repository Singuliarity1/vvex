<?php


namespace App\Models;


use App\Handlers\OtherHandler;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends BaseModel implements AuthenticatableContract {

    use Authenticatable;

    protected $table = "users";
    public $timestamps = false;
    protected $fillable = [
        'email',
        'phone',
        'active',
        'activate_token',
        'page_title',
        'name',
        'email_secondary',
        'country',
        'city',
        'fb',
        'google',
        'vk',
        'apple',
        'password',
        'reset_token',
        'about',
        'strava_link',
        'insta_link',
        'fb_link',
        'yt_link',
        'vk_link',
        'tw_link',
        'agreement'
    ];

    public function makeSave() {
        global $request;

        if ( $this->getOriginal( 'password' ) != $this->password ) {
            $this->password = bcrypt( $this->password );
        }
        if ( $request->location ) {
            $location      = explode( ",", $request->input( "location" ) );
            $this->country = $location[0];
            if ( count( $location ) > 1 ) {
                $this->city = trim( implode( ",", array_splice( $location, 1 ) ) );
            }
        }
        $this->agreement = $request->agreement;

        return parent::makeSave();
    }

    public function makeUpdate() {
        global $request;

        $bg_removed = $request->input( "remove" );

        if ( $bg_removed ) {
            UserFile::removeFile( implode(",", $bg_removed) );
            $id_image = UserFile::saveDefaultBackground();
        }

        $this->updated_at = new \DateTime();
        $photo            = UserFile::savePhoto();
        $bg               = UserFile::saveBg();
        $this->updateImage( $photo, "photo_id" );
        $this->updateImage( $bg, "bg_id" );
        if ( $bg_removed ) { $this->updateImage( [$id_image], "bg_id" ); }
        $full_location      = OtherHandler::getLocation( $request->input( "location" ) );
        $request["country"] = $full_location["country"];
        $request["city"]    = $full_location["city"];
        User::updateByValues( [ "id" => $this->id ], $request->input() );

        parent::makeSave();
    }

    private function updateImage( $new_ids, $field ) {
        if ( $new_ids != null && $new_ids[0] != $this->{$field} ) {
            UserFile::removeFile( $this->{$field} );
            $this->{$field} = $new_ids[0];
        }

        return null;
    }

    public function getAuthIdentifierName() {

        return "id";
    }

    public function getAuthIdentifier() {

        return $this->id;
    }

    public function getAuthPassword() {

        return $this->password;
    }

    public function getRememberToken() {
        // TODO: Implement getRememberToken() method.
    }

    public function setRememberToken( $value ) {
        // TODO: Implement setRememberToken() method.
    }

    public function getRememberTokenName() {
        // TODO: Implement getRememberTokenName() method.
    }
    private function generateToken(){
        return sha1(time() . $this->email);
    }
    public function activate(){
        $this->active=true;
        $this->save();
    }
    public function createToken($key){
        $token=$this->{$key}=$this->generateToken();
        $this->save();
        return $token;
    }
    public function getBg() {

        return $this->hasOne( "App\Models\UserFile", "id", "bg_id" );
    }

    public function getPhoto() {

        return $this->hasOne( "App\Models\UserFile", "id", "photo_id" );
    }

    public function getUserBikes() {

        return $this->hasMany( "App\Models\UserBike", "users_id", "id" );
    }

    public function delete() {
        $bikes=UserBike::where('users_id',$this->id)->get();
        if($bikes) {
           foreach ($bikes as $bike) {
               $bike->makeDelete();
            }
        }
        return parent::delete();
    }

}
