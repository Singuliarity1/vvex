<?php


namespace App\Models;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserBikeLike extends BaseModel {

    protected $table = "users_bikes_likes";
    public $timestamps = false;
    protected $fillable = [
        'bikes_id',
        'like'
    ];

    public function makeSave() {

        $query = $this::where( "bikes_id", $this->bikes_id );
        if ( Auth::check() ) {
            $this->users_id = auth()->user()->id;
            $query          = $query->where( "users_id", $this->users_id );
        } else {
            $this->session_id = Session::getId();
            $query            = $query->where( "session_id", $this->session_id );
        }
        if ( $query->first() == null ) {
            return parent::save();
        }

        return null;
    }

}
