<?php


namespace App\Models;


use Google_Client;
use Google_Service_Sheets;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Components extends BaseModel {
    protected $table = "components";
    public $timestamps = false;
    protected $fillable = [
        "type",
        "brand_list",
        "brand_slug_list",
        "model_list",
        "model_slug_list",
    ];
    public static $sheet_slug = [
        'brand'                 => 'Велосипеды Бренд',
        'size'                  => 'Размер рамы',
        'material'              => 'Материал рамы',
        //'groupset_drive_unit' => [
        'equipment_group'       => 'Группа оборудования',
        'pedals'                => 'Педали',
        'carriage'              => 'Каретка',
        'cassette'              => 'Кассета',
        'planetary_hub'         => 'Планетарная втулка',
        'singspeed'             => 'Сингспид',
        'connecting_rods'       => 'Шатуны',
        'chainring'             => 'Передняя звезда',
        'chain'                 => 'Цепь',
        // ],
        // 'shifters_switches'   => [
        'shifters'              => 'Шифтеры',
        'front_switch'          => 'Пер. переключатель',
        'rear_switch'           => 'Задний переключатель',
        'switch_rollers'        => 'Ролики переключателя',
        //],
        //'control'             => [
        'aerobar'               => 'Аэробар',
        'steering_wheel'        => 'Руль',
        'grips'                 => 'Грипсы',
        'handlebar_winding'     => 'Обмотка руля',
        'steering_column'       => 'Рулевая колонка',
        'stem'                  => 'Вынос руля',
        //],
        //'suspension_fork'     => [
        'rigid_fork'            => 'Ригидная вилка',
        'shock_absorber_26'     => 'Передний амортизатор 26"',
        'shock_absorber_27_5'   => 'Передний амортизатор 27.5"',
        'shock_absorber_29'     => 'Передний амортизатор 29"',
        'shock_absorber_700с'   => 'Передний амортизатор 700C',
        'spring'                => 'Пружина',
        'rear_shock_absorber'   => 'Задний амортизатор',
        //],
        //'brakes'              => [
        'rim_brakes'            => 'Ободные тормоза',
        'disc_brake'            => 'Дисковый тормоз',
        'brake_levers'          => 'Тормозные ручки',
        //],
        //'saddle_seat_post'    => [
        'saddle'                => 'Седло',
        'seatpost'              => 'Подседельный штырь',
        'cushioned_seatpost'    => 'Амортизированный подседельный штырь',
        'telescopic_seatpost'   => 'Телескопический подседельный штырь',
        //],
        //'wheels_tires_tubes'  => [
        'wheels_26'             => 'Колеса 26"',
        'wheels_27_5'           => 'Колеса 27.5"',
        'wheels_29'             => 'Колеса 29"',
        'wheels_700C'           => 'Колеса 700C',
        'rims'                  => 'Обода',
        'tires'                 => 'Шины',
        'inner_tire'            => 'Камера',
        'sleeve'                => 'Втулка',
        'drum_bushing'          => 'Барабан втулки',
        //],
        //'power_meter'         => [
        'power_connecting_rods' => 'Измеритель мощности (шатуны)',
        'power_pedals'          => 'Измеритель мощности (педали)',
        //],
//            'component_front_derailleur' => 'Пер. переключатель ШОССЕ',
//            'component_rear_derailleur'  => 'Зад. переключатель. ШОССЕ',
//            'component_shifters'         => 'Шифтеры ШОССЕ',
//            'component_system'           => 'Система ШОССЕ',
//            'component_carriage'         => 'Каретка ШОССЕ',
//            'component_connecting_rods'  => 'Шатуны',
//            'component_cassette'         => 'Кассета ШОССЕ',
//            'component_chain'            => 'Цепь',
//            'component_brakes'           => 'Тормоза',
//            'component_wheel_brand'      => 'Марка колес',
//            'component_tire_brand'       => 'Марка шин',
//            'component_tire_size'        => 'Размер шин',
//            'component_stem'             => 'Руль',
//            'component_steering_wheel'   => 'Вынос руля',
//            'component_saddle'           => 'Седло',
//            'component_seat_post'        => 'Подседельный штырь',
//            'component_pedals'           => 'Педали ШОССЕ',
    ];

    private static function getService() {
        $client = new Google_Client();

        $client->setAuthConfig( base_path( env( 'GOOGLE_APPLICATION_CREDENTIALS_FILE' ) ) );
        $client->setApplicationName( 'Velovex' );
        $client->setScopes( 'https://www.googleapis.com/auth/spreadsheets' );
        $client->setPrompt( 'select_account consent' );
        $client->setAccessType( 'online' );
        $client->addScope( 'https://www.googleapis.com/auth/spreadsheets' );
        $client->setDeveloperKey( env( 'GOOGLE_DEVELOPER_KEY' ) );

        $service = new Google_Service_Sheets( $client );

        return $service;
    }

    public static function setDataFromTable() {
        $service       = self::getService();
        $spreadsheetId = '1OO_NvsekHa5IgrS2rUK7ci9fwImYopppmxssQHws0Ok';
        $sheets        = $service->spreadsheets->get( $spreadsheetId )->getSheets();
        foreach ( self::$sheet_slug as $slug => $sheet ) {
            foreach ( $sheets as $sheet_one ) {
                if ( $sheet != $sheet_one->getProperties()->getTitle() ) {
                    continue;
                }
                $range  = "'" . $sheet_one->getProperties()->getTitle() . "'!A2:EL";
                $values = $service->spreadsheets_values->get( $spreadsheetId, $range )->getValues();

                foreach ( $values as $val ) {
                    if ( in_array( 'Loading...', $val ) ) {
                        continue;
                    }
                    $fillable_ar = [
                        'type'            => $slug,
                        'brand_list'      => $val[0] ?? null,
                        'brand_slug_list' => $val[1] ?? null,
                        'model_list'      => $val[2] ?? null,
                        'model_slug_list' => $val[3] ?? null,

                    ];
                    $component   = Components::where( 'type', $fillable_ar['type'] )->
                    where( 'brand_list', $fillable_ar['brand_list'] )->
                    where( 'brand_slug_list', $fillable_ar['brand_slug_list'] )->
                    where( 'model_list', $fillable_ar['model_list'] )->
                    where( 'model_slug_list', $fillable_ar['model_slug_list'] )->first();
                    if ( $component == null ) {
                        ( new Components( $fillable_ar ) )->makeSave();
                    } else {
                        $component->fill( $fillable_ar );
                        $component->makeSave();
                    }
                }
            }
        }

        echo 'Synchronized';

    }

    public static function getAllBrand() {
        $all = Components::where( 'brand_list', '<>', "Нет в списке" )->
        orderBy( 'type' )->
        orderBy( 'brand_list' );
        $not_in_list = Components::where( 'brand_list', '=', "Нет в списке" )->get()->toArray();


        $all = array_merge($all->get()->toArray(),$not_in_list);

        $array = [];

        foreach ( $all as $attributes ) {
            if ( ! isset( $array[ $attributes['type'] ] ) ) {
                $array[ $attributes['type'] ] = [];
            }

            if ( $attributes['model_list'] == null ) {

                if ( isset( $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ] ) ) {
                    $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ]['id']         = $attributes['id'];
                    $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ]['brand_list'] = $attributes['brand_list'];
                } else {

                    $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ] = [
                        'id'         => $attributes['id'],
                        'brand_list' => $attributes['brand_list'],
                        'models'     => []
                    ];
                }
            } elseif ( $attributes['brand_list'] == null ) {

                $array[ $attributes['type'] ][ $attributes['model_slug_list'] ] = $attributes['model_list'];
            } else {

                $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ]['brand_list'] = $attributes['brand_list'];

                if ( ! isset( $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ] ) ||
                     ! is_array( $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ] ) ) {
                    $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ]['models'] = [];
                }


                $array[ $attributes['type'] ][ $attributes['brand_slug_list'] ]['models'][ $attributes['model_slug_list'] ] = [
                    'id'         => $attributes['id'],
                    'model_list' => $attributes['model_list'],
                ];
            }
        }

        return $array;
    }

}
