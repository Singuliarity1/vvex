<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmark extends BaseModel {
    use SoftDeletes;

    protected $table = "bookmarks";
    public $timestamps = false;
    protected $fillable = [
        'bikes_id',
    ];

    public function makeSave() {

        $this->users_id = auth()->user()->id;
        if ( Bookmark::where( "users_id", $this->users_id )->where( "bikes_id", $this->bikes_id )->first() == null ) {
            return parent::save();
        }
        return null;
    }
}
