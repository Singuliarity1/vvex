<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "email" => "required|email|unique:users",
            "password" => "required",
            "phone" => "unique:users",
        ];
    }

    public function failedValidation( Validator $validator ):void {

    }

    public function messages(): array {
        return [
            "email.required" => "Необходимо указать e-mail",
            "email.email"    => "Необходимо указать e-mail",
            "email.unique"   => "Такой e-mail адрес уже зарегистрировн",
            "phone.unique"   => "Такой номер уже зарегистрировн"
        ];
    }
}
