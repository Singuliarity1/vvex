<?php


namespace App\Http\Requests;


class UserBikeRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "bikes_id" => "required",
        ];
    }


}
