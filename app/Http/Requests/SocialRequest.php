<?php


namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;


class SocialRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "vk"     => "unique:users,vk",
            "apple"  => "unique:users,apple",
            "google" => "unique:users,google",
            "fb"     => "unique:users,fb",
        ];
    }

    public function failedValidation( Validator $validator ): void {


    }

    private function errorText( $social ) {
        return "Пользователь {$social} уже зарегистрирован";
    }

    public function messages(): array {
        return [
            "vk.unique"     => $this->errorText( "vk" ),
            "apple.unique"  => $this->errorText( "apple" ),
            "google.unique" => $this->errorText( "google" ),
            "fb.unique"     => $this->errorText( "fb" ),
        ];
    }

}
