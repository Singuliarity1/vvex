<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "email" => "required|exists:users,email",

        ];
    }

    public function failedValidation( Validator $validator ):void {


    }

    public function messages(): array {
        return [
            "email.required" => "Необходимо указать e-mail",
            "email.exists" => "Пользователя с таким e-mail в базе VELOVEX не существует",
        ];
    }
}
