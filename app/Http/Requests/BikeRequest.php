<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Log;

class BikeRequest extends BaseRequest {

    public function rules(): array {


        return [
            "serial"   => "required|unique:bikes,serial" . $this->getIgnore(),
            "brand"    => "required",
            "model"    => "required",
            "year"     => "required",
            "material" => "required",
            "size"     => "required",
            "files"    => "array",
            "files.*"  => "image|max:10240",
        ];
    }

    public function failedValidation( Validator $validator ): void {


    }

    public function messages(): array {
        return [
            "serial.required"       => "Необходимо указать серийный номер",
            "serial.exists"         => "Серийный номер должен быть уникальным",
            "brand.required"        => "Необходимо указать бренд",
            "year.required"         => "Необходимо указать год",
            "material.required"     => "Необходимо указать материал",
            "size.required"         => "Необходимо указать размер",
            "files.*.max"           => "Максимальный размер изображения 10Мб",
            "files.*.mimes"         => "Неверный формат изображения (jpg,jpeg,png)"
        ];
    }
}
