<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;

class RestorePassRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "password" => "required|confirmed"
        ];
    }

    public function failedValidation( Validator $validator ):void {


    }

    public function messages(): array {
        return [
            "password.required" => "Необходимо указать пароль",
            "password.confirmed" => "Пароль не совпадает",
        ];
    }

}
