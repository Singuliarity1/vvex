<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Auth;

class PasswordRequest extends BaseRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return string
     */
    public function getIgnore(){
        return Auth::check()? ",".(Auth::id()).",id" : "";
    }

    public function rules(): array
    {
        return [
            "password" => "confirmed",
            "phone" => "unique:users,phone" . $this->getIgnore()."|Nullable",
            "email" => "required|unique:users,email" . $this->getIgnore(),
            "photo"  => "mimes:jpg,jpeg,png | max:8192",
            "bg"  => "mimes:jpg,jpeg,png | max:10240",
        ];
    }

    public function failedValidation(Validator $validator): void
    {
    }

    public function messages(): array
    {
        return [
            "password.confirmed" => "Пароль не совпадает",
            "phone.unique" => "Такой номер уже зарегистрирован",
            "email.unique" => "Такой email уже зарегистрирован",
            "unique" => "Такой пользователь уже зарегистрирован",
            "photo.max" => "Размер файла не должен превышать 8Мб.",
            "photo.mimes" => "Неверный формат изображения (jpg,jpeg,png)",
            "bg.max" => "Размер файла не должен превышать 10Мб.",
            "bg.mimes" => "Неверный формат изображения (jpg,jpeg,png)",
            'uploaded' => 'Ошибка загрузки'
        ];
    }

}
