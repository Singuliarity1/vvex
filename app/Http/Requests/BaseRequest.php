<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

abstract class BaseRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public $ignoreId = 0;
    public function authorize(): bool {
        return true;
    }


    public function validateWithIgnore( $id ) {
        $this->ignoreId = $id;
        $this->validate($this->rules());
    }

    public function getIgnore(){
        return $this->ignoreId != 0 ? ",{$this->ignoreId},id" : "";
    }

    public function failedValidation( Validator $validator ): void {
        $errors = $validator->errors();
        throw new HttpResponseException( response()->json( [
            "result" => [
                "status" => false,
                'errors' => $errors
            ]
        ], Response::HTTP_UNPROCESSABLE_ENTITY ) );
    }


}
