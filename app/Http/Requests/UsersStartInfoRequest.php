<?php


namespace App\Http\Requests;


class UsersStartInfoRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "name"    => "required",
            "surname" => "required",
        ];
    }

    public function messages(): array {
        return [
            "name.required"    => "Необходимо указать Имя",
            "surname.required" => "Необходимо указать Фамилию",
        ];
    }

}
