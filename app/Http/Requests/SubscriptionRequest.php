<?php


namespace App\Http\Requests;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SubscriptionRequest extends BaseRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array {
        return [
            "email" => "required|email",
        ];
    }

    public function failedValidation( Validator $validator ): void {
        throw new HttpResponseException( response( [ "status" => false ] ) );
    }

    public function messages(): array {
        return [
            "email.required" => "Необходимо указать e-mail"
        ];
    }
}
