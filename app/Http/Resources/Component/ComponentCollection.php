<?php


namespace App\Http\Resources\Component;


use App\Http\Resources\Base\BaseCollection;

class ComponentCollection  extends BaseCollection {
    public $collects = 'App\Http\Resources\Component\ComponentResource';

}
