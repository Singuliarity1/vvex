<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\Bike\BikeResource;
use App\Http\Resources\UserFile\UserFileResource;
use App\Http\Resources\UserBike\UserBikeResource;
use App\Models\UserBike;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends BaseResource {
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request = null ) {
        global $request;
        if ( $this->resource != null ) {
            $array = $this->resource->toArray();
            if ( ! $request->input( "bookmark" ) ) {
                $request['authorId'] = $array["id"];
            }
            $array["bg"]    = ( new UserFileResource( $this->getBg ) )->toArray();
            $array["photo"] = ( new UserFileResource( $this->getPhoto ) )->toArray();
            $array["bikes"] = UserBikeResource::collection( UserBike::searchFilter()["bikes"] )->toArray( null );

            return $array;
        }

        return null;
    }
}
