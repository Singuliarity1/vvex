<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Base\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\User\UserResource';
}
