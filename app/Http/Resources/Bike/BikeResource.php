<?php

namespace App\Http\Resources\Bike;

use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\BikeCategory\BikeCategoryResource;
use App\Http\Resources\BikeFile\BikeFileResource;
use App\Http\Resources\Bookmark\BookmarkResource;
use App\Http\Resources\Component\ComponentResource;
use App\Models\Components;
use App\Http\Resources\UserBikeLike\UserBikeLikeResource;
use Illuminate\Support\Facades\Auth;

class BikeResource extends BaseResource {
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request = null ) {

        if ( $this->resource != null ) {
            $components_type_list=[
                'brand',
                'material',
                'equipment_group',
                'pedals',
                'carriage',
                'cassette',
                'planetary_hub',
                'singspeed',
                'connecting_rods',
                'chainring',
                'chain',
                'shifters',
                'front_switch',
                'rear_switch',
                'roller_skates',
                'switch_rollers',
                'aerobar',
                'steering_wheel',
                'grips',
                'handlebar_winding',
                'steering_column',
                'stem',
                'rigid_fork',
                'shock_absorber_26',
                'shock_absorber_27_5',
                'shock_absorber_29',
                'shock_absorber_700с',
                'spring',
                'rear_shock_absorber',
                'rim_brakes',
                'disc_brake',
                'brake_levers',
                'saddle',
                'seatpost',
                'cushioned_seatpost',
                'telescopic_seatpost',
                'wheels_26',
                'wheels_27',
                'wheels_29',
                'wheels_700C',
                'rims',
                'tires',
                'inner_tire',
                'sleeve',
                'drum_bushing',
                'power_connecting_rods',
                'power_pedals',
            ];
            $array = $this->resource->toArray();
            $array["bike_like"]     = ( new UserBikeLikeResource( $this->getUserBikeLike() ) )->toArray();
            $array["bike_files"]    = BikeFileResource::collection( $this->getBikeFiles )->toArray( $request );
            $array["bike_category"] = ( new BikeCategoryResource( $this->getBikeCategory ) )->toArray();
            $array["bike_bookmark"] = Auth::check() ? ( new BookmarkResource( $this->getBookmark() ) )->toArray() : [];
            foreach($components_type_list as $type){
                if($array[$type]){
                    $array[$type]=(new ComponentResource(Components::find($array[$type])))->toArray();
                }
            }

            return $array;
        }

        return null;
    }
}
