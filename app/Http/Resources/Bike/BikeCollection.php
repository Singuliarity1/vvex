<?php

namespace App\Http\Resources\Bike;

use App\Http\Resources\Base\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BikeCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\Bike\BikeResource';
}
