<?php


namespace App\Http\Resources\UserBike;


use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\Bike\BikeResource;
use App\Http\Resources\BikeFile\BikeFileResource;
use App\Http\Resources\User\UserResource;

class UserBikeResource extends BaseResource {
    public function toArray( $request=null ) {
        if($this->resource!=null) {
            $stringLocation = '';
            $array = $this->resource->toArray();

            foreach( $array as $key => $value ){
                if($key == "country" && $value) {
                    $stringLocation = $stringLocation . $value;
                }
                if($key == "city" && $value) {
                    $stringLocation =  $stringLocation . "," . $value;
                }
                if($key == "address" && $value) {
                    $stringLocation =  $stringLocation . " " . $value;
                }
            }

            $array["bike"]             = ( new BikeResource( $this->getBike ) )->toArray();
            $array["bike"]["location"] = $stringLocation;

            return $array;
        }
        return null;
    }
}
