<?php


namespace App\Http\Resources\UserBike;


use App\Http\Resources\Base\BaseCollection;

class UserBikeCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\UserBike\UserBikeResource';
}
