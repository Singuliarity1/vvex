<?php


namespace App\Http\Resources\ImageSize;


use App\Http\Resources\Base\BaseResource;

class ImageSizeResource extends BaseResource {
    public function toArray( $request = null ) {
        if($this->resource) {
            $array         = $this->resource->toArray();
            $array["path"] = !stripos($array["path"],'://')?url( '/storage' ) . $array["path"]:$array["path"];
            return $array;
        }
        return null;
    }
}
