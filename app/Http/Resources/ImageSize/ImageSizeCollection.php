<?php


namespace App\Http\Resources\ImageSize;


use App\Http\Resources\Base\BaseCollection;

class ImageSizeCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\ImageSize\ImageSizeResource';

}
