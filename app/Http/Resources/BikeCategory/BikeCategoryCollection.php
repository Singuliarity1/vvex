<?php

namespace App\Http\Resources\BikeCategory;

use App\Http\Resources\Base\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BikeCategoryCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\BikeCategory\BikeCategoryResource';
}
