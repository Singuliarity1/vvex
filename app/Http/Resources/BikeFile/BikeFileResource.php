<?php

namespace App\Http\Resources\BikeFile;

use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\UserFile\UserFileCollection;
use App\Http\Resources\UserFile\UserFileResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BikeFileResource extends BaseResource {
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request=null ) {
        $array=$this->resource->toArray();
        $array["file"]= (new UserFileResource($this->getFiles))->toArray();
        return $array;
    }
}
