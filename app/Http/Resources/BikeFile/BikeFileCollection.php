<?php

namespace App\Http\Resources\BikeFile;

use App\Http\Resources\Base\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BikeFileCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\BikeFile\BikeFileResource';
}
