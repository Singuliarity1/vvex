<?php

namespace App\Http\Resources\UserBikeLike;

use App\Http\Resources\Base\BaseCollection;

class UserBikeLikeCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\UserBikeLike\UserBikeLikeResource';
}
