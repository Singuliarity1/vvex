<?php

namespace App\Http\Resources\UserBikeLike;

use App\Http\Resources\Base\BaseResource;
use App\Models\UserBikeLike;

class UserBikeLikeResource extends BaseResource {


    public function toArray( $request = null ) {
        if ( $this->resource != null && count( $this->resource->toArray() ) > 0 ) {
            $array = $this->resource->toArray();
            $bike_like = UserBikeLike::where( "bikes_id", $array["bikes_id"] );

            $array["like"] = (clone $bike_like)->where( "like", 1 )->count();
            $array["fire"] = $bike_like->where( "like", 2 )->count();

            return $array;
        }

        return null;
    }
}
