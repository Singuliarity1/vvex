<?php

namespace App\Http\Resources\Bookmark;

use App\Http\Resources\Base\BaseCollection;

class BookmarkCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\Bookmark\BookmarkResource';

}
