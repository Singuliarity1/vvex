<?php

namespace App\Http\Resources\UserFile;

use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\BikeFile\BikeFileResource;
use App\Http\Resources\ImageSize\ImageSizeResource;
use App\Models\ImageSize;
use Illuminate\Http\Resources\Json\JsonResource;

class UserFileResource extends BaseResource {

    public function toArray( $request = null ) {
        if ( $this->resource != null ) {

            $sizes                = ImageSizeResource::collection( $this->getImageSizes )->toArray( $request );
            $array                = $this->resource->toArray();
            $array["image_sizes"] = $sizes != null ? $this->destructByKey( $sizes ) : null;
            $array["path"]        = $array["path"]!='default'?(!stripos($array["path"],'://')?url( '/storage' ) . $array["path"]:$array["path"]):$array["path"];


            return $array;
        }

        return null;
    }

    private function destructByKey( $sizes ) {
        $destruct_arr = [];
        foreach ( $sizes as $size ) {
            $destruct_arr[ $size["size_type"] ] = $size["path"];
        }

        return $destruct_arr;
    }

}
