<?php

namespace App\Http\Resources\UserFile;

use App\Http\Resources\Base\BaseCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserFileCollection extends BaseCollection {
    public $collects = 'App\Http\Resources\File\FileResource';
}
