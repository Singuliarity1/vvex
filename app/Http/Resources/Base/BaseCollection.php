<?php


namespace App\Http\Resources\Base;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseCollection extends ResourceCollection {
    public $collects = 'App\Http\Resources\Base\BaseResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request = null ) {
        return $this->collection->map( function ( $row ) use ( $request ) {
            return ( new $this->collects( $row ) )->toArray( $request );
        })->toArray();
    }
}
