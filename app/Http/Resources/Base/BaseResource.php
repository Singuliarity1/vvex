<?php


namespace App\Http\Resources\Base;


use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource {
    public function __construct( $resource ) {
        parent::__construct( $resource );
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray( $request = null ) {
        return $this->resource == null ? null : $this->resource->toArray();
    }
}
