<?php


namespace App\Http\Handlers;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailHandler extends Mailable {
    use Queueable, SerializesModels;

    private $link;
    private $template;
    private $user;
    private $bike;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $link, $template, $user, $bike = null ) {
        $this->link = $link;
        $this->template = $template;
        $this->user = $user;
        $this->bike = $bike;

    }

    public function setTemplate( $template ) {
        $this->template = $template;
    }

    public function getTemplateHtml() {
        return view( $this->template, [
            'link' => $this->link,
            'user' => $this->user,
            'bike' => $this->bike
        ] )->render();
    }

    public function sendMail( $email, $subject ) {
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From: hello@velovex.ru\r\n";
        $headers .= "Content-Type: text/html; charet=UTF-8\r\n";
        return mail( $email, $subject, $this->getTemplateHtml(), $headers );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->from( 'info@lobsterlab.ru' )
                    ->view( $this->template )
                    ->with( [
                        'link' => $this->link,
                        'user' => $this->user
                    ] );
    }
}
