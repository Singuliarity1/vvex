<?php

namespace App\Http\Controllers;


use App\Http\Handlers\MailHandler;
use App\Http\Requests\BaseRequest;
use App\Repositories\FileRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\True_;
use Symfony\Component\HttpFoundation\Request;

/**
 * @template T
 */
abstract class MainController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $model = "";

    protected function getDataRequest( $request ) {
        return count( $request->json()->all() ) == 0 ? $request->all() : $request->json()->all();
    }

    protected function save( $request ) {

        $request->validated();
        $model = new $this->model( $request->input() );
        if($model->makeSave()){
            return $model;
        }
        return null;
    }



    public function apiResponse( $modelResponse=null ) {
        $status = false;

        if ( $modelResponse != null ) {
            $status = ! $status;
        }

        return response( [ "status" => $status, "result" => $modelResponse ] );
    }
}
