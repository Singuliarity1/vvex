<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;

use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PersonalController extends MainController {

    protected function update( Request $request ) {
        if ( Auth::check() ) {
            return $this->apiResponse( User::updateByValues( [ "id" => auth()->user()->id ], $request->input() ) );
        }

        return false;
    }

    protected function personal_info( Request $request ) {
        if ( Auth::check() ) {
            return $this->apiResponse( (new UserResource(User::find( auth()->user()->id)))->toArray() );
        }

        return false;
    }
}
