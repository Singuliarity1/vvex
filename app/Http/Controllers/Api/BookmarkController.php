<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;
use App\Http\Requests\BikeRequest;
use App\Http\Requests\BookmarkRequest;
use App\Models\Bike;
use App\Models\Bookmark;
use App\Models\UserBike;

class BookmarkController extends MainController {

    protected $model = Bookmark::class;

    protected function store(BookmarkRequest $request){

        return $this->apiResponse($this->save( $request ));
    }

    protected function deleteAll( BookmarkRequest $request ) {

        $this->model::where( "users_id", auth()->user()->id )->delete();
        return $this->apiResponse([]);
    }

    protected function delete( BookmarkRequest $request, $bookmark_id ) {

        $this->model::where( "id", $bookmark_id )->where( "users_id", auth()->user()->id )->delete();
        return $this->apiResponse([]);
    }
}
