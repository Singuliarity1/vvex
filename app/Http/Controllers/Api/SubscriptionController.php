<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;
use App\Http\Requests\SubscriptionRequest;
use App\Models\Bike;
use App\Models\Subscription;

class SubscriptionController extends MainController {

    protected $model = Subscription::class;

    protected function store(SubscriptionRequest $request){

        return $this->apiResponse((new $this->model($request->all()))->makeSave());

    }
}
