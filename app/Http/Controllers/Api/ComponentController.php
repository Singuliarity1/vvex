<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\MainController;
use App\Models\Components;

class ComponentController extends MainController {

    public function sync(){
        Components::setDataFromTable();
    }

}
