<?php


namespace App\Http\Controllers\Api;

use App\Http\Controllers\MainController;

use App\Handlers\SearchHandler;
use App\Http\Requests\BikeRequest;


class SearchController extends MainController {

    public function search( BikeRequest $request ) {
        return json_encode( SearchHandler::search() );
    }

}
