<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;
use App\Http\Requests\BikeRequest;
use App\Http\Requests\UserBikeRequest;
use App\Models\Bike;
use App\Models\UserBike;

class UserBikeController extends MainController {

    protected $model = UserBike::class;

    protected function update( UserBikeRequest $request ) {

        //$request->validated();
        try {
            $bike=$this->model::where("bikes_id", $request->input("bikes_id"))->where("users_id",auth()->user()->id)->withTrashed()->first();
            if($bike) {
                return $this->apiResponse( $this->model::updateByValues( [
                    "bikes_id" => $request->input( "bikes_id" ),
                    "users_id" => auth()->user()->id
                ], $request->input() ) );
            }
        }catch(\Exception $e){

        }
        return $this->apiResponse([]);
    }

    protected function delete( UserBikeRequest $request ) {

        $request->validated();

        $bike=$this->model::where('bikes_id',$request->input("bikes_id"))->where('users_id',auth()->user()->id)->withTrashed()->first();
        if($bike) {

            return $this->apiResponse( $bike->makeDelete() );
        }

        return $this->apiResponse( false );
    }


}
