<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;
use App\Models\ImageSize;
use Illuminate\Http\Request;

class ImageController extends MainController {

    protected $model = ImageSize::class;

    protected function regenerate(Request $request){
        $this->model::regenerateImage();
        return $this->apiResponse([]);
    }

}
