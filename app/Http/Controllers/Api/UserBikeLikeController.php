<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\MainController;
use App\Http\Requests\UserBikeLikeRequest;
use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\UserBikeLike\UserBikeLikeResource;
use App\Models\UserBikeLike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserBikeLikeController extends MainController {

    protected $model = UserBikeLike::class;

    protected function store( UserBikeLikeRequest $request ) {

        return $this->apiResponse( ( new UserBikeLikeResource( $this->save( $request ) ) )->toArray() );
    }

    protected function statistic( Request $request ) {
        clearstatcache();
        $user_bike = UserBikeLike::where( "bikes_id", $request->input( "bikes_id" ) );
        if ( Auth::check() ) {
            $user_bike = $user_bike->where( "users_id", auth()->user()->id );
        } else {
            $user_bike = $user_bike->where( "session_id", Session::getId() );
        }

        if ( $user_bike->first() != null ) {
            return $this->apiResponse( ( new UserBikeLikeResource( UserBikeLike::where( "bikes_id", $request->input( "bikes_id" ) )->first() ) )->toArray() );
        }

        return $this->apiResponse( null );
    }


}
