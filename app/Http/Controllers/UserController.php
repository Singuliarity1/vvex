<?php


namespace App\Http\Controllers;


use App\Handlers\OtherHandler;
use App\Http\Handlers\MailHandler;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\RestorePassRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\BikeFile\BikeFileResource;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserController extends MainController {
    protected $model = User::class;


    protected function restore( UserRequest $request ) {

        $request->validated();
        $user = $this->model::where( 'email', $request->email )->first();
        if ( $user ) {
            $token = $user->createToken( 'reset_token' );
            try {
                ( new MailHandler( asset( "account/recover/{$token}" ),
                    'mails.restore',
                    $user->toArray() ) )->sendMail( $request->email, 'Restore Pass' );
            } catch ( Exception $e ) {
                Log::info( $e->getMessage() );
            }

            return view( 'pages/authorization/restored', array( "type_meta_info" => "recover" ) );
        }

        return view( 'errors/404', array( "type_meta_info" => "recover" ) );
    }

    protected function restore_show( UserRequest $request, $token ) {

        if ( $this->model::where( "reset_token", $token )->first() != null ) {
            return view( 'pages/authorization/new_password', array( "type_meta_info" => "recover" ) );
        }

        return redirect( 404 );
    }

    protected function show_profile( UserRequest $request ) {
        if ( auth()->user()->name != "" ) {
            return redirect( "settings" );
        }

        return view( "pages.authorization.create", [ "type_meta_info" => "login" ] );

    }

    protected function create_profile( UserRequest $request ) {
        $request->validated();
        $id   = auth()->user()->id;
        $user = $this->model::find( $id );
        $user->makeUpdate();

        return redirect( "/settings" );

    }

    protected function restore_password( RestorePassRequest $request, $token ) {

        $request->validated();
        $this->model::updateByValues(
            [ "reset_token" => $token ],
            [
                "active"      => true,
                "password"    => $request->password,
                "reset_token" => null,
            ]
        );

        return redirect( "/login" );
    }

    protected function settings( PasswordRequest $request ) {
        global $request;
        OtherHandler::setUrlReturn();
        $request->request->add( [ 'bookmark' => 1 ] );

        return $this->showUserView( 'pages/users/settings', auth()->user()->id, "page-settings" );
    }

    protected function saveSettings( PasswordRequest $request ) {
        $id = auth()->user()->id;
        $request['bookmark'] = 1;
        $user                = $this->model::find( $id );
        $user->makeUpdate();

        return $this->showUserView( 'pages/users/settings', auth()->user()->id );

    }

    protected function delete( UserRequest $request ) {

        $this->model::find( auth()->user()->id )->delete();

        return redirect( "/register" );
    }

    protected function profile( UserRequest $request, $id ) {
        global $request;

        $request->request->add( [ 'authorId' => $id ] );

        return $this->showUserView( 'pages/users/profile', $id, "page-profile" );
    }

    private function showUserView( $view, $id, $class = "" ) {
        $model = $this->model::where( "id", $id )->first();
        $res   = new UserResource( $model );
        if ( $res->toArray() != null ) {
            return view( $view, [ "data" => $res->toArray(), "type_meta_info" => "user", "class" => $class ] );
        }

        return redirect( 404 );
    }


}
