<?php


namespace App\Http\Controllers;


use App\Handlers\OtherHandler;
use App\Handlers\SearchHandler;
use App\Http\Handlers\MailHandler;
use App\Http\Requests\BikeRequest;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\UserBike\UserBikeResource;
use App\Models\Bike;
use App\Models\Components;
use App\Models\User;
use App\Models\UserBike;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class BikeController extends MainController {
    protected $model = Bike::class;

    protected function create() {

        return view( 'pages/bike/edit', [
            "method" => "POST",
            "class"  => "page-bike-edit new_bike",
            "models" => Components::getAllBrand()
        ] );
    }

    protected function store( BikeRequest $request ) {
        $request->validated();
        $new_bike = new $this->model( $request->input() );
        if ( $new_bike->makeSave( $request ) ) {
            $url="bikes/" . $new_bike->id;
            try {
                ( new MailHandler( asset( $url ),
                    'mails.new_bike',
                    auth()->user(),
                    $this->getBikeResource( $new_bike->id )) )->sendMail('k.oralkin@yandex.ru, hello@velovex.ru, shestpsov@gmail.com', 'New Bike' );
            } catch ( Exception $e ) {
                Log::info( $e->getMessage() );
            }
            return redirect( "bikes/" . $new_bike->id );
        }

        return redirect( "bikes/new" );
    }

    protected function edit( BikeRequest $request, $bike_id, $isPro ) {

        OtherHandler::setUrlReturn();
        $resource = $this->getBikeResource( $bike_id );
        if ( $resource ) {
            return view( 'pages/bike/edit', [
                "isPro"          => $isPro,
                "data"           => $this->getBikeResource( $bike_id ),
                "method"         => "PUT",
                "type_meta_info" => "bike",
                "class"          => "page-bike-edit",
                "models"         => Components::getAllBrand()
            ] );
        }

        return redirect( 404 );
    }

    protected function update( BikeRequest $request, $id ) {
        $request->validateWithIgnore( $id );
        $this->model::find( $id )->fill( $request->input() )->makeSave( $request );

        if($request->input('screen_size')&&$request->input('screen_size')<640){
            return redirect( 'bikes/' .$id.'/edit/0/#' );
        }
        return redirect( "bikes/" . $id );
    }


    protected function show( BikeRequest $request, $bike_id ) {
        global $request;
        OtherHandler::setUrlReturn();
        $resource = $this->getBikeResource( $bike_id );
        if ( $resource != null ) {
            $request["authorId"] = $resource["users_id"];

            $user = ( new UserResource( User::find( $resource["users_id"] ) ) )->toArray();

            if ( $resource["status"] == 2 ) {
                return view( 'pages/bike/search', [
                    "data"           => $resource,
                    "user"           => $user,
                    "type_meta_info" => "wanted_bike",
                    "class"          => "page-bike-in-search"
                ] );
            }

            return view( 'pages/bike/show', [
                "data"           => $this->getBikeResource( $bike_id ),
                "user"           => $user,
                "type_meta_info" => "bike",
                "class"          => "page-bike"
            ] );
        }

        return redirect( 404 );

    }

    protected function searchBike( BikeRequest $request, $bike_id ) {
        OtherHandler::setUrlReturn();
        $bike_resource = $this->getBikeResource( $bike_id );
        $user_resource = ( new UserResource( User::find( $bike_resource["users_id"] ) ) )->toArray();

        return view( 'pages/bike/search', [
            "data"           => $bike_resource,
            "user"           => $user_resource,
            "hide_info"      => false,
            "type_meta_info" => "wanted_bike"
        ] );
    }


    protected function delete( BikeRequest $request, $bike_id ) {

        UserBike::where( "bikes_id", $bike_id )->where( "users_id", auth()->user()->id )->delete();

        return redirect( "/my_bikes" );
    }


    protected function bikeInSearch( BikeRequest $request ) {
        global $request;
        OtherHandler::setUrlReturn();
        $request["isSearch"] = 1;
        $request["per_page"] = 10;
        $bike_resource       = UserBike::searchFilter();
        $bikes               = UserBikeResource::collection( $bike_resource["bikes"] )->toArray( $request );

        return view( 'pages/bike/result_in_search', [
            "data"           => ['bikes'=>$bikes,'count'=>$bike_resource["all_count"]],
            "hide_info"      => true,
            "class"          => "result-search",
            "type_meta_info" => "wanted"
        ] );
    }

    protected function myBikes( BikeRequest $request ) {
        global $request;
        OtherHandler::setUrlReturn();

        $request["author"]    = 1;
        $request["isDeleted"] = 1;
        $request["per_page"]  = - 1;

        $bike_resource = UserBike::searchFilter()["bikes"];

        return view( 'pages/bike/my_bikes', [
            "data"       => UserBikeResource::collection( $bike_resource )->toArray( null ),
            "isNotEmpty" => count( $bike_resource ) > 0,
            "class"      => "page-my-bike"
        ] );
    }

    protected function myBikeResult( BikeRequest $request ) {
        OtherHandler::setUrlReturn();
        $bike_info = UserBike::searchFilter();
        $bikes     = UserBikeResource::collection( $bike_info['bikes'] )->toArray( $request );

        $data = [
            "html"  => view( 'ajax/bikes_list', [ 'data' => $bikes, "all_count" => "" ] )->render(),
            "count" => $bike_info['all_count'],
        ];

        return view( 'pages/bike/find_bikes_by_params', [ "data" => $data, "class" => "result-search-category" ] );
    }

    private function getBikeResource( $bike_id ) {

        $model = UserBike::where( "bikes_id", $bike_id )->first();
        $res   = new UserBikeResource( $model );

        return $res->toArray();
    }


}
