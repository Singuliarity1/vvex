<?php


namespace App\Http\Controllers;


use App\Handlers\OtherHandler;
use App\Handlers\SearchHandler;
use App\Http\Resources\Base\BaseResource;
use App\Http\Resources\Bike\BikeResource;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\UserBike\UserBikeResource;
use App\Models\Bike;
use App\Models\Components;
use App\Models\User;
use App\Models\UserBike;
use Illuminate\Http\Request;

class PageController extends MainController {

    protected function check( Request $request ) {
        OtherHandler::setUrlReturn();
        $bike_res = SearchHandler::search();
        $serial   = $request->input( 'serial' ) != null ? $request->input( 'serial' ) : "";
        $search   = $serial != "";
        $class    = $search ? ( $search && $bike_res != null ? "isFind" : "isError" ) : "";

        return view( 'pages/text_page/check', array(
            "class"  => "page-check $class",
            "title"  => "Check",
            "data"   => $bike_res,
            "search" => $search,
            "serial" => $serial,
            "type_meta_info" => "check",
        ) );
    }

    protected function start_goal( Request $request ) {
        OtherHandler::setUrlReturn();
        $id_team = [ 106, 110, 105 ];

        return view( 'pages/text_page/start_goal', array(
            "title"          => "Start",
            "class"          => "page-start-goal",
            "type_meta_info" => "team",
            "users"          => UserResource::collection( User::whereIn( "id", $id_team )->get() )->toArray( null )
        ) );
    }

    protected function search( Request $request ) {
        $categories = Bike::leftJoin('bikes_categories',
            'bikes.bikes_categories_id','=','bikes_categories.id')
            ->leftJoin('users_bikes',
                'users_bikes.bikes_id','=','bikes.id')
                          ->select('bikes_categories.title')
                          ->whereNull('users_bikes.deleted_at')
                          ->groupBy("bikes.bikes_categories_id")
                          ->get('bikes_categories.title');

        return view( 'pages/text_page/search_by_category', array( "title" => "Search", "class" => "page-search","categories"=>$categories->toArray() ) );
    }

    protected function start_check( Request $request ) {
        return view( 'pages/text_page/start_check', array(
            "title"          => "Start Check",
            "class"          => "page-start-check",
            "type_meta_info" => "benefits"
        ) );
    }

    protected function not_found( Request $request ) {
        return view( 'errors/404', [
            "title"          => "Not Found",
            "class"          => "page-not_found",
        ] );
    }

    protected function main( Request $request ) {
        $request['isSearch'] = 1;
        OtherHandler::setUrlReturn();
        return view( 'pages/text_page/main', array(
            "title"        => "Main",
            "class"        => "page-main",
            "search_bikes" => UserBikeResource::collection( UserBike::searchFilter()["bikes"] )->toArray( null ),
            "like_bikes"   => UserBikeResource::collection( UserBike::where( "status", '=', 1 )->orderByRaw( 'RAND()' )->limit( 10 )->get() )->toArray( null ),
            "about_cards"  => array(
                ["id" => "01", "title" => "Проверка номера",
                "active_title" => "
                    У каждой велорамы свой <br/>
                    уникальный номер. Воспользуйся <br/>
                    поиском и узнай подробности. <br/>
                    База велосипедов пополняется постоянно. <div class='title-mobile'>У каждой велорамы свой <p></p>
                    уникальный номер. Просто введи <p></p> его в поиске и узнай <p></p> подробности. База постоянно пополняется.</div>",
                "img" => "layouts/build/img/pictures/about-main.png", "soon" => "", "url" => "/check"],
                ["id" => "02", "title" => "База велосипедов",
                "active_title" => "
                    Мы собрали самые популярные <br/>
                    и редкие категории. Создай подробную
                    карточку своего байка или даже целой <br/>
                    велоколлекции.",
                "img" => "layouts/build/img/pictures/about-main-2.png", "soon" => "", "url" => ""],
                ["id" => "03", "title" => "Страницы владельцев",
                "active_title" => "
                    Хочешь рассказать о своей страсти или новом сетапе? <br/>
                    Создавай аккаунт, наполняй его инфой и не забудь добавить <p></p> ссылки на соцсети.",
                "img" => "layouts/build/img/pictures/about-main-3.png", "soon" => "", "url" => ""],
                ["id" => "04", "title" => "Розыск велосипедов",
                "active_title" => "
                    VELOVEX помогает привлечь внимание в случае кражи — колокольчик <br/>
                    добавит байк в  «Розыск велосипедов». <br/> <p></p>
                    А шеринг стилизованной карточки в
                    соцсети расширить зону поиска.",
                "img" => "layouts/build/img/pictures/about-main-4.png", "soon" => "", "url" => "/wanted"],
                ["id" => "05", "title" => "Партнерские программы", "active_title" => "", "img" => "", "soon" => "Скоро", "url" => ""],
                ["id" => "06", "title" => "Календарь событий", "active_title" => "", "img" => "", "soon" => "Скоро", "url" => ""],
                ["id" => "07", "title" => "Прокат велосипедов", "active_title" => "", "img" => "", "soon" => "Скоро", "url" => ""],
        )
        ) );
    }

}
