<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;


class Authenticate extends Middleware {


    protected function redirectTo( $request ) {
        if ( ! $request->expectsJson() ) {
            Session::push( 'url_back',Crypt::encrypt( $request->url()) );
            return route( 'register' );
        }
    }


}
