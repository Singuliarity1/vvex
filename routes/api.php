<?php


use App\Http\Controllers\Api\BookmarkController;
use App\Http\Controllers\Api\ComponentController;
use App\Http\Controllers\Api\ImageController;
use App\Http\Controllers\Api\PersonalController;
use App\Http\Controllers\Api\SearchController;
use App\Http\Controllers\Api\UserBikeController;
use App\Http\Controllers\Api\UserBikeLikeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use \App\Http\Controllers\Api\SubscriptionController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource( 'users', AuthController::class );

Route::apiResource( 'subscribe', SubscriptionController::class );
Route::middleware( 'auth:api' )->get( '/user', function ( Request $request ) {
    return $request->user();
} );

Route::middleware( 'auth:api' )->put( 'user_bike', [ UserBikeController::class, "update" ] );
Route::middleware( 'auth:api' )->delete( 'user_bike', [ UserBikeController::class, "delete" ] );
Route::middleware( 'auth:api' )->post( 'bookmark', [ BookmarkController::class, "store" ] );
Route::middleware( 'auth:api' )->put( 'update_personal', [ PersonalController::class, "update" ] );
Route::middleware( 'auth:api' )->get( 'personal-info', [ PersonalController::class, "personal_info" ] );

Route::get( 'search', [ SearchController::class, "search" ] );
Route::post( 'like', [ UserBikeLikeController::class, "store" ] );
Route::get( 'like/statistic', [ UserBikeLikeController::class, "statistic" ] );
Route::get( 'sync/component', [ ComponentController::class, "sync" ] );
