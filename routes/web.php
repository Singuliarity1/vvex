<?php

use App\Http\Controllers\Api\BookmarkController;
use App\Http\Controllers\Api\SearchController;
use App\Http\Controllers\Api\UserBikeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\Api\UserBikeLikeController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BikeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 *
 * Authorization/Registration
 *
 * */
Route::get( '/login', function () {
    return view( 'pages/authorization/login', array( "type_meta_info" => "login" ) );
} )->name( 'login' );

Route::get( '/register', function () {
    return view( 'pages/authorization/authorization_index', array( "type_meta_info" => "register" ) );
} )->name( 'register' );

Route::get( '/register/email', function () {
    return view( 'pages/authorization/registration', array( "type_meta_info" => "register" ) );
} )->name( 'register_email' );

Route::get( '/register/create', function () {
    return view( 'pages/authorization/create', array( "type_meta_info" => "register" ) );
} )->name( 'register_create' );

Route::get( '/register/confirm', function () {
    return view( 'pages/authorization/confirm', array( "type_meta_info" => "register" ) );
} )->name( 'register_email' );

Route::get( '/register/confirmation', [ AuthController::class, "confirmation" ] )->name( 'confirmation' );


Route::get( '/logout', [ AuthController::class, "logout" ] );

Route::get( '/account/recover/', function () {
    return view( 'pages/authorization/restore', array( "type_meta_info" => "recover" ) );
} );

Route::get( '/account/recovered/', function () {
    return view( 'pages/authorization/restored', array( "type_meta_info" => "recover" ) );
} );

Route::post( '/register/email', [ AuthController::class, "registration" ] );
Route::post( '/login', [ AuthController::class, "login" ] );


/*
 *
 * Text page
 *
 * */
Route::get( '/check', [ PageController::class, "check" ] );


Route::get( '/', [ PageController::class, "main" ] );
Route::get( '/about', function () {
    return view( 'pages/text_page/about', array( "type_meta_info" => "about" ) );
} );
Route::get( '/about/benefits', [ PageController::class, "start_check" ] );
Route::get( '/about/team', [ PageController::class, "start_goal" ] );


Route::get( '/terms', function () {
    return view( 'pages/text_page/term', array( "type_meta_info" => "terms" ) );
} )->name( 'terms' );

/*
 *
 * Restore pass
 *
 * */
Route::get( '/account/recover/{token}', [ UserController::class, "restore_show" ] );
Route::post( '/account/recover/{token}', [ UserController::class, "restore_password" ] );
Route::post( '/account/recover', [ UserController::class, "restore" ] );

/*
 *
 * Social
 *
 * */
Route::get( '/redirect/{type}', [ SocialController::class, "redirectToProvider" ] );
Route::get( '/callback/{type}', [ SocialController::class, "handleProviderCallback" ] );


Route::get( '/search', [ PageController::class, "search" ] );
Route::get( '/wanted', [ BikeController::class, "bikeInSearch" ] );
Route::get( '/users/{user_id}', [ UserController::class, "profile" ] );

Route::group( [ 'middleware' => 'auth' ], function () {

    Route::get( '/register/create', [ UserController::class, "show_profile" ] )->name( 'register_create' );

    Route::post( '/register/create',  [ UserController::class, "create_profile" ] );

    Route::get( '/bikes/new', [ BikeController::class, "create" ] );
    Route::post( '/bikes/new', [ BikeController::class, "store" ] );
    Route::get( '/bikes/{bike_id}/edit/{isPro}', [ BikeController::class, "edit" ] );
    Route::put( '/bikes/{bike_id}/edit/{isPro}', [ BikeController::class, "update" ] );

    Route::delete( '/bikes/{bike_id}', [ BikeController::class, "delete" ] );

    Route::get( '/settings', [ UserController::class, "settings" ] )->name( "settings" );
    Route::put( '/settings', [ UserController::class, "saveSettings" ] );
    Route::delete( '/settings', [ UserController::class, "delete" ] );


    Route::get( '/my_bikes', [ BikeController::class, "myBikes" ] );


    Route::delete( '/bookmark/all', [ BookmarkController::class, "deleteAll" ] );
    Route::delete( '/bookmark/{bookmark_id}', [ BookmarkController::class, "delete" ] );


} );
Route::get( '/bikes/wanted/{bike_id}', [ BikeController::class, "searchBike" ] );
Route::get( '/bikes/{bike_id}', [ BikeController::class, "show" ] );
Route::get( '/my_bikes_result', [ BikeController::class, "myBikeResult" ] );
Route::any('{catchall}', [ PageController::class, "not_found" ])->where('catchall', '.*');
