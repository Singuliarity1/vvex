@extends('errors.main_error')
@section('content_error')
    <div class="page-notfound__title">То, что вы ищете уже устарело или не существует.</div>
    <div class="page-notfound__text-content">
        Не отчаивайтесь и попробуйте еще раз.<br/>
        Например <a href="{{asset("/about/")}}">почитайте о VELOVEX</a> или <a href="{{asset("/bikes/new")}}">добавьте</a><br/>
        свой байк в базу.
    </div>
@endsection
