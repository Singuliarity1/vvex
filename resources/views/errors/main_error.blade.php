@extends('layouts.index')
@section('content')


    <main class="page-notfound__content">
        <div class="loader">
            <div></div>
        </div>
        <div class="page-notfound__start">
            <div class="page-notfound__block">
                <div class="page-notfound__text">
                    @yield('content_error')

                </div>
                <div class="page-notfound__image">
                    <img src="{{asset("layouts/build/img/pictures/404-image.png")}}" alt="">
                </div>
            </div>
        </div>

    </main>

@endsection
