@extends('layouts.index')
@section('content')

<div class="page-settings__mobile-content">
    <div class="page-settings__label page-settings__mobile-account">
        <a href="../pages/account.html">
            <div class="page-settings__mobile-img">
                <img src="../img/pictures/user-photo.jpg" alt="">
            </div>
            <div class="page-settings__mobile-info">
                <p>Aleksei Melnik</p>
                <span>Ваше имя, фото, контакты и что-то еще</span>
            </div>
        </a>
    </div>
    <span class="page-settings__mobile-title">НАСТРОЙКИ</span>
    <div class="page-settings__label page-settings__mobile-password">
        <div>
            <a href="../pages/password.html">Пароль</a>
            <span>Не установлен</span>
        </div>
        <a href="../pages/my-page.html">Настройки страницы</a>
    </div>
    <span class="page-settings__mobile-title">АКТИВНОСТЬ</span>
    <div class="page-settings__label page-settings__mobile-bookmark">
        <a href="../pages/bookmark.html">Закладки</a>
    </div>
    <button class="page-settings__mobile-logout">Выйти из аккаунта</button>
</div>
</div>
@endsection
