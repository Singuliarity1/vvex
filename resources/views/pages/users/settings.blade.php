@inject('view', 'App\Services\BikeViewService')
@inject('user', 'App\Services\UserService')
@inject('image', 'App\Services\ImageService')
@inject('category', 'App\Services\BikeCategoryViewService')
@extends('layouts.index')
@section('content')
    @php(
	$arr = [
      'Января',
      'Февраля',
      'Марта',
      'Апреля',
      'Мая',
      'Июня',
      'Июля',
      'Августа',
      'Сентября',
      'Октября',
      'Ноября',
      'Декабря'
     ])
    @php(
     $time=strtotime($data["updated_at"])
    )

    <div class="loader">
        <div></div>
    </div>
    @if(!$data['name'])
        @include("parts.add_name")
    @endif
    <div class="mobile-content__title">
        <a href="#" class="mobile-content__back--link-back go-back">
            <span class="visually-hidden">Назад</span>
        </a>
        <span>Настройки</span>
    </div>
    <div class="page-settings__mobile mobile-content">
        <div class="page-settings__mobile-content">
            <span hidden class="page-settings__userId">{{$data["id"]}}</span>
            <div class="page-settings__label page-settings__mobile-account">
                <a href="#account">
                    <div class="page-settings__mobile-img">
                        <img src="{{$user->userImg()}}" alt="">
                    </div>
                    <div class="page-settings__mobile-info">
                        <p>{{$data["name"]}}</p>
                        <span>Ваше имя, фото, контакты и что-то еще</span>
                    </div>
                </a>
            </div>
            <span class="page-settings__mobile-title">НАСТРОЙКИ</span>
            <div class="page-settings__label page-settings__mobile-password">
                <div>
                    <a href="#password">Пароль</a>
                    @if(!Auth::user()->getAuthPassword())
                        <span>Не установлен</span>
                    @endif
                </div>
                <a href="#customization">Настройки страницы</a>
            </div>
            <span class="page-settings__mobile-title">АКТИВНОСТЬ</span>
            <div class="page-settings__label page-settings__mobile-bookmark">
                <a href="#activity">Закладки</a>
                <span class="count">{{count($data["bikes"])}}</span>
            </div>
            <a href="{{asset("/logout")}}" class="page-settings__mobile-logout">Выйти из аккаунта</a>
        </div>
    </div>


    <main class="page-settings__content content">
        <div class="page-settings__menu">
            <a href="#account" class="settings_links">Аккаунт</a>
            <a href="#password" class="settings_links">Пароль</a>
            <a href="#customization" class="settings_links">Настройки страницы</a>
            <a href="#activity" class="settings_links">Активность</a>
        </div>
        <div class="page-settings__wrapper">
            @foreach($errors->all() as $error)
                <div class="settings__form-message-error">
                    {{$error=='validation.unique'?'Такой пользователь уже существует':$error}}
                </div>
            @endforeach
            <div id="account" class="page-settings__bg page-settings__bg-1">
                <div class="page-settings__block page-settings__block-1">
                    <div class="page-settings__main">
                        <form method="POST" enctype="multipart/form-data" >
                            @csrf
                            {{method_field("PUT")}}
                            <div class="page-settings__main-block-1">
                                <div class="page-settings__user">
                                    <div class="page-settings__img">
                                        <img src="{{$user->userImg(null,"photo")}}" alt="photo">
                                        <label>
                                            <input type="file" class="pick-image" accept="image/*" name="photo">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="page-settings__info">
                                        <div class="page-settings__info-label">Ваше имя</div>
                                        <div class="page-settings__info-name">
                                            <span>{{$data["name"]}}</span>
                                            <button onclick="return false"><span
                                                    class="visually-hidden">Редактировать</span></button>
                                            <label>
                                                <input type="text" name="name" value="{{$data["name"]}}">
                                                <span class="save_name">Сохранить</span>
                                            </label>
                                            <div class="reset">Отменить</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page-settings__contacts-title">Контактные данные</div>
                                <div class="page-settings__contacts">
                                    <div class="page-settings__contacts-info">
                                        <div>
                                            <label>
                                                <span>Основной e-mail</span>
                                                <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$" id="user_email"
                                                       value="{{$data["email"]}}"
                                                       required  />
                                                </label>
                                            <span class="page-settings__email-trigger "
                                                  style="{{$data["email_secondary"]?"display:none":""}}">Оставить запасной e-mail
                                            </span>
                                        </div>
                                        <div class="page-settings__email-second {{$data["email_secondary"]?"active":""}}">
                                            <label>
                                                <span>Запасной e-mail</span>
                                                <input type="email" name="email_secondary"
                                                       value="{{idn_to_utf8($data["email_secondary"], IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46)}}">
                                            </label>
                                            <span class="reset">Отменить</span>
                                        </div>
                                        <label>
                                            <span>Телефон</span>
                                            <input type="tel" name="phone" value="{{$data["phone"]}}"
                                                   pattern="\+7\s?[\(]{0,1}[0-9]{3}[\)]{0,1}\s?\d{3}[-]{0,1}\d{2}[-]{0,1}\d{2}">

                                        </label>
                                        <label class="page-settings__location">
                                            <span>Местонахождение</span>
                                            <input type="text" name="location"
                                                   value="{{$data["country"]}}{{$data["city"]!=""?", ".$data["city"]:""}}">
                                        </label>
                                        <label class="page-settings__country">
                                            <span>Страна</span>
                                            <input type="text" name="country" value="{{$data["country"]}}">
                                        </label>
                                        <label class="page-settings__city">
                                            <span>Город</span>
                                            <input type="text" name="city" value="{{$data["city"]}}">
                                        </label>
                                    </div>
                                    <p class="mobile page-settings__personalization-btns">
                                        <span class="page-settings__personalization-reset">Сбросить</span>
                                        <button type="submit" class="btn-blue">Сохранить</button>
                                    </p>
                                    <div class="page-settings__contacts-text">
                                        Эту информацию видят только
                                        администраторы
                                    </div>
                                </div>
                            </div>
                            <div class="page-settings__main-block-2">
                                <span class="page-settings__main-agree-text mobile">
                                  Разрешите нам связаться с вами, если мы считаем, что ваш велосипед
                                  был украден, даже если он не отмечен вами как украденный.
                                </span>
                                <label>
                                    <input type="checkbox" value="1"
                                           {{$data["agreement"]==null ||$data["agreement"]==0?"":"checked"}} name="agreement">
                                    <span class="check"></span>
                                    <span class="page-settings__main-agree pc">
                                      Разрешите нам связаться с вами <strong>по телефону</strong>, если мы считаем, что ваш велосипед
                                      был украден, даже если он не отмечен вами как украденный.
                                    </span>
                                    <span class="page-settings__main-agree mobile">Разрешаю связаться со мной</span>
                                </label>


                            </div>
                            <div class="page-settings__main-block-3">
                                <p class="page-settings__personalization-btns">
                                    <span class="page-settings__personalization-reset">Сбросить</span>
                                    <button type="submit" class="btn-blue">Сохранить</button>
                                </p>
                                <div>
                                    Любое использование вашей личной информации регулируется условиями нашей <a
                                        href="{{asset("/agreement.pdf")}}" target="_blank">Политики
                                        конфиденциальности</a>. Если не хотите ее читать, тут простым языком — мы не
                                    продаем, не обмениваем и не сдаем в аренду вашу личную информацию. Мы такие же, как
                                    вы,
                                    фанаты велосипедов и хотим, чтобы наши отношения были доверительными.
                                </div>

                            </div>

                            <div class="page-settings__main-block-4">
                                <button onclick="return false">Удалить мой аккаунт</button>
                            </div>
                        </form>
                    </div>
                    <div class="page-settings__delete">
                        <div class="page-settings__delete-header">
                            Нам искренне жаль, что вы решили уйти.
                        </div>
                        <form class="page-settings__delete-form" method="POST">
                            @csrf
                            {{ method_field('DELETE') }}
                            <div class="page-settings__delete-title">Пожалуйста, расскажите нам о причине вашего ухода
                            </div>
                            <label>
                                <input type="radio" name="delete" checked="">
                                <span class="check"></span>
                                <span>Меня беспокоит безопасность моих данных</span>
                            </label>
                            <label>
                                <input type="radio" name="delete">
                                <span class="check"></span>
                                <span>VELOVEX отнимает слишком много времени</span>
                            </label>
                            <label>
                                <input type="radio" name="delete">
                                <span class="check"></span>
                                <span>Я больше не интересуюсь велосипедами</span>
                            </label>
                            <label>
                                <input type="radio" name="delete">
                                <span class="check"></span>
                                <span>Другая причина</span>
                            </label>
                            <textarea name="" cols="30" rows="10"></textarea>
                            <div class="page-settings__delete-btns">
                                <a href="#!">Отменить</a>
                                <button class="btn-blue" type="submit">Удалить страницу</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="password" class="page-settings__bg page-settings__bg-2">
                <div class="page-settings__block page-settings__block-2">
                    <div class="page-settings__social">
                        <div class="page-settings__social-wrapper">
                            {{-- @foreach($errors->all() as $error)
                                <div class="settings__form-message-error">
                                        {{$error}}
                                </div>
                            @endforeach --}}
                            <div class="page-settings__social-title">Входить с другим аккаунтом</div>
                            <a href="{{asset("redirect/facebook")}}"
                               class="page-settings__social-btn page-settings__social-1 {{$user->getSocialAuthNow("facebook")?"page-settings__social-use":""}}">
                                <span><span>Войти с </span>Facebook</span>
                            </a>
                            <a href="{{asset("redirect/google")}}"
                               class="page-settings__social-btn page-settings__social-2 {{$user->getSocialAuthNow("google")?"page-settings__social-use":""}}">
                                <span><span>Войти с </span>Google</span>
                            </a>
                            <a href="{{asset("redirect/vkontakte")}}"
                               class="page-settings__social-btn page-settings__social-3 {{$user->getSocialAuthNow("vkontakte")?"page-settings__social-use":""}}">
                                <span><span>Войти с </span>VK</span>
                            </a>
                            <!-- <a href="#!"
                               class="page-settings__social-btn page-settings__social-4 {{$user->getSocialAuthNow("apple")?"page-settings__social-use":""}}">
                                <span><span>Регистрация с </span>Apple</span>
                            </a> -->
                        </div>
                    </div>
                    <div class="page-settings__password">
                        <div class="page-settings__password-title">Установить пароль</div>
                        <div class="page-settings__password-2 page-settings__password-active">
                            <div>
                                <input type="password" value="{{$user->getOriginalPass()}}" disabled="">
                                <button>Сменить пароль</button>
                            </div>
                            <span>Последнее изменение: {{date("d {$arr[date("n",$time)-1]} Y",$time)}}г.</span>
                        </div>
                        <form class="page-settings__password-3" method="POST">
                            @csrf
                            {{method_field("PUT")}}
                            <input type="hidden" value="1"
                                   {{$data["agreement"]==null ||$data["agreement"]==0?"":"checked"}} name="agreement">
                            <div class="page-settings__password-inputs">
                                <div class="page-settings__password-field">
                                    <label class="input__password">
                                        <span>Новый пароль</span>
                                        <input type="password" class="password" name="password" value="">
                                    </label>
                                    <label class="show__password">
                                        <input type="checkbox" class="showPassword"><span>Показать</span>
                                    </label>
                                </div>
                                <div class="page-settings__password-field">
                                    <label class="input__password">
                                        <span>Пароль еще раз</span>
                                        <input type="password" class="password" name="password_confirmation"
                                               value="">
                                    </label>
                                    <label class="show__password">
                                        <input type="checkbox" class="showPassword"><span>Показать</span>
                                    </label>
                                </div>
                            </div>
                            <div class="page-settings__password-btns">
                                <a href="#">Отменить</a>
                                <button type="submit" class="btn-blue">Сохранить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="customization" class="page-settings__bg page-settings__bg-3">
                <div class="page-settings__block page-settings__block-3">
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        {{method_field("PUT")}}
                        <input type="hidden" value="1"
                               {{$data["agreement"]==null ||$data["agreement"]==0?"":"checked"}} name="agreement">
                        <div class="page-settings__cover">
                            <div class="page-settings__cover-wrapper {{$data["bg"]==null?"notBg":""}}">
                                <div class="page-settings__title-wrapper">
                                    <div class="page-settings__title mobile">Добавить обложку страницы</div>
                                    <div class="page-settings__title pc">Обложка страницы</div>
                                    <span class="page-settings__link-defaultImg">Оставить стандартную</span>
                                </div>
                                <div class="page-settings__cover-img">
                                    <span class="notBg__title">Добавить обложку</span>

                                    <div style="background-image: url({{$data["bg"]!=null?($image->getImageSize($data["bg"],'cover') != 'default' ? $image->getImageSize($data["bg"],'cover') : '/layouts/build/img/pictures/profile-bg-new.png') : ''}})"></div>
                                    <label>
                                        <input class="pick-image" accept="image/*" type="file" name="bg">
                                        <span></span>
                                    </label>
                                </div>
                                <div class="page-settings__recommendation">Рекомендуем загружать в размере 711x124px
                                </div>
                            </div>
                        </div>
                        <div class="page-settings__links">
                            <div class="page-settings__links-wrapper">
                                <div class="page-settings__title">Показать ссылки на мои страницы в соцсетях</div>
                                <div class="page-settings__links-list">
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-1">Strava</span>
                                        <input type="text" name="strava_link" value="{{$data["strava_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-2">Instagram</span>
                                        <input type="text" name="insta_link"  value="{{$data["insta_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-3">Facebook</span>
                                        <input type="text" name="fb_link"  value="{{$data["fb_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-4">VK</span>
                                        <input type="text" name="vk_link" value="{{$data["vk_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-5">YouTube</span>
                                        <input type="text" name="yt_link" value="{{$data["yt_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                    <label class="page-settings__links-item">
                                        <span class="page-settings__links-6">Twitter</span>
                                        <input type="text" name="tw_link" value="{{$data["tw_link"]}}" placeholder="Укажите адрес">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="page-settings__personalization">
                            <div class="page-settings__personalization-wrapper">
                                <div class="page-settings__title">Персонализация страницы
                                </div>
                                <div class="page-settings__personalization-block">
                                    <div class="page-settings__personalization-left">
                  <span>
                    Название вашей страницы
                  </span>
                                        <p>
                                            Название указывается в заголовке сайта.
                                            По умолчанию название страницы
                                            это ваше имя указанное при регистрации.
                                        </p>
                                    </div>
                                    <div class="page-settings__personalization-right">
                                        <input type="text" name="page_title" value="{{$data["page_title"]}}">
                                    </div>
                                </div>
                                <div class="page-settings__personalization-block">
                                    <div class="page-settings__personalization-left">
                                          <span>
                                            Расскажите немного о себе
                                          </span>
                                        <p>
                                            Этот текст будет размещен под вашим никнеймом.
                                        </p>
                                    </div>
                                    <div class="page-settings__personalization-right">
                                        <textarea name="about" maxlength="100">{{$data["about"]}}</textarea>
                                        <div><span class="count">0</span> / 100</div>
                                    </div>
                                </div>
                                <div class="page-settings__personalization-img">
                                    <div>

                                        <img src="{{$user->userImg($data["id"])}}"
                                             alt="">
                                    </div>
                                    <label>
                                        <input type="file" class="pick-image" accept="image/*" name="photo">
                                        <span>Изменить аватар страницы</span>
                                    </label>
                                </div>
                            </div>
                            <div class="page-settings__personalization-btns">
                                <a href="#!">Сбросить все</a>
                                <button type="submit" class="btn-blue">Сохранить</button>
                            </div>
                        </div>
                    </form>

                    <div class="page-settings__copy">
                        <div class="page-settings__copy-wrapper">
                            <span class="page-settings__copy-title">Ссылка на страницу</span>
                            <input value="{{asset("users/{$data["id"]}")}}"
                                   class="page-settings__copy-link visually-hidden" inputmode = "none"/>
                            <a class="page-settings__copy-text" href="{{asset("users/{$data["id"]}")}}">{{asset("users/{$data["id"]}")}}</a>
                            <span class="page-settings__copy-links">Скопировать ссылку</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="activity" class="page-settings__bg page-settings__bg-4">
                <div class="page-settings__block page-settings__block-4">
                    <div class="page-settings__bookmark">
                        <span>Закладки</span>
                    </div>
                    <div class="page-settings__bookmark-content {{count($data["bikes"])>0?"":"isEmpty"}}">
                        <form id="delete_all_bikes" method="POST">
                            @csrf
                            {{method_field("DELETE")}}
                            <a href="#!" class="page-settings__bookmark-delete {{count($data["bikes"])>1?"":"hidden"}}">Удалить все</a>
                        </form>
                        <ul>
                            @if($data["bikes"]!=null)
                                @foreach($data["bikes"] as $bike)
                                    <li>
                                        @include('parts.bike_form')
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
