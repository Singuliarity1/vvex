@inject('view', 'App\Services\BikeViewService')
@inject('user', 'App\Services\UserService')
@inject('category', 'App\Services\BikeCategoryViewService')
@extends('layouts.index')
@section('content')

    <main class="page-profile__content content">
        <div class="page-profile__content-header">
            <div class="page-profile__background

                @if(!isset($data["bg"]["path"]) || (isset($data["bg"]["path"]) && $data["bg"]["path"]==="default")) page-profile__background--default @endif"
                style="background-image: url({{($data["bg"]==null ?
                       $user->userImg($data["id"],"bg") : $data["bg"]["path"] === "default") ?
                      asset("/layouts/build/img/pictures/bike-bg_2.svg") : $data["bg"]["path"]}});">
            </div>
            <div class="page-profile__wrapper">
                <div class="page-profile__block">
                    <div class="page-profile__info">
                        <div class="page-profile__img">

                            <img src="{{$user->userImg($data["id"])}}"
                                 alt="name">
                        </div>
                        <div>
                            <div class="page-profile__name">
                                {{$data["name"]}}
                            </div>
                            <div class="page-profile__location">
                                @if($data["country"])
                                    {{$data["country"]}}, {{$data["city"]}}
                                @endif
                            </div>
                            <div class="page-profile__description">
                                {{$data["about"]}}
                            </div>
                        </div>
                    </div>
                    <button class="page-profile__btn-bikes">Велосипеды</button>
                </div>

                @include('parts.social',["html"=>$user->socialButton($data)])

            </div>
        </div>
        <div class="page-profile__content-body">
            <div class="page-profile__content-mobile">
                <h3>Велосипеды</h3>
                <span>{{count($data["bikes"])}}</span>
            </div>
            <ul>
                @if($data!=null)
                    @foreach($data["bikes"] as $bike)
                        <li>
                            @include('parts.bike_card')
                        </li>
                    @endforeach
                @endif
            </ul>
            <a class="all-bikes" href="/my_bikes">Смотреть все велосипеды</a>
        </div>
    </main>


@endsection
