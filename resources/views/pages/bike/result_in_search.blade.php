@inject('view', 'App\Services\BikeViewService')
@inject('category', 'App\Services\BikeCategoryViewService')
@extends('layouts.index')
@section('content')

    <main class="content result-search__content {{$data["count"]?"":"isEmpty"}}">
        <div class="loader">
            <div></div>
        </div>
        <div class="result-search__empty">
            <div>
                <div class="result-search__empty_text">

                    <span>Чтобы добавить велосипед в розыск необходимо иметь аккаунт VELOVEX и карточку велосипеда. Если
                        аккаунт уже есть, просто нажмите колокольчик <span class="small_bell"></span>
                    в карточке велосипеда
                    </span>
                </div>
                <a href="{{asset("register")}}" class="btn-black">Создать аккаунт</a>
            </div>
        </div>
        <div class="result-search__attention">
            <div><span></span>
                <p>Пожалуйста свяжитесь с нами в <a href="https://t.me/velovex">телеграм-канале VELOVEX</a>, если у вас есть информация о местонахождении велосипеда или напишите на <a href="mailto:hello@velovex.ru" class="btn-copy-link-search">hello@velovex.ru</a>.</p>
            </div>
        </div>
        <div class="result-search__wrapper">
            <ul class="result-search__list">
                @if($data!=null && $data['bikes'])
                    @foreach($data['bikes'] as $bike)
                        <li>
                            @include('parts.bike_card',["hide_white"=>true])
                        </li>
                    @endforeach
                @endif


            </ul>
            @include('parts.add_more')

        </div>
    </main>

@endsection
