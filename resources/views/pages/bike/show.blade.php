@inject('view', 'App\Services\BikeViewService')
@extends('layouts.index')
@section('content')
    <main class="page-bike__content content">
        @include('parts.bike_slider_fullscreen')
        <div class="page-bike__content-wrapper">
            @include('parts.bike_show',["view"=>$view])
        </div>
    </main>

@endsection
