@inject('view', 'App\Services\BikeViewService')
@extends('layouts.index')
@section('content')
    <main class="page-bike__content content">
        @include('parts.bike_slider_fullscreen')
        <div class="page-bike__content-wrapper">
            <div class="page-bike__attention">
                <div class="page-bike__attention-text">
                    Велосипед в розыске!
                    <div id="hint-wanted" class="hint__wrapper">
                        <div class="hint__trigger">
                            <img src="{{asset("layouts/build/img/icons/hint-icon-white.svg")}}" alt="Подсказка">
                        </div>
                    </div>
                    <div class="hint__text hint-wanted hint-top-center">
                        <button type="button" class="hint__btn-close">
                            <span class="visually-hidden">Закрыть</span>
                        </button>
                        <span>
                            Владелец этого велосипеда объявил его в розыск.<br>
                            Если у вас есть информация о местонахождении велосипеда, пожалуйста
                            <a class="hint__link-bold" href="mailto:no-reply@velovex.ru">
                                свяжитесь с нами.
                            </a>
                        </span>
                    </div>
                </div>
                <a href="#!" class="page-bike__attention-close"></a>
            </div>


            @include('parts.bike_show')
        </div>
    </main>

@endsection
