@inject('view', 'App\Services\BikeViewService')
@extends('layouts.index')
@section('content')
    @php($last_index=substr($data["count"],-1))
    <main class="content result-category__content">
        <div class="result-category__wrapper">
            <div class="result-category__title">
                <span class="count">{{$data["count"]}}</span>
                результат{{($last_index==0 || $last_index>4)?'ов':($last_index==1?'':'а')}} по запросу <span
                    class="category">«{{$_GET['category'] ?? ($_GET['brand'] ?? '')}}»</span>
            </div>
            <ul class="result-search__list">
                @if($data["html"]!==null)
                    {!! $data["html"] !!}
                @endif
            </ul>
            @include('parts.add_more')
        </div>
    </main>

@endsection
