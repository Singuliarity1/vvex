@inject('view', 'App\Services\BikeViewService')
@inject('category', 'App\Services\BikeCategoryViewService')
@extends('layouts.index')
@section('content')
    <div class="loader">
        <div></div>
    </div>
    <main class="page-my-bike__content content {{$isNotEmpty?"isNotEmpty":"isEmpty"}}">
        <div class="page-my-bike__content-header">
            <h2 class="page-my-bike__content-title">
                Мои велосипеды
            </h2>

            <div class="page-my-bike__tabs">
                <label class="selected">
                    <input type="radio" name="bike_tabs" value="available" checked="">
                    <span>Доступные</span>
                </label>
                <label>
                    <input type="radio" name="bike_tabs" value="inSearch">
                    <span>В розыске</span>
                </label>
                <label>
                    <input type="radio" name="bike_tabs" value="isDeleted">
                    <span>Удаленные</span>
                </label>
            </div>
        </div>

        <div class="page-my-bike__tabs-content available">

            <div class="page-my-bike__tabs-wrapper" style="display: {{$isNotEmpty?"block":"none"}}">
                <div class="page-my-bike__tabs-cards page-my-bike__tabs-cards-all ">
                    <!-- <div class="page-my-bike__sorting">
                        <label class="page-my-bike__sorting-btn">
                            <input type="checkbox">
                            <span>Сортировать</span>
                        </label>
                    </div> -->
                    <div class="sorting">
                        @if($data!=null)
                            @foreach($data as $bike)
                                @if($bike)
                                    @include('parts.bike_card',['bike'=>$bike])
                                @endif
                            @endforeach
                        @endif
                    </div>
                    <ul class="page-my-bike__list">
                    </ul>
                </div>

            </div>

            <div class="page-my-bike__tabs-add">
                <a href="{{asset("/bikes/new")}}">
                    <button>Добавить велосипед</button>
                </a>
            </div>
        </div>

    </main>

@endsection
