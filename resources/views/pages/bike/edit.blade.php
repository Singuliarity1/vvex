@inject('view', 'App\Services\BikeViewService')
@inject('category', 'App\Services\BikeCategoryViewService')
@extends('layouts.index')
@section('content')
    @php(	$main_params=[
                        "brand"=>["items"=>$models["brand"]],
                        "size"=>["items"=>$models["size"]],
                        "material"=>["items"=>$models["material"]]]
                        )
    @php(
      $group_set=[
              ["title"=>"Группа оборудования","slug"=>"equipment_group","items"=>$models["equipment_group"]],
              ["title"=>"Педали","slug"=>"pedals","items"=>$models["pedals"]],
              ["title"=>"Каретка","slug"=>"carriage","items"=>$models["carriage"]],
              ["title"=>"Кассета","slug"=>"cassette","items"=>$models["cassette"]],
              ["title"=>"Планетарная втулка","slug"=>"planetary_hub","items"=>$models["planetary_hub"]],
              ["title"=>"Сингспид","slug"=>"singspeed","items"=>$models["singspeed"]],
              ["title"=>"Шатуны","slug"=>"connecting_rods","items"=>$models["connecting_rods"]],
              ["title"=>"Передняя звезда","slug"=>"chainring","items"=>$models["chainring"]],
              ["title"=>"Цепь","slug"=>"chain","items"=>$models["chain"]]
          ]
  )

    @php(
       $shifters_switch=[
               ["title"=>"Шифтеры","slug"=>"shifters","items"=>$models["shifters"]],
               ["title"=>"Пер. переключатель","slug"=>"front_switch","items"=>$models["front_switch"]],
               ["title"=>"Задний переключатель","slug"=>"rear_switch","items"=>$models["rear_switch"]],
               ["title"=>"Ролики переключателя","slug"=>"switch_rollers","items"=>$models["switch_rollers"]],
           ]
   )

    @php(
  $control=[
          ["title"=>"Аэробар","slug"=>"aerobar","items"=>$models["aerobar"]],
          ["title"=>"Руль","slug"=>"steering_wheel","items"=>$models["steering_wheel"]],
          ["title"=>"Грипсы","slug"=>"grips","items"=>$models["grips"]],
          ["title"=>"Обмотка руля","slug"=>"handlebar_winding","items"=>$models["handlebar_winding"]],
              ["title"=>"Рулевая колонка","slug"=>"steering_column","items"=>$models["steering_column"]],
              ["title"=>"Вынос руля","slug"=>"stem","items"=>$models["stem"]],
      ]
)

    @php(
$absorber_fork=[
      ["title"=>"Ригидная вилка","slug"=>"rigid_fork","items"=>$models["rigid_fork"]],
      ["title"=>'Передний амортизатор 26"',"slug"=>"shock_absorber_26","items"=>$models["shock_absorber_26"]],
      ["title"=>'Передний амортизатор 27.5"',"slug"=>"shock_absorber_27_5","items"=>$models["shock_absorber_27_5"]],
      ["title"=>'Передний амортизатор 29"',"slug"=>"shock_absorber_29","items"=>$models["shock_absorber_29"]],
          ["title"=>'Передний амортизатор 700C',"slug"=>"shock_absorber_700с","items"=>$models["shock_absorber_700с"]],
          ["title"=>'Пружина',"slug"=>"spring","items"=>$models["spring"]],
          ["title"=>'Задний амортизатор',"slug"=>"rear_shock_absorber","items"=>$models["rear_shock_absorber"]],
  ]
)


    @php(
$breakers=[
  ["title"=>"Ободные тормоза","slug"=>"rim_brakes","items"=>$models["rim_brakes"]],
  ["title"=>'Дисковый тормоз',"slug"=>"disc_brake","items"=>$models["disc_brake"]],
  ["title"=>'Тормозные ручки',"slug"=>"brake_levers","items"=>$models["brake_levers"]],
]
)

    @php(
$saddle_seatpost=[
["title"=>"Седло","slug"=>"saddle","items"=>$models["saddle"]],
["title"=>'Подседельный штырь',"slug"=>"seatpost","items"=>$models["seatpost"]],
["title"=>'Амортизированный подседельный штырь',"slug"=>"cushioned_seatpost","items"=>$models["cushioned_seatpost"]],
["title"=>'Телескопический подседельный штырь',"slug"=>"telescopic_seatpost","items"=>$models["telescopic_seatpost"]],
]
)

    @php(
$wheels_tires=[
["title"=>'Колеса 26"',"slug"=>"wheels_26","items"=>$models["wheels_26"]],
["title"=>'Колеса 27.5"',"slug"=>"wheels_27","items"=>$models["wheels_27_5"]],
["title"=>'Колеса 29"',"slug"=>"wheels_29","items"=>$models["wheels_29"]],
["title"=>'Колеса 700C',"slug"=>"wheels_700C","items"=>$models["wheels_700C"]],
["title"=>'Обода',"slug"=>"rims","items"=>$models["rims"]],
["title"=>'Шины',"slug"=>"tires","items"=>$models["tires"]],
["title"=>'Камера',"slug"=>"inner_tire","items"=>$models["inner_tire"]],
["title"=>'Втулка',"slug"=>"sleeve","items"=>$models["sleeve"]],
["title"=>'Барабан втулки',"slug"=>"drum_bushing","items"=>$models["drum_bushing"]],
]
)

    @php(
$power=[
["title"=>'Измеритель мощности (шатуны)',"slug"=>"power_connecting_rods","items"=>$models["power_connecting_rods"]],
["title"=>'Измеритель мощности (педали)',"slug"=>"power_pedals","items"=>$models["power_pedals"]],
]
)
@php($exploded_url = explode( "#", $_SERVER['REQUEST_URI'] ) )
@php($hash = $exploded_url[1]??null )

    @php($isPro=isset($data["bike"])?$view->isPro($data["bike"]):false)
    @php($mainClass=isset($data["bike"])? isset($data["bike"]) && !$isPro?'is-pro':'is-basic':'is-new')

    <main
        class="page-bike-edit__content content @if(!isset($data)) popup-active @else bike-edit__mobile-steps @endif {{$mainClass}}">
        <div class="loader">
            <div></div>
        </div>
        <div class="page-bike-edit__menu edit-menu">
            {!! $category->getCategory(isset($data)?$data["bike"]["bike_category"]:null) !!}
            <a href="#basic" class="edit-menu__about-bike">О велосипеде </a>
            <a href="#components" class=" @if(!$isPro) hide-pro @endif edit-menu__about-bike hide_mobile">Компоненты </a>

        </div>

        <a href="#steps" class="btn-top btn-white go"></a>
        <div id="steps" class="page-bike-edit__steps">
            <div class="edit-form__basic-title  edit-form__title">
                <a href="{{isset($data)?asset('/bikes/'.$data["bike"]['id']):Session::get( 'link_back' )}}" class="btn-left btn-mobile-white"></a>
                <span>Редактировать</span>
            </div>
            <div class="edit__steps-item edit__steps-content">
                <div class="edit__steps-item edit__steps-basic" data-step="basic">
                    <p>О велосипеде</p>
                    <p>Модель, рама, цвет, местонахождение</p>
                </div>
                <div class="edit__steps-item edit__steps-gallery" data-step="gallery">
                    <p>Фотогалерея</p>
                    <span class="edit__steps-gallery-content">{!! $view->allMobileImages(isset($data)?$data["bike"]["bike_files"]:null)!!}</span>
                </div>
                <!-- <div class="edit__steps-item edit__steps-notif"></div> -->
                <div class="edit__steps-item edit__steps-components" data-step="components">
                    <p>Компоненты</p>
                    <p>Модель, рама, цвет, местонахождение</p>
                </div>
            </div>
        </div>

        <form class="page-bike-edit__form edit-form" action="" method="POST" enctype="multipart/form-data">
            {{ $method=="PUT"?method_field($method):"" }}
            @csrf
            <input type="hidden" name="screen_size" id="screen_size" value="">
            <a id="basic" class="visually-hidden"></a>

            @if(!isset($data))

                <div class="page-bike-edit__notif page-bike-edit__notif--basic">
                    <div class="page-bike-edit__notif-title">Чтобы добавить велосипед в базу VELOVEX, заполните все поля
                        ниже и получите такой значок
                    </div>
                    <img src="{{asset("layouts/build/img/pictures/velocode_badge-sm.svg")}}" alt=""
                         class="page-bike-edit__notif-img">
                </div>
            @endif
            <div class="edit-form__basic edit-form__fieldset  @if(!$isPro) no-rounded-top @endif" data-menu="basic">
                <div class="edit-form__basic-title  edit-form__title about-bike">
                    <a href="#" class="mobile-content__back">
                        <span class="visually-hidden">Назад</span>
                    </a>
                    <span>О велосипеде</span>
                </div>
                <div class="edit-form__field edit-form__field-input">
                    <div class="edit-form__field-title edit-form__field-number">
                        Номер рамы
                    </div>
                    <div class="hint__text hint-about hint-bottom-center">
                        <button type="button" class="hint__btn-close">
                            <span class="visually-hidden">Закрыть</span>
                        </button>
                        <span>
                        Только владельцы, которые указали серийный номер в карточке велосипеда получают
                        значок VELOVEX.<br> А еще наличие номера увеличивает шансы найти велосипед в случае
                        кражи.
                        <a target="_blank" class="hint__link-blue" href="{{asset("/check#find")}}">Как мне найти
                            серийный
                            номер?
                        </a>
                    </span>
                    </div>
                    <label class="
                        @if($errors->has('serial'))
                        invalid
@endif">
                        <input class="required-form__field" autocomplete="off" type="text" name="serial"
                               placeholder="не указан"
                               value="{{isset($data)?$view->getFieldFromData($data["bike"],"serial"):""}}">
                        <span class="required-form__field-clear">Изменить</span>
                    </label>
                </div>
                <div id="hint-about" class="edit-form__field  edit-form__field-about">
                    <a href="{{asset("/check#find")}}" target="_blank" class="">Где найти номер</a>
                </div>

                <div class="edit-form__field edit-form__field-select">
                    <div class="edit-form__field-title">Производитель</div>
                    <select class="page__edit-select required-form__field part_select brand" name="brand">
                        <option value="" disabled selected>не указан</option>
                        @include("parts.options_bike",["items"=>$main_params["brand"]["items"],"value"=>isset($data)?$view->getFieldFromData($data["bike"],"brand"):""])
                    </select>
                </div>
                <div class="edit-form__field edit-form__field-input">
                    <div class="edit-form__field-title">
                        Модель
                    </div>
                    <label class="
                            @if($errors->has('model'))
                        invalid
@endif">
                        <input class="required-form__field" type="text" autocomplete="off" type="text" name="model"
                               placeholder="не указана"
                               value="{{isset($data)?$view->getFieldFromData($data["bike"],"model"):""}}">
                        <span class="required-form__field-clear">Изменить</span>
                    </label>
                </div>
                <div class="edit-form__field edit-form__field-input">
                    <div class="edit-form__field-title">Год выпуска</div>
                    <label class="
                            @if($errors->has('year'))
                        invalid
@endif">
                        <input class="required-form__field" type="text" autocomplete="off" name="year"
                               placeholder="не указан"
                               value="{{isset($data)?$view->getFieldFromData($data["bike"],"year"):""}}">
                        <span class="required-form__field-clear">Изменить</span>
                    </label>
                </div>
                <div class="edit-form__field edit-form__color edit-form__field-checkbox">
                    <div class="edit-form__field-title">Цвет рамы</div>
                    <div class="edit-form__color-description">
                        Вы можете выбрать один или несколько цветов
                    </div>
                    <div class="edit-form__field-wrapper">
                        {!!  $view->allColors(isset($data)?$data["bike"]["color"]:null)!!}

                    </div>
                </div>

                <div class="edit-form__field  edit-form__field-select disable-select2-input">
                    <div class="edit-form__field-title">Материал рамы</div>
                    <select class="page__edit-select required-form__field part_select material material--first-child"
                            name="material">
                        <option value="" disabled selected>не выбран</option>
                        @include("parts.options_bike",["items"=>$main_params["material"]["items"],"value"=>isset($data)?$view->getFieldFromData($data["bike"],"material"):""])
                    </select>
                </div>
                <div class="edit-form__field  edit-form__field-select select__form-size">
                    <div class="edit-form__field-title">Размер рамы</div>
                    <select class="page__edit-select required-form__field part_select material material--second-child" name="size">
                        <option value="" disabled selected>не выбран</option>
                        @include("parts.options_bike",["items"=>$main_params["size"]["items"],"value"=>isset($data)?$view->getFieldFromData($data["bike"],"size"):""])
                    </select>
                    <div class="edit-form__field-input">
                        <label class="@if($errors->has('year'))
                            invalid
@endif">
                            <input class="required-form__field required-form__sizeInput" type="text" autocomplete="off"
                                placeholder="Введите размер" value="{{isset($data)?$view->getFieldFromData($data["bike"],"size"):""}}">
                            <span class="required-form__field-clear">Изменить</span>
                        </label>
                    </div>
                </div>
                <div class="edit-form__field  edit-form__field-size">
                    <span class="select-size__trigger">или введите свой</span>
                </div>
                <a href="#gallery" class="btn-continue btn-edit-phone btn-black go" type="button">Продолжить</a>
                <div id="gallery" class="edit-form__gallery" data-menu="gallery">
                    <div class="edit-form__gallery-title edit-form__title">
                        <a href="#basic" class="mobile-content__back">
                            <span class="visually-hidden">Назад</span>
                        </a>
                        <span>Фотогалерея</span>
                    </div>
                    <div class="edit-form__gallery-block">
                        <div class="edit-form__gallery-wrapper">
                            {!! $view->allImages(isset($data)?$data["bike"]["bike_files"]:null)!!}

                        </div>
                        <div class="edit-form__gallery-text">
                            <p>Добавьте не более 10 шт.</p>
                        </div>
                    </div>
                    <div class="edit-form__gallery-mobile-block">
                        <div>
                            Добавьте не более 10 шт.
                        </div>
                        <div class="edit-form__gallery-mobile-wrapper">
                            {!! $view->allMobileImages(isset($data)?$data["bike"]["bike_files"]:null)!!}
                            <label>
                                <input class="visually-hidden pick-image-hide" accept="image/*"
                                       type="file" >
                                <span class="big_icon"></span>
                            </label>
                        </div>
                        @if(isset($data))
                            <button id="gallery-save" onclick="this.form.submitted=this.value;" class="btn-black btn-edit-phone hidden" type="submit">Сохранить</button>
                        @endif
                        @if(!isset($data))
                            <button class="btn-black btn-edit-phone go" onclick="this.form.submitted=this.value;" type="submit" value="toBase2">Добавить в базу VELOVEX</button>
                        @endif
                    </div>

                    <div class="edit-form-container hide_desktop">

                        @if(isset($data) && !$data["bike"]["bike_files"])
                            <button class="edit-form__btn-submit-basic" onclick="this.form.submitted=this.value;" type="submit" value="saveBasic">Сохранить</button>
                        @endif
                        @if(!isset($data))
                            <button class="edit-form__btn-submit-basic" onclick="this.form.submitted=this.value;" type="submit" value="toBase1">Добавить в базу VELOVEX</button>
                        @endif
                    </div>
                </div>
            </div>
            </div>
            <div class="edit-form__location edit-form__fieldset" data-menu="location">
                <div class="edit-form__location-wrapper">
                    <div class="edit-form__field edit-form__components-header">
                        <div class="edit-form__components-title">Где сейчас находится ваш велосипед</div>
                    </div>
                    <div class="edit-location__top">
                        <span class="edit-location__top-title">Местонахождение</span>
                        <input type="text" name="location"
                               value="{{isset($data)?$view->getFieldFromData($data["bike"],"location"):""}}"
                               class="edit-location__top-value">
                    </div>
                    <div class="edit-location__bottom" style="padding-right: 0; margin-bottom: 0">
                        <label>
                            Не показывать эту информацию на странице велосипеда
                            <input type="checkbox" value=1
                                   {{isset($data) && $data["privacy"]==1?"checked":""}} name="privacy">
                            <span></span>
                        </label>
                    </div>
                </div>
                @if(isset($data))
                    <button class="edit-form__btn-submit-basic" onclick="this.form.submitted=this.value;" type="submit" value="saveBasic">Сохранить</button>
                @endif
                @if(!isset($data))
                    <button class="edit-form__btn-submit-basic" onclick="this.form.submitted=this.value;" type="submit" value="toBase1">Добавить в базу VELOVEX</button>
                @endif
            </div>
            <a id="components" class="visually-hidden"></a>
            @if(isset($data) && !$isPro)

                <div class="page-bike-edit__notif page-bike-edit__notif--pro @if(!$isPro) hide-pro @endif active-components-block">
                    <div class="page-bike-edit__notif-title">Добавляйте компоненты вашего велосипеда и получайте значок PRO
                    </div>
                    <img src="{{asset("layouts/build/img/icons/velocode_badge_pro.svg")}}" alt=""
                         class="page-bike-edit__notif-img">
                </div>
            @endif
            @if(isset($data["bike"]))
            <div id="components" class="@if($isPro)) no-border @else hide-pro @endif  edit-form__components  edit-form__fieldset " data-menu="components">
                <div class="edit-form__basic-title edit-form__title">
                    <a href="#gallery" class="mobile-content__back">
                        <span class="visually-hidden">Назад</span>
                    </a>
                    <span>Компоненты</span>
                </div>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-1">
                        <div class="edit-form__components-title">Привод</div>
                    </div>

                    @foreach($view->sortArray($group_set) as $key=>$component)

                        @include("parts/bike_component_selector", ["data" => $group_set,'bike'=>$data["bike"]??null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-2">
                        <div class="edit-form__components-title">Шифтеры и переключатели</div>
                    </div>
                    @foreach($view->sortArray($shifters_switch) as $key=>$component)

                        @include("parts/bike_component_selector", ["data" => $shifters_switch,'bike'=>$data["bike"]??null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-3">
                        <div class="edit-form__components-title">Управление</div>
                    </div>
                    @foreach($view->sortArray($control) as $key=>$component)
                        @include("parts/bike_component_selector", ["data" => $control,'bike'=>$data["bike"]??null])
                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-4">
                        <div class="edit-form__components-title">Подвеска</div>
                    </div>
                    @foreach($view->sortArray($absorber_fork) as $key=>$component)

                        @include("parts/bike_component_selector", ["data" => $absorber_fork,'bike'=>$data["bike"]??null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-5">
                        <div class="edit-form__components-title">Тормоза / тормозные ручки</div>
                    </div>
                    @foreach($view->sortArray($breakers) as $key=>$component)

                        @include("parts/bike_component_selector", ["data" => $breakers,'bike'=>$data["bike"]??null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-6">
                        <div class="edit-form__components-title">Седло / подседельный штырь</div>
                    </div>
                    @foreach($view->sortArray($saddle_seatpost) as $key=>$component)
                        @include("parts/bike_component_selector", ["data" => $saddle_seatpost,'bike'=>$data["bike"]??null])
                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>
                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-7">
                        <div class="edit-form__components-title">Колеса / покрышки / камеры</div>
                    </div>
                    @foreach($view->sortArray($wheels_tires) as $key=>$component)

                        @include("parts/bike_component_selector", ["data" => $wheels_tires,'bike'=>$data["bike"]??null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                </div>

                <a id="vilset" class="visually-hidden"></a>
                <div class="edit-form__vilset edit-form__fieldset edit-form--components" data-menu="vilset">
                    <div class="edit-form__field edit-form__components-header component-content__item-8">
                        <div class="edit-form__components-title">Измеритель мощности</div>
                    </div>

                    @foreach($view->sortArray($power) as $key=>$component)
                        @include("parts/bike_component_selector", ["data" => $power,'bike'=>$data["bike"] ?? null])

                    @endforeach
                    <div class="edit-form--components-block">
                        <div id="component-add" class="edit-form--components-btn edit-form--components-btn-add btn-black">еще компонент
                        </div>
                        <div id="component-clear" class="edit-form--components-btn edit-form--components-btn-delete btn-black">
                            Очистить
                        </div>
                    </div>
                    <div class="edit-form-container">
                        <button class="edit-form__btn-submit btn-black"
                                onclick="this.form.submitted=this.value;" type="submit" value="Pro1">Сохранить PRO</button>
                    </div>
                </div>
            </div>
            @endif
            <a id="location" class="visually-hidden"></a>
            <div class="edit-form__location edit-form__fieldset display-pro @if(!$isPro) hide-pro @endif hide_mobile" data-menu="location">
                <div class="edit-form__location-wrapper">
                    <div class="edit-form-container">


                        @if(isset($data))
                            <button onclick="this.form.submitted=this.value;" class="btn-edit-phone btn-black go" type="submit">Сохранить</button>
                        @else
                            <a href="#gallery" class="btn-continue btn-edit-phone btn-black go" type="button">Продолжить</a>
                        @endif

                        <button class="edit-form__btn-submit-mobile btn-edit-phone btn-black" onclick="this.form.submitted=this.value;" type="submit" value="Pro2">
                            Сохранить PRO
                        </button>

                        <!-- <button class="edit-form__btn-cancel" type="button">Отменить</button> -->
                            @if (isset($data))
                              <a href="{{asset('bikes/'.$data["bike"]["id"])}}" class="edit-form__btn-cancel" type="button">Отменить</a>
                            @endif
                            <button class="edit-form__btn-submit btn-black" onclick="this.form.submitted=this.value;" type="submit" value="Pro3">
                                Сохранить PRO
                            </button>
                    </div>
                </div>
            </div>

            @if(isset($data))
                <button onclick="this.form.submitted=this.value;" class="save_category btn-black btn-category btn-edit-phone" type="submit">Сохранить</button>
            @else
                <a href="#basic" class="btn-continue btn-black btn-category btn-edit-phone go" type="button">
                    Следующий шаг
                </a>
            @endif


            <a href="#basic" class="btn-top btn-white go"></a>
            @if(isset($data))
                <div class="popup__pro  @if (!$isPro) hint-block-active @endif">
                    <div class="popup-pro__content">
                        <div class="popup-pro__center">
                            <div class="popup-pro__trigger"><p>Добавить компоненты <span>и получить значок PRO</span></p></div>
                            <img src="{{asset("layouts/build/img/icons/velocode_badge_pro.svg")}}" alt=""
                                 class="popup-pro-center-img">
                            <a href="#components"
                               class="btn-link btn-black">Добавить</a>
                            <img width="24" src="{{asset("layouts/build/img/icons/close_x1.svg")}}" alt="Закрыть"
                                 class="popup-pro-close hint-block__btn-close">
                        </div>
                    </div>
                </div>
            @endif
            <div class="popup__category">
                <div class="page-category__content content">
                    <button type="button" class="btn-left btn-white"></button>
                    <a href="#steps" class="btn-left btn-mobile-white @if(!isset($data)) go-back @else close_popup @endif"></a>
                    <div class="page-category__form">
                        <h2 class="page-category__title">
                            Выберите категорию велосипеда
                        </h2>
                        <h2 class="page-category__title--mobile">
                            Категория
                        </h2>
                        {!! $category->allCategories($data["bike"]["bike_category"]["id"] ?? null) !!}
                    </div>
                </div>
            </div>

            <div class="edit-form__gallery-popup page-bike__content-fullscreen">
                <div class="swiper-container bike-fullscreen">
                  <button class="btn-mobile-fullscreen"></button>
                  <!-- Additional required wrapper -->
                  <div class="swiper-wrapper">
                      @if(isset($data))
                        @foreach($data["bike"]["bike_files"] as $i => $slideImg)
                            <div class="swiper-slide"  data-key="{{$i}}">
                                <input class="visually-hidden pick-image-hide"  accept="image/*" data-key="{{$i}}" type="file">
                                <div class="bike-swiper__img bike-thumb__img">
                                    <img src="{{$slideImg["file"][ 'path' ] ?? ''}}" alt="">
                                </div>
                            </div>
                        @endforeach
                      @endif
                  </div>
                    <div class="slider_background">
                        <div class="swiper-pagination bike-fullscreen__swiper-pagination"></div>
                    </div>
                  <div class="btn-fullscreen-close"></div>
                  <button class="btn-fullscreen" id="slider"></button>
                  <div class="bike-fullscreen-btn-control">

                      <div class="bike-fullscreen-btn-delete">
                          <img src="{{asset('/layouts/build/img/icons/cart.svg')}}">
                          <p>Удалить</p>
                      </div>
                      <div class="bike-fullscreen-btn-repeat">
                          <img src="{{asset('/layouts/build/img/icons/arrows.svg')}}">
                          <p>Заменить</p>
                      </div>
                  </div>
                </div>
            </div>

        </form>
    </main>
@endsection
