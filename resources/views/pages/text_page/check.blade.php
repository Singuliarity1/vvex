@extends('layouts.index')
@section('content')

    <main class="page-check__content content {{$search?$class:""}}">
        <div class="loader">
            <div></div>
        </div>
        <div class="page-check__top">
            <h2 class="page-check__top-title">
                Проверьте номер<br>
                велосипеда по базе<br>
                VELOVEX
            </h2>
            <div class="page-check__top-img"><img src="{{asset("layouts/build/img/pictures/bike_icon-1.svg")}}" alt="bike_icon"></div>
        </div>
        <form class="page-check__search" method="GET">
            <label>
                <input type="text" class="page-check__search-input" name="serial" value="{{$serial}}" placeholder=" ">
                <span class="page-check__search-placeholder">Серийный номер рамы</span>
                <span class="page-check__search-error">Номер не найден</span>
            </label>
            <button type="submit" class="btn-black">Проверить</button>
            <a href="#find">Не можете найти номер?</a>
        </form>

            <div class="page-check__result">
                <h3 class="page-check__result-title">Номер найден:</h3>
                <div class="page-check__result-content">

                </div>
            </div>


            <div class="page-check__error">
                <div class="page-check__error-message">
                    <span>Упс!</span>
                    <div class="page-check__error-text">
                        Номера вашего велосипеда нет в базе VELOVEX. Проверьте номер и попробуйте еще раз.
                    </div>
                </div>
                <div class="page-check__error-info page-check__info">
                    <h3 class="page-check__info-title">
                        Возможно ваш велосипед<br>
                        еще не добавлен в базу VELOVEX
                    </h3>
                    <div class="page-check__info-text">
                        Добавьте его прямо сейчас и получите защитный значок VELOVEX.
                    </div>
                    <div class="page-check__info-btns">
                        <a href="{{asset("/bikes/new")}}" class="btn-black">Добавить</a>
                        <a href="{{asset("/about/benefits")}}" class="btn-black">Зачем мне это нужно?</a>
                    </div>
                </div>
            </div>

            <div class="page-check__find">
                <a name="find" class="visually-hidden"></a>
                <h3 class="page-check__find-title">Где найти серийный номер велосипеда</h3>
                <div class="page-check__find-description">
                    <p>Чаще всего номера размещают на нижней части рамы.</p>
                    <p>Это может быть наклейка с номером (у карбоновых рам) или выбитый номер.</p>
                    <p>Если все же вы не смогли найти номер, <a href="mailto:hello@velovex.ru">напишите нам</a>, указав
                        название
                        велосипеда.</p>
                </div>
                <div class="page-check__find-images">
                    <div class="page-check__find-image-1">
                        <img src="{{asset("layouts/build/img/pictures/bike_icon-2.svg")}}" alt="check">
                    </div>
                    <div class="page-check__find-image-2">
                        <img src="{{asset("layouts/build/img/pictures/check-image-1.jpg")}}" alt="check-image-1">
                        <img src="{{asset("layouts/build/img/pictures/check-image-2.jpg")}}" alt="check-image-2">
                    </div>
                </div>
            </div>

    </main>

@endsection
