@extends('layouts.index')
@section('content')

    <main class="page-main__content">
        <div class="loader">
            <div></div>
        </div>
        <div class="page-main__start">
            <div class="page-main__animation">
                <div class="page-main__photo-wrapper page-main__photo-wrapper-1">
                    <div class="page-main__photo page-main__photo-1"></div>
                    <div class="page-main__photo page-main__photo-2"></div>
                    <div class="page-main__photo page-main__photo-3"></div>
                    <div class="page-main__photo page-main__photo-4"></div>
                </div>
                <div class="page-main__photo-wrapper page-main__photo-wrapper-2">
                    <div class="page-main__photo page-main__photo-1"></div>
                    <div class="page-main__photo page-main__photo-2"></div>
                    <div class="page-main__photo page-main__photo-3"></div>
                    <div class="page-main__photo page-main__photo-4"></div>
                </div>
                <div class="page-main__photo-wrapper page-main__photo-wrapper-3">
                    <div class="page-main__photo page-main__photo-1"></div>
                    <div class="page-main__photo page-main__photo-2"></div>
                    <div class="page-main__photo page-main__photo-3"></div>
                    <div class="page-main__photo page-main__photo-4"></div>
                </div>
            </div>
            <div class="page-main__start-title">
        <span class="transp-text-black">
          Велосипед — <br>
          это не стиль<br>
          жизни.
        </span>
                Это жизнь.
            </div>
            <a href="#about" class="page-main__go btn-black go">Гоу!</a>
            <a href="#about" class="page-main__bottom go">
                <svg class="page-main__bottom-arrow" width="22" height="48" viewBox="0 0 22 48" fill="none"
                     xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M0.26367 37.0645L10.7256 47.5264L21.1875 37.0645L20.1611 36.038L11.4514 44.7477L11.4514 0.0483403L9.9998 0.0483403L9.99979 44.7477L1.29012 36.038L0.26367 37.0645Z"
                          fill="white"/>
                </svg>
            </a>
            <a href="{{asset('/check')}}" class="page-main__check btn-transp-icon"><span>Проверить номер</span></a>
        </div>
        <div class="page-main__about">
            <a id="about" class="visually-hidden"></a>
            <h2 class="page-main__about-title">
                Как тут все устроено
            </h2>
            <div class="page-main__about-description">
                Рассказываем на 7 карточках
            </div>

            <div class="swiper-container about-slider">
                <div class="swiper-wrapper">
                    @foreach($about_cards as $field)
                        <div class="swiper-slide @if($field["img"]) about-active-slide @endif
                            @if(!$field["img"]) about-active-slide--empty  @endif
                            @if($field["url"] === "/wanted") about-slider--main-slide-red @endif"
                            @if($field["url"]) href="{{asset($field["url"])}}" @endif >
                            <div class="about-slider__item">
                                <div class="about-slider__counter about-slider--active-id">{{$field["id"]}}</div>
                                <div class="about-slider__counter about-slider--active-counter">{{$field["title"]}}</div>
                                <div class="about-slider__title about-slider--active-title-big">{{$field["title"]}}</div>
                                <div class="about-slider__title about-slider--active-title">{{$field["active_title"]}}</div>
                                <div class="about-slider__more about-slider--active-img" style="background-image:url({{asset($field["img"])}})"></div>
                                <div class="about-slider__more about-slider--active-soon">{{$field["soon"]}}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <button class="slider-btn-prev about-slider__prev">
                    <span class="visually-hidden">Предыдущий</span>
                </button>
                <div class="swiper-pagination about-slider__pagination"></div>
                <button class="slider-btn-next about-slider__next">
                    <span class="visually-hidden">Следующий</span>
                </button>
            </div>

        </div>
        <div class="page-main__hall">
            <a id="hall" class="visually-hidden"></a>
            <h2 class="page-main__hall-title">
                Зал славы
            </h2>
            <div class="page-main__hall-description">
                Как вам этот экземпляр? Выглядит круто? Поддержите владельцев своей реакцией.
            </div>

            <div class="page-main__hall-slider">
                <!-- Slider main container -->
                <div class="hall-slider swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach($like_bikes as $like_bike)
                            @include('parts.swiper_slide')
                        @endforeach
                    </div>

                    <!-- If we need navigation buttons -->
                    <div class="slider-btn-prev hall-slider__prev">
                        <span class="visually-hidden">Предыдущий</span>
                    </div>
                    <!--<div class="swiper-pagination hall-slider__pagination"></div>-->
                    <div class="slider-btn-next hall-slider__next">
                        <span class="visually-hidden">Следующий</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="goal-link" id="about">
            <a href="{{asset("about/team")}}" class="check-link__wrapper">
                <h2 class="goal-link__title">
                    Кто мы и в чем наша цель
                </h2>
            </a>
        </div>
        @if(count($search_bikes)!=0)
            <div class="page-main__search">
                <div class="page-main__search-counter">
                    В розыске <a href="{{asset("/wanted")}}"><span>{{count($search_bikes)}}</span></a>
                </div>
                <div class="swiper-container search-slider">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        @foreach($search_bikes as $bike)
                            @include('parts.slide_search_card')
                        @endforeach
                        <div class="swiper-slide swiper-slide-last"></div>
                    </div>

                    <!-- If we need navigation buttons -->
                    @if(count($search_bikes) >= 4)
                    <button class="slider-btn-prev search-slider__prev">
                        <span class="visually-hidden">Предыдущий</span>
                    </button>
                    <button class="slider-btn-next search-slider__next">
                        <span class="visually-hidden">Следующий</span>
                    </button>
                    @endif
                </div>
                @if(count($search_bikes) >= 4)
                <a href="{{asset("/wanted")}}" class="page-main__search-btn btn-link btn-black">
                    Все объявления
                </a>
                @endif
            </div>
        @endif
        <div class="check-link">
            <a class="check-link__wrapper" href="{{asset("about/benefits")}}">
                <h2 class="check-link__title">
                    VELOVEX с проверкой<br> номера велосипеда
                </h2>
            </a>
        </div>
        @include('parts.subscribe')
    </main>

@endsection
