@extends('layouts.index')
@section('content')

  <main class="page-start__content content">
    <div class="content__left">
      <div class="content__left-img">
        <img src="{{asset("layouts/build/img/pictures/velocode_badge.svg")}}" alt="VELOVEX с проверкой номера велосипеда">
      </div>
      <h2 class="content__left-description">
                VELOVEX с проверкой<br> номера велосипеда
                                               </h2>
      <a href="{{asset("/about/benefits")}}" class="btn-link btn-black content__left-btn">Узнать</a>
    </div>
    <div class="content__right">
      <div class="content__right-text content__right-text--desk">
        Наша миссия<br> и кто мы
      </div>
      <div class="content__right-text content__right-text--mob">
        Кто мы и в чем наша<br> цель
      </div>
      <a href="{{asset("/about/team")}}" class="btn-link btn-orange content__right-btn">Узнать</a>
    </div>
  </main>

@endsection
