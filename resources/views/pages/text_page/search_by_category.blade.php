@extends('layouts.index')
@section('content')
    <main class="page-search__content content">
        <div class="loader">
            <div></div>
        </div>
        <div class="search-popup__wrapper">
            <div class="search-popup__header">
                <div class="search-popup__search-field">
                    <span></span>
                    <input autocomplete="off" class="search-popup__search-input" type="search" name="search" id="search"
                           value="{{$_GET['query']??''}}">
                           <div class="search-popup__search-clear">очистить</div>
                </div>
                <button class="search-popup__close go-back">
      <span class="visually-hidden">
        Закрыть
      </span>
                </button>
            </div>
            <button class="search-popup__btn">Найти</button>
            <div class="search-popup__content">
                <div class="search-popup__popular">
                    <h4 class="search-popup__popular-title">Популярные запросы</h4>
                    <ul class="search-popup__popular-links">
                        @foreach($categories as $category)
                            @if($category['title']!=null)
                                <li>
                                    <button class="btn-tag btn-black">{{$category['title']}}</button>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="search-popup__empty">Ничего не найдено</div>
                <div class="search-popup__result">
                    <h3 class="search-popup__result-title"><span class="result_count">0</span> <span class="result_count_text">результатов</span></h3>
                    <ul class="search-popup__items">

                    </ul>
                    <div class="search__footer">
                        <div class="footer__left">
                            <div class="footer__left-links">
                                <a href="#" class="footer__logo">
                                    <img src="{{asset("layouts/build/img/pictures/logo.svg")}}" alt="VELOVEX">
                                </a>
                                <ul class="footer__social">
                                    <li class="footer-social-link footer__social-fb">
                                        <a href="https://facebook.com" target="_blank" rel="noopener noreferrer">FB</a>
                                    </li>
                                    <li class="footer-social-link footer__social-vk">
                                        <a href="https://vk.com" target="_blank" rel="noopener noreferrer">VK</a>
                                    </li>
                                    <li class="footer-social-link footer__social-inst">
                                        <a href="https://instagram.com" target="_blank"
                                           rel="noopener noreferrer">INST</a>
                                    </li>
                                    <li class="footer-social-link footer__social-tg">
                                        <a href="https://t.me/velovex" target="_blank" rel="noopener noreferrer">Встречаемся тут</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="footer__right">
                            <ul class="footer__navigation">
                                <li>
                                    <a href="{{asset("/check")}}">Проверить номер</a>
                                </li>
                                <li>
                                    <a href="{{asset("/wanted")}}">Розыск велосипедов</a>
                                </li>
                                <li>
                                    <a href="{{asset("/about")}}">Что такое VELOVEX?</a>
                                </li>
                            </ul>
                            <div class="footer__bottom">
                                <div class="footer__partnership">
                                    <span>По вопросам сотрудничества:</span>
                                    <a href="mailto:hello@velovex.ru">hello@velovex.ru</a>
                                </div>
                                <div class="footer__info">
                                    <div class="footer__info-links">
                                        <a href="{{asset("/about")}}" target="_blank" class="footer__info-about">О
                                            проекте</a>
                                        <a href="{{asset("/agreement.pdf")}}" target="_blank">Пользовательское соглашение</a>
                                    </div>
                                    <span>©VELOVEX 2021</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="search-popup__footer">
                    <a href="/">
                        <img src="{{asset("layouts/build/img/pictures/logo.svg")}}" alt="velovex">
                    </a>
                </div>
    </main>
@endsection
