@extends('layouts.index')
@section('content')
    <main class="page-start-check__content check-content">
        <div class="check-content__header">
            <div class="check-content__header-img">
                <img src="{{asset("layouts/build/img//pictures/velocode_badge.svg")}}" alt="VELOVEX">
            </div>
            <div class="check-content__header-text">
                <h2 class="check-content__header-title">
                    <span class="transp-text-white">VELOVEX</span><br>
                    c проверкой<br>
                    номера
                </h2>
                <div class="check-content__header-description">
                    — это лучшая платформа для регистрации и поиска информации о велосипедах.
                </div>
            </div>
        </div>
        <ul class="check-content__list">
            <li class="check-content__item-1">
                <h3 class="check-content__list-title">
                    Защита
                </h3>
                <p class="check-content__list-text">
                    Мы создали VELOVEX, чтобы помочь вернуть ваш байк в случае кражи. Все, что требуется, - это
                    бесплатная
                    регистрация (займет не более 5 минут), и мы добавим информацию о вас и вашем велосипеде в базу, в
                    которой уже
                    зарегистрированы тысячи пользователей.
                </p>
            </li>
            <li class="check-content__item-2">
                <h3 class="check-content__list-title">
                    Выгода
                </h3>
                <p class="check-content__list-text check-content__list-text--desk">
                    Более того, VELOVEX — это партнерские программы с веломагазинами,
                    сервисами и мастерскими, клубами и другими организациями.
                    Вы всегда сможете найти что то интересное или выгодное.
                    Предложения постоянно обновляются.
                </p>
                <p class="check-content__list-text check-content__list-text--mob">
                    Более того VELOVEX это партнерские программы
                    с веломагазинами, сервисами и мастерскими, клубами и другими организациями. Вы сможете найти что то
                    интересное
                    и выгодное. Списки постоянно обновляются.
                </p>
            </li>
            <li class="check-content__item-3">
                <h3 class="check-content__list-title">
                    Удобство
                </h3>
                <p class="check-content__list-text">
                    Мы не забываем об удобстве и предлагаем быстрый способ поделиться карточкой вашего велосипеда с
                    другими. А еще
                    вы сможете быстро и безопасно передать права на велосипед в случае продажи.
                </p>
            </li>
            <li class="check-content__item-4">
                <h3 class="check-content__list-title">
                    Возможности
                </h3>
                <p class="check-content__list-text">
                    Не ждите — добавьте свой велосипед в базу
                    и начните использовать все инструменты
                    и возможности VELOVEX.
                </p>
                <a href="{{asset("bikes/new")}}" class="btn-link btn-orange">Добавить</a>
            </li>
        </ul>

        <div class="goal-link">
            <a href="{{asset("about/team")}}" class="check-link__wrapper">
                <h2 class="goal-link__title">
                    Кто мы и в чем наша цель
                </h2>
            </a>
        </div>

        @include('parts.subscribe')
    </main>
@endsection
