@extends('layouts.index')
@section('content')

    <main class="page-start-goal__content">
        <div class="page-start-goal__content-wrapper">
            <div class="page-start-goal__content-text">
                <div class="page-start-goal__content-title">
                    <span class="transp-text-black">наша миссия —</span>
                    <div class="">сохранять любовь <br> к велосипеду</div>
                </div>
                <div class="page-start-goal__content-description">
                Для нас велосипед — это способ получить максимум в ощущении свободы и красоты окружающего мира.
                Велосипед — это свобода во всех измерениях.
                И не важно это поездка на работу или скоростной спуск с горы, кругосветка или первые километры.
                На велосипеде мы сами решаем, какой путь выбрать.
                </div>
            </div>
        </div>
        <div class="page-start-goal__gallery">
            <div class="page-start-goal__gallery-item-1">
                <img src="{{asset("layouts/build/img/pictures/goal-photo-1.jpg")}}" alt="фото 1">
            </div>
            <div class="page-start-goal__gallery-item-2">
                <img src="{{asset("layouts/build/img/pictures/goal-photo-2.jpg")}}" alt="фото 2">
            </div>
            <div class="page-start-goal__gallery-item-3">
                <img src="{{asset("layouts/build/img/pictures/goal-photo-3.jpg")}}" alt="фото 3">
            </div>
        </div>

        <div class="page-start-goal__team">
            <div class="page-start-goal__team-title">
                Команда VELOVEX
            </div>
            <div class="page-start-goal__team-description">
            Мы, создатели VELOVEX, без ума от велосипедов.
            Мы разные, но каждый из нас захотел принять участие в этом проекте,
            потому что это реальная возможность внести свой вклад в развитие велокультуры,
            укрепление добрых отношений с множеством вело сообществ и помощь тем,
            кому нужен профессиональный совет.
            Мы не ограничены количеством мест в команде — двери VELOVEX всегда открыты!
            </div>
            <div class="page-start-goal__team-container">
                <div class='page-start-goal__team-list'>

                    @if($users!=[])
                        @foreach($users as $user)
                            @include('parts.team_user')
                        @endforeach
                    @endif
                    <div class='page-start-goal__team-item'>
                       <div id="hint-start" class="hint__wrapper">
                          <div class="hint__trigger">
                            <div class="page-start-goal__team-card">
                              Хотите<br>
                              в команду
                              VELOVEX?
                            </div>
                          </div>
                          <div class="hint__text hint-start hint-center-scale">
                            <button type="button" class="hint__btn-close">
                              <span class="visually-hidden">Закрыть</span>
                            </button>
                            <span>
                              VELOVEX создают увлеченные велосипедами люди.
                              Если вы из таких и хотите помочь в развитиии
                              проекта — welcome! Напишите нам письмо и расскажите в нем о своем желании.
                              <button class="btn-link btn-orange btn-copy-link content__right-btn">hiteam@velovex.ru</button>
                            </span>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="check-link">
            <a href="{{asset("about/benefits")}}" class="check-link__wrapper">
                <h2 class="check-link__title">
                    VELOVEX с проверкой<br> номера велосипеда
                </h2>
            </a>
        </div>

        @include('parts.subscribe')


    </main>

@endsection
