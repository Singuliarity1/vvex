@extends('layouts.authorization')
@section('content')
    <div class="authorization__form authorization__form-7 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-content">
                Возвращайтесь к нам<br>
                перейдя по ссылке которую<br>
                мы вам отправили на почту
            </div>
        </div>
        <div class="authorization__form-info">
            <a href="{{asset("/")}}" class="authorization__link">А пока вернусь на главную</a>
        </div>
    </div>
@endsection
