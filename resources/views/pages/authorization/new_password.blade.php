@extends('layouts.authorization')
@section('content')
    <div class="authorization__form authorization__form-6 auth__form-active">
        <div class="authorization__form-block">
            <p class="authorization__form-text">
                Укажите новый пароль
            </p>
            <div class="authorization__form-content">
                <form action="" method="post">
                @csrf <!-- {{ csrf_field() }} -->
                    @foreach($errors->all() as $error)
                        <div class="authorization__form-message-error" style="display:block">
                            {{$error}}
                        </div>
                    @endforeach
                    <div class="authorization__field">
                        <label for="password">Новый пароль</label>
                        <input type="password" name="password" id="password">

                        <label for="password_confirmation">Подтверждение пароля</label>
                        <input type="password" name="password_confirmation" id="password_confirmation">
                    </div>
                    <button class="authorization__btn " type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
