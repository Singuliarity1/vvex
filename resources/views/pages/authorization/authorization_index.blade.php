@extends('layouts.authorization')
@section('content')
    <div class="authorization__form authorization__form-1 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-title">
                Прокатимся!
            </div>
            <div class="authorization__form-content">
                <ul class="authorization__social">
                    <li class="authorization__social-item authorization__social-item-1">
                        <a href="{{asset("redirect/facebook?".http_build_query($_GET))}}" class="authorization__social-btn btn-transp-half">
                            <span>Войти с Facebook</span>
                        </a>
                    </li>
                    <li class="authorization__social-item authorization__social-item-2">
                        <a href="{{asset("redirect/vkontakte?".http_build_query($_GET))}}"
                           class="authorization__social-btn btn-transp-half"><span>Войти с VK</span></a>
                    </li>
                    <li class="authorization__social-item authorization__social-item-3">
                        <a href="{{asset("redirect/google?".http_build_query($_GET))}}" class="authorization__social-btn btn-transp-half"><span>Войти с Google</span>
                        </a>
                    </li>
                    <li class="authorization__social-item authorization__social-item-4">
                        <button onclick="location.href='{{asset("/login".http_build_query($_GET))}}'"
                                class="authorization__social-btn btn-transp-half auth_handler-1">
                                <span>Войти по e-mail</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <!-- <div onclick="location.href='{{asset("register".http_build_query($_GET))}}'" class="authorization__form-info">
            <button class="authorization__link">У меня есть аккаунт</button>
        </div> -->
    </div>
@endsection
