@extends('layouts.authorization')
@section('content')
    <div class="authorization__form authorization__form-7 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-content">
                {{$message}} <br>
            </div>
        </div>
        @if($confirm)
            <div class="authorization__form-info com-in-block">
                <a href="{{asset("/settings")}}" class="authorization__link come-in">Войти в личный кабинет</a>
            </div>
        @else
            <div class="authorization__form-info">
                <a href="{{asset("/")}}" class="authorization__link">Пока вернусь на главную</a>
            </div>
        @endif
    </div>
@endsection
