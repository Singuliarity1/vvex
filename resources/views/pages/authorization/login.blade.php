@extends('layouts.authorization')
@section('content')


    <div class="authorization__form authorization__form-2 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-title">
                Прокатимся!
            </div>
            <div class="authorization__form-content">
                <form action="" method="post">
                    @csrf
                    @php($all_error=$errors->all())
                    @if(isset($error))
                        @php($all_error[]=$error)
                    @endif
                    @if($all_error)
                        @foreach($all_error as $error)
                            <div class="authorization__form-message-error" style="display:block">
                                {{$error}}
                            </div>
                        @endforeach
                    @endif
                    <div class="authorization__field">
                        <label for="user_email">Ваш e-mail</label>
                        <input type="email" name="email" required id="user_email">
                        <span class="user_error">E-mail не верный</span>
                    </div>
                    <div class="authorization__field">
                        <label for="user_password">Ваш пароль</label>
                        <input type="password" required name="password" id="user_password">
                    </div>

                    <button class="authorization__btn" type="submit">Войти</button>
                </form>
            </div>
            <a class="restore_pass" href="{{asset("/account/recover")}}">Восстановить пароль</a>
        </div>

        <div class="authorization__form-info">
            <p>
                Регистрируясь в VELOVEX, вы принимаете условия нашего
                <a href="{{asset("/agreement.pdf")}}">пользовательского соглашения</a>
            </p>
        </div>
    </div>
@endsection
