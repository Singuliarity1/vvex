@extends('layouts.authorization')
@section('content')


    <div class="authorization__form authorization__form-2 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-title">
                Создайте свой <br>аккаунт
            </div>
            <div class="authorization__form-content">
                <form action="{{asset("/register/email")}}" method="post">
                    @csrf
                    @foreach($errors->all() as $error)
                        <div class="authorization__form-message-error" style="display:block">
                            {{$error}}
                        </div>
                    @endforeach

                    <div class="authorization__field">
                        <label for="user_email">Ваш e-mail</label>
                        <input type="email" required name="email" id="user_email">
                        <span class="user_error">E-mail не верный</span>
                    </div>
                    <div class="authorization__field">
                        <label for="user_password">Ваш пароль</label>
                        <input type="password" required name="password" id="user_password">
                    </div>


                    <button class="authorization__btn" type="submit">Зарегистрироваться</button>
                </form>
            </div>
            <a class="login_pass" href="{{asset("/register")}}">У меня есть аккаунт</a>
        </div>
        <div class="authorization__form-info">
            <p>
                Регистрируясь в VELOVEX, вы принимаете условия нашего
                <a href="{{asset("/agreement.pdf")}}">пользовательского соглашения</a>
            </p>
        </div>
    </div>
@endsection
