@extends('layouts.authorization')
@section('content')
    <div class="authorization__form authorization__form-6 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-title">
                С нами не забудешь
            </div>
            <p class="authorization__form-text">
                Укажите e-mail, который вы используете для этого аккаунта
            </p>
            <div class="authorization__form-content">
                <form action="{{asset("/account/recover/")}}" method="post">
                    @csrf <!-- {{ csrf_field() }} -->
                        @foreach($errors->all() as $error)
                            <div class="authorization__form-message-error" style="display:block">
                                {{$error}}
                            </div>
                        @endforeach

                    <div class="authorization__field">
                        <label for="user_email">Ваш e-mail</label>
                        <input type="email" name="email" id="user_email">
                        <span class="user_error">E-mail не верный</span>
                    </div>
                    <button class="authorization__btn " type="submit">Отправить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
