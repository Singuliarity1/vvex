@extends('layouts.authorization')
@section('content')


    <div class="authorization__form authorization__form-3 auth__form-active">
        <div class="authorization__form-block">
            <div class="authorization__form-title">
                Создайте свой <br>аккаунт
            </div>
            <div class="authorization__form-content">
                <form action="" method="post">
                    @csrf

                    @if(isset($error))
                        <div class="authorization__form-message-error" style="display:block">
                            {{$error}}
                        </div>
                    @endif
                    <div class="authorization__field">
                        <label for="user_name">Ваше имя</label>
                        <input type="text" name="name" required id="user_name">
                    </div>
                    <div class="authorization__field">
                        <label for="user_surname">Ваша фамилия</label>
                        <input type="text" name="surname" required id="user_surname">
                    </div>


                    <button class="authorization__btn" type="submit">Продолжить</button>
                </form>
            </div>
        </div>

        <div class="authorization__form-info">

            <p>
                Регистрируясь в VELOVEX, вы принимаете условия нашего
                <a href="{{asset("/agreement.pdf")}}">пользовательского соглашения</a>
            </p>
        </div>
    </div>
@endsection
