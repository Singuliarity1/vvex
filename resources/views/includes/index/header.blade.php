@inject('user', 'App\Services\UserService')
@include('includes.base.header')
<?php
    $class_body=$class??"page-start";
    $white=strripos($class_body,"page-start-check")!==false?"-white":"";
?>
<body class="{{$class_body}}">
<header class="header {{$user->isLogged()}}">
    <div class="header__navigation">
        <div class="header__location">
            <div class="header__logo">
                <a href="{{asset("/")}}"><img src="{{asset("layouts/build/img/pictures/logo{$white}.svg")}}" alt="Velovex"></a>
            </div>

            <a href="{{asset("/search")}}" class="header__search">
                <img src="{{asset("layouts/build/img/icons/search-icon{$white}.svg")}}" alt="Поиск">
            </a>
        </div>
        <nav class="header__navigation-wrapper">
            <button class="header__navigation-close">
                <span class="line"></span>
                <span class="visually-hidden">Close</span>
            </button>
            <ul>
                <li class="header__navigation-item">
                    <a href="{{asset("/check")}}">Проверить номер</a>
                </li>
                <li class="header__navigation-item">
                    <a href="{{asset("/wanted")}}">Розыск велосипедов</a>
                </li>
                <li class="header__navigation-item">
                    <a href="{{asset("/about")}}">Что такое <span>VELOVEX?</span></a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="header__user">
        <a href="{{asset("/search")}}" class="header__search">
            <img src="{{asset("layouts/build/img/icons/search-icon{$white}.svg")}}" alt="Поиск">
        </a>
        <a href="{{asset("/settings#activity")}}" class="header__btn-bookmark">
            <img src="{{asset("layouts/build/img/icons/bookmark-icon{$white}.svg")}}" alt="Закладка">
        </a>
        <a href="{{asset("/bikes/new")}}" class="header__btn-add">
            <img src="{{asset("layouts/build/img/icons/add-icon{$white}.svg")}}" alt="Добавить">
        </a>
        <div class="header__profile">
            <a href="{{asset("/register")}}" class="header__btn-login">
                Войти
            </a>
            <div class="header__profile-img">
                <img src="{{$user->userImg()}}" alt="">
            </div>
            <div class="header__profile-menu">
                <ul>
                    <li><div class="header__profile-link header__profile-link--close"><img src="{{asset("layouts/build/img/icons/close_x1.svg")}}">Закрыть</div></li>
                    <li><a href="{{Auth::check()?asset("/users")."/".auth()->user()->id:""}}" class="header__profile-link">Моя страница</a></li>
                    <li><a href="{{asset("/my_bikes")}}" class="header__profile-link">Мои велосипеды</a></li>
                    <li><a href="{{asset("/settings")}}" class="header__profile-link">Настройки аккаунта</a></li>
                    <li><a href="{{asset("/logout")}}" class="header__profile-logout">Выйти</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>
