<footer class="page-start__footer footer">
    <div class="footer__left">
        <div class="footer__left-links">
            <span class="footer__logo">
                <img src="{{asset("layouts/build/img/pictures/logo-white.svg")}}" alt="VELOVEX">
            </span>
            <ul class="footer__social">
                <li class="footer-social-link footer__social-fb">
                    <a href="https://facebook.com" target="_blank" rel="noopener noreferrer">FB</a>
                </li>
                <li class="footer-social-link footer__social-vk">
                    <a href="https://vk.com" target="_blank" rel="noopener noreferrer">VK</a>
                </li>
                <li class="footer-social-link footer__social-inst">
                    <a href="https://instagram.com" target="_blank" rel="noopener noreferrer">INST</a>
                </li>
                <li class="footer-social-link footer__social-tg">
                    <a href="https://t.me/velovex" target="_blank" rel="noopener noreferrer">Встречаемся тут</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer__right">
        <ul class="footer__navigation">
            <li>
                <a href="{{asset("/check")}}">Проверить номер</a>
            </li>
            <li>
                <a href="{{asset("/wanted")}}">Розыск велосипедов</a>
            </li>
            <li>
                <a href="{{asset("/about")}}">Что такое VELOVEX?</a>
            </li>
        </ul>
        <div class="footer__bottom">
            <div class="footer__partnership">
                <span>По вопросам сотрудничества:</span>
                <a href="mailto:hello@velovex.ru">hello@velovex.ru</a>
            </div>
            <div class="footer__info">
                <div class="footer__info-links">
                    <a href="{{asset("/about")}}" target="_blank" class="footer__info-about">О проекте</a>
                    <a href="{{asset("/agreement.pdf")}}" target="_blank">Пользовательское соглашение</a>
                </div>
                <span>©VELOVEX 2022</span>
            </div>
        </div>
    </div>
</footer>
<input type="hidden" id="user_id" value="{{\Illuminate\Support\Facades\Auth::check()?auth()->user()->id:""}}">
@include('includes.base.footer')
