@inject('ceo', 'App\Services\CeoService')
@php(
$type=isset($type_meta_info)?$type_meta_info:""
)
@php(
$meta=$ceo->showMeta($type,isset($data)?$data:null)
)
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/png" sizes="32×32" href="{!! asset("/layouts/build/img/favicon1.png") !!}">
    <link rel="icon" type="image/png" sizes="128×128" href="{!! asset("/layouts/build/img/favicon2.png") !!}">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <link rel="stylesheet" href="{!!  asset('/layouts/build/css/style.min.css') !!}"  onload="this.media='all'" />
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>{{$meta["title"]}}</title>
    <meta name="description" content="{{$meta["description"]}}"/>
    @if(isset($meta['property']))
        @foreach($meta['property'] as $key=>$property)
            <meta name="{{$key}}" content="{{$property}}" />
        @endforeach
    @endif
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            var z = null;m[i].l=1*new Date();
            for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(89951723, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/89951723" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>

