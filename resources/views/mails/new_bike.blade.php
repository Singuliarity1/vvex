<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Restore Pass</title>
</head>

<body
    style="width:100%!important;height:100%;background:#efefef;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">
<table cellspacing="0" cellpadding="0" class="body-wrap"
       style="border: none;width:100%!important;height:100%;background:#efefef;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">
    <tr>
        <td class="container"
            style="background:#ffffff;display:block!important;clear:both!important;margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;max-width:600px!important;">

            <table cellspacing="0" cellpadding="0" style="background:#ffffff;width:100%!important;">
                <tr>
                    <td align="left" class="masthead"
                        style="background-size:cover;background-repeat:no-repeat;padding-top:0px;padding-right:0px;padding-bottom:10px;padding-left:0px;height:200px;max-height:200px;background-size:cover;background-repeat:no-repeat;height:100%;pointer-events:none;">
                        <img src="{{asset('/')}}/layouts/build/img/pictures/letter-image.png" alt=""
                             style="max-width:100%;margin:0auto;display:block;">
                    </td>
                </tr>
                <tr>
                    <td class="content" style="padding-top:30px;padding-right:79px;padding-bottom:30px;padding-left:80px;">

                        <p
                            style="font-weight:700;font-size:16px;line-height:25px;letter-spacing:0.04em;margin-bottom:25px;color:#000000;">
                            Регистрация велосипеда
                        </p>
                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.04em;margin-bottom:25px;color:#000000;">
                            Владелец - {{$user['name']}}
                        </p>
                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.04em;margin-bottom:25px;color:#000000;">
                            Бренд велосипеда - {{$bike["bike"]["brand"]['brand_list'] ?? ''}}
                        </p>
                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.04em;margin-bottom:25px;color:#000000;">
                            Ссылка - <a href="{{$link}}">{{$link}}</a>
                        </p>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>

</html>
