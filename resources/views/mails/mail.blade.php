<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Title</title>
</head>

<body
    style="width:100%!important;height:100%;background:#efefef;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">
<table cellspacing="0" cellpadding="0" class="body-wrap"
       style="border: none;width:100%!important;height:100%;background:#efefef;-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;">
    <tr>
        <td class="container"
            style="background:#ffffff;display:block!important;clear:both!important;margin-top:0px;margin-right:auto;margin-bottom:0px;margin-left:auto;max-width:600px!important;">

            <table cellspacing="0" cellpadding="0" style="background:#ffffff;width:100%!important;">
                <tr>
                    <td align="left" class="masthead"
                        style="background-size:cover;background-repeat:no-repeat;padding-top:0px;padding-right:0px;padding-bottom:10px;padding-left:0px;height:200px;max-height:200px;background-size:cover;background-repeat:no-repeat;height:100%;pointer-events:none;">
                        <img src="{{asset('/')}}/layouts/build/img/pictures/letter-image.png" alt=""
                             style="max-width:100%;margin:0 auto;display:block;">
                    </td>
                </tr>
                <tr>
                    <td class="content" style="padding-top:30px;padding-right:80px;padding-bottom:30px;padding-left:80px;">

                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.02em;margin-bottom:25px;color:#000000;">
                            Вы в одном шаге от завершения регистрации<br>
                            VELOVEX.
                        </p>
                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.02em;margin-bottom:25px;color:#000000;">
                            Для этого подтвердите свой адрес электронной почты здесь:
                        </p>

                        <table cellspacing="0" cellpadding="0" style="border: none;background:#ffffff;width:100%!important;">
                            <tr>
                                <td align="left">
                                    <p
                                        style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.02em;margin-bottom:25px;color:#000000;">
                                        <a href="{{$link}}" class="button btn-black"
                                           style="display:inline-block;color:#ffffff!important;background:#000000;border:1px solid #000000;font-weight:400;font-size:16px;line-height:0px;text-align:center;letter-spacing:0.02em;border-radius:25px;width:270px;height:0px;transition:0.3s;text-decoration:none;margin-top:12px;margin-right:auto;margin-bottom:28px;margin-left:auto;padding-top:22px;padding-right:0px;padding-bottom:22px;padding-left:0px;">
                                            Завершить
                                            регистрацию
                                        </a>
                                    </p>
                                </td>
                            </tr>
                        </table>

                        <p
                            style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.02em;margin-bottom:25px;color:#000000;">
                            Команда
                            <a href="{{asset('/about/team')}}" style="color: #1E91D6;">VELOVEX</a>.
                        </p>
                        <p class="footer-block"
                           style="font-weight:400;font-size:16px;line-height:25px;letter-spacing:0.02em;margin-bottom:25px;color:#000000;background:none;margin-bottom:0;margin-top:-5px;color:#8E959D;text-align:start;font-weight:400;font-size:10px;line-height:10px;">
                            Вы получили это электронное письмо, так как на сайте
                            <a href="{{asset('/')}}" style="color: #1E91D6;">
                                velovex.ru
                            </a>
                            была создана учетная запись с
                            использованием
                            вашего адреса электронной почты. Если вы не делали этого, пожалуйста
                            <a href="mailto:hello@velovex.ru" style="color: #1E91D6;">
                                напишите нам
                            </a>
                            об этом.
                        </p>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>

</html>
