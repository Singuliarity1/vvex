@inject('image', 'App\Services\ImageService')
@inject('view', 'App\Services\BikeViewService')
@if($bike["bike"])
    @php($isPro=$view->isPro($bike["bike"]))

    <div class="bike-card {{isset($hide_white)&&$hide_white?"":"bike-card__white"}}
    {{$bike["status"]==1 && $bike["deleted_at"]==null?"available":""}}
    {{$bike["status"]==2?"inSearch":""}}
    {{$bike["deleted_at"]!=null?"isDeleted":""}}
        " bike_id="{{$bike["bike"]["id"]}}">

        <div class="bike-card__img">
            <a href="{{asset("/bikes/{$bike["bike"]["id"]}")}}">
                <img
                    src="{{isset($bike["bike"]["bike_files"])&&count($bike["bike"]["bike_files"])>0?
(isset($bike["bike"]["bike_files"][0]["file"])?$bike["bike"]["bike_files"][0]["file"]["path"]:""):""}}"
                    alt="{{($bike["bike"]["brand"]['brand_list']??'')." ".$bike["bike"]["model"]." ".$bike["bike"]["year"]}}">



            </a>
        </div>
        <img src="{{asset("layouts/build/img/icons/velocode_badge_pro.svg")}}" alt=""
             class="page-bike-edit__notif-img hide_desktop @if(!$isPro) hide-pro @endif">
        <div class="bike-card__info">
            <div class="bike-card__info-top">
                <a href="{{asset("/bikes/{$bike["bike"]["id"]}")}}"
                   class="bike-card__info-title">

                    <span>{{($bike["bike"]["brand"]['brand_list']??'')." ".$bike["bike"]["model"]." ".$bike["bike"]["year"]}}</span>
                </a>
                <button class="bike-card__info-number"><span>{{$bike["bike"]["serial"]}}</span></button>
            </div>
            <div class="content_info">
                <div class="bike-card__info-content">
                    <span>Модельный год</span><span>{{$bike["bike"]["year"]}}</span>
                    <span>Размер рамы</span><span>{{$bike["bike"]["size"]}}</span>
                    <span>Материал рамы</span><span>{{$bike["bike"]["material"]["brand_list"]??""}}</span>
                    @if($bike["privacy"]!=1 && $bike["bike"]["location"]) <span>Местонахождение</span>
                    <span>{{$bike["bike"]["location"]}}</span> @endif
                </div>
                <div class="control_bike">
                    <form method="POST" class="bike-card__btns">
                        @csrf
                        @method("PUT")
                        <input type="hidden" name="bikes_id" value="{{$bike["bike"]["id"]}}">
                        <div class="number_form">
                            <button class="bike-card__info-number"><span>{{$bike["bike"]["serial"]}}</span></button>
                        </div>
                        <div class="buttons_form">
                            <button class="bike-card__btns-share"></button>
                            @if(Auth::check())
                                @if(auth()->user()->id === $bike["users_id"]?true:false)
                                    <button class="bike-card__btns-alert" data-status="2"></button>@endif
                            @endif
                            <a href="#!" class="bike-card__link bike-card__link-delete_full" data-delete="true">Удалить
                                навсегда</a>
                            <a href="#!" class="bike-card__link bike-card__link-recover" data-restore="true">Восстановить</a>
                            <a href="#!" class="bike-card__link bike-card__link-find">
                                @if(Auth::check())
                                    @if(auth()->user()->id === $bike["users_id"]?true:false)<span class="pc"
                                                                                                  data-status="1">Больше не в розыске</span>@endif
                                @endif
                                <span class="mobile" data-status="1">Не в розыске</span>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bike-card__social">
            <div class="bike-card__social-links">
                <a href="https://vk.com/share.php?url={{asset("/bikes/{$bike["bike"]["id"]}")}}" target="_blank"
                   class="bike-card__social-1 bike-card__social-item">ВКонтакте</a>
                <a href="https://www.facebook.com/sharer.php?u={{asset("/bikes/{$bike["bike"]["id"]}")}}"
                   class="bike-card__social-2 bike-card__social-item" target="_blank">Facebook</a>
                <a href="https://telegram.me/share/url?url={{asset("/bikes/{$bike["bike"]["id"]}")}}"
                   class="bike-card__social-3 bike-card__social-item" target="_blank">Telegram</a>
                <a href="https://wa.me/?text={{asset("/bikes/{$bike["bike"]["id"]}")}}"
                   class="bike-card__social-4 bike-card__social-item" target="_blank">WhatsApp</a>
            </div>
            <input type="text" class="bike-card__social-link visually-hidden"
                   value="{{asset("/bikes/".$bike["bike"]["id"])}}" inputmode="none">
            <span class="bike-card__social-copy">Скопировать ссылку</span>
        </div>
    </div>
@endif
