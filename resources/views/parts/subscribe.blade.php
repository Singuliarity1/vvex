<div class="page-start-goal__subscribe subscribe">
    <form  method="post" class="subscription" novalidate>
        @csrf
        <div class="subscribe__wrapper">
            <div class="subscribe__title">
                Хотите быть в теме?
            </div>
            <div class="subscribe__block">
                <div class="subscribe__text">
                    Следите за новостями, событиями и эксклюзивными предложениями VELOVEX.
                </div>
                <div class="subscribe__block-wrapper">
                <input required type="email" name="email" id="user_email" placeholder="Ваш e-mail">
                <span class="page-check__search-error">E-mail указан некорректно</span>
                <button type="submit" class="subscribe__btn btn-black">
                    Подписаться
                </button>
                </div>
            </div>
        </div>
        <div class="subscribe__message">
            <span>Готово! Вы в теме</span>
        </div>
    </form>
</div>
