@inject('image', 'App\Services\ImageService')
<div class="swiper-slide">
    <a href="{{asset("bikes/{$bike["bike"]["id"]}")}}" >
        <div class="search-slider__item">
            <div class="search-slider__item-img">
                <img
                    src="{{count($bike["bike"]["bike_files"])>0?$image->getImageSize($bike["bike"]["bike_files"][0]["file"],"big_slider"):""}}"
                    alt="{{($bike["bike"]["brand"]?$bike["bike"]["brand"]['brand_list']:'')." ".$bike["bike"]["model"]." ".$bike["bike"]["year"]}}">
            </div>
            <span
               class="search-slider__item-name">{{($bike["bike"]["brand"]?$bike["bike"]["brand"]['brand_list']:'')." ".$bike["bike"]["model"]." ".$bike["bike"]["year"]}}
            </span>
            <span
                class="search-slider__item-date">В розыске с {{date("d-m-Y",strtotime($bike["updated_at"]))}}
            </span>
        </div>
    </a>
</div>
