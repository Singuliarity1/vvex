@inject('getImage', 'App\Services\UserService')
@inject('view', 'App\Services\BikeViewService')
@php($all_components=[])
@php( $all_vilset=[])
@php($all_other=[])
@php($isPro=isset($data["bike"])?$view->isPro($data["bike"]):false)

<div class="page-bike__bread-crumbs">

    @php($bread_ar=[
	    $data["bike"]["bike_category"]["title"]??null,
        $data["bike"]["brand"]['brand_list']??null,
        $data["bike"]["model"]??null
    ])
    @foreach($bread_ar as $key=>$bread)
        @if($bread)
            <span>
                @if($key!=(count($bread_ar)-1))
                    <a href="{{asset('my_bikes_result?'.($key==1?'brand':'category').'='.$bread)}}">
                    {{$bread}}
                </a>
                @else
                    {{$bread}}
                @endif
            </span>
            @if($key!=(count($bread_ar)-1))
                /
            @endif
        @endif
    @endforeach
</div>
<div class="page-bike__block page-bike__block-left">
    <div class="swiper-container bike-swiper">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            {!! $view->showSlide($data["bike"]["bike_files"],"bike-swiper-slide")!!}
        </div>

        <!-- If we need navigation buttons -->
        <div class="slider-btn-prev bike-button-prev"></div>
        <div class="slider-btn-next bike-button-next"></div>
        <button class="btn-fullscreen" id="slider"></button>
        <div class="swiper-pagination bike-swiper__swiper-pagination"></div>
    </div>

    <div class="bike-thumb__wrapper">
        <div class="swiper-container bike-thumb">
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                {!! $view->showSlide($data["bike"]["bike_files"],"bike-thumb-slide")!!}
            </div>
        </div>
    </div>
</div>

<div class="page-bike__block page-bike__block-right">
    <div class="page-bike__header">
        <div class="page-bike__header-logo hint__hover-wrapper">
            <div class="hint__trigger">

                <img
                    src="{{asset("layouts/build/img/".(!$isPro?"pictures/velocode_badge-sm.svg":'icons/velocode_badge_pro.svg'))}}"
                    alt="Подсказка">
            </div>
            <div class="hint__hover">
                Номер рамы этого велосипеда внесен в базу VELOVEX
            </div>
        </div>
        <span>{{$data["bike"]["brand"]['brand_list']??''}} {{$data["bike"]["model"]}}</span>
        @if($data['status']==2)
            <div id="hint-in-wanted" class="page-bike__header-btn hint__hover-wrapper">
                <div class="hint__trigger">
                    <button class="page-bike__header-btn-search">в розыске</button>
                    <div class="block-hint__hover" style="">
                        <div class="hint-in-wanted hint-bottom-right hint__hover">
                            Владелец этого велосипеда объявил его в розыск.<br>
                            Если у вас есть информация о местонахождении велосипеда, пожалуйста
                            <a class="hint__link-bold" href="mailto:no-reply@velovex.ru">
                                свяжитесь с нами.
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(Auth::check())
            <form method="POST" class="form_bookmark">
                @csrf
                <input type="hidden" name="bikes_id" class="bikes_id" value="{{$data["bike"]["id"]}}">
                @if($data["bike"]["bike_bookmark"]==null)
                    @if(Auth::check() && $data["users_id"] != auth()->user()->id)
                        <a href="#" class="page-bike__header-bookmark">
                            <span class="visually-hidden">Закладка</span>
                        </a>
                    @endif
                @endif
            </form>
        @else
            <a href="/register" class="page-bike__header-bookmark-no-user">
                <span class="visually-hidden">Закладка</span>
            </a>
        @endif
        @if(Auth::check() && $data["users_id"]==auth()->user()->id)
            <div id="hint-edit" class="page-bike__btns hint--adaptive">
                <p>&#9679;</p>
                <p>&#9679;</p>
                <p>&#9679;</p>
            </div>
        @endif
        @if(Auth::check() && $data["users_id"]==auth()->user()->id)
        <div class="hint-black-screen"></div>
        <div class="hint__text hint-edit hint-top-left">
            <form method="POST">
                @csrf
                @method("DELETE")
                <a href="{{asset("bikes/".$data["bike"]["id"]."/edit/0")}}" class="hint-edit-text">Редактировать информацию о велосипеде</a>
                @if($view->isPro($data["bike"]))
                    <a href="{{asset("bikes/".$data["bike"]["id"]."/edit/0")}}#components" class="edit-pro hint-edit-text" style="display: block;">Редактировать компоненты</a>
                @endif
                <a href="#" class="hint-edit-delete delete_bike">Удалить велосипед</a>
                <div class="hint--adaptive-close">Отмена</div>
            </form>
        </div>
        @endif
    </div>

    <div class="page-bike__component @if($view->isPro($data["bike"])) page-bike__component--pro @endif">
        <div class="page-bike__info">
            <div class="page-bike__info-item">
                <span>Серийный номер</span>
                <p class="page-bike__info-number">{{$data["bike"]["serial"]}}</p>
            </div>
            <div class="page-bike__info-item info-item--border"></div>

            <div class="page-bike__info-item">
                <span>Производитель</span>
                <p>{{$data["bike"]["brand"]['brand_list']??''}}</p>
            </div>
            <div class="page-bike__info-item">
                <span>Модель</span>
                <p>{{$data["bike"]["model"]}}</p>
            </div>
            <div class="page-bike__info-item">
                <span>Модельный год</span>
                <p>{{$data["bike"]["year"]}}</p>
            </div>
            <div class="page-bike__info-item info-item--border"></div>
            <div class="page-bike__info-item">
                <span>Цвет рамы</span>
                <p>
                    <span class="color color-list">

                        @if($data["bike"]["color"])
                            @php($all_colors=$view->getTitleColors($data["bike"]["color"]))
                            @foreach($all_colors as $color)
                                @if($color=="several")
                                    <span class="green"></span>
                                    <span class="blue"></span>
                                    <span class="red"></span>
                                    <span class="orange"></span>
                                @elseif($color==="white")
                                    <span class="{{$color}}{{count($all_colors)}}"></span>
                                @else
                                    <span class="{{$color}}"></span>
                                @endif
                            @endforeach
                        @endif

                    </span>
                </p>
            </div>
            <div class="page-bike__info-item">
                <span>Материал рамы</span>
                <p>{{$data["bike"]["material"]['brand_list']}}</p>
            </div>
            <div class="page-bike__info-item">
                <span>Размер рамы</span>
                <p>{{$data["bike"]["size"]}}</p>
            </div>
        </div>
        @if($data["privacy"]!=1 && $data["bike"]["location"])
            <div class="page-bike__component__location">
                <div class="page-bike__info">
                    <div class="page-bike__info-item">
                        <span>Местонахождение</span>
                        <p>{{$data["bike"]["location"]}}</p>
                    </div>
                </div>
            </div>
            <div class="page-bike__component__location page-bike__component__location--mobile">
                <div class="page-bike__info">
                    <div class="page-bike__info-item">
                        <span>Местонахождение</span>
                        <p>{{$data["bike"]["location"]}}</p>
                    </div>
                </div>
            </div>
        @endif
        @if(!$view->isEmpty($data["bike"],'group_set'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-1" checked>
            <label for="checkbox-1"><p>Привод</p></label>
            <div class="component-content">
                @if($data["bike"]["equipment_group"])
                    <div class="component-content__item component-content__item-1">
                        <span class="component-content__title">Группа оборудования</span>
                        <p>
                            <strong>{!!$data["bike"]["equipment_group"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["equipment_group"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["pedals"])
                    <div class="component-content__item component-content__item-2">
                        <span class="component-content__title">Педали</span>
                        <p>
                            <strong>{!!$data["bike"]["pedals"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["pedals"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["carriage"])
                    <div class="component-content__item component-content__item-3">
                        <span class="component-content__title">Каретка</span>
                        <p>
                            <strong>{!!$data["bike"]["carriage"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["carriage"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["cassette"])
                    <div class="component-content__item component-content__item-4">
                        <span class="component-content__title">Кассета</span>
                        <p>
                            <strong>{!!$data["bike"]["cassette"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["cassette"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["planetary_hub"])
                    <div class="component-content__item component-content__item-5">
                        <span class="component-content__title">Планетарная втулка</span>
                        <p>
                            <strong>{!!$data["bike"]["planetary_hub"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["planetary_hub"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["singspeed"])
                    <div class="component-content__item component-content__item-6">
                        <span class="component-content__title">Сингспид</span>
                        <p>
                            <strong>{!!$data["bike"]["singspeed"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["singspeed"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["connecting_rods"])
                    <div class="component-content__item component-content__item-7">
                        <span class="component-content__title">Шатуны</span>
                        <p>
                            <strong>{!!$data["bike"]["connecting_rods"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["connecting_rods"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["chainring"])
                    <div class="component-content__item component-content__item-8">
                        <span class="component-content__title">Передняя звезда</span>
                        <p>
                            <strong>{!!$data["bike"]["chainring"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["chainring"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["chain"])
                    <div class="component-content__item component-content__item-9">
                        <span class="component-content__title">Цепь</span>
                        <p>
                            <strong>{!!$data["bike"]["chain"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["chain"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'shifters_switch'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-2" checked>
            <label for="checkbox-2"><p>Шифтеры и переключатели</p></label>
            <div class="component-content">
                @if($data["bike"]["shifters"])
                    <div class="component-content__item component-content__item-10">
                        <span class="component-content__title">Шифтеры</span>
                        <p>
                            <strong>{!!$data["bike"]["shifters"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["shifters"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["front_switch"])
                    <div class="component-content__item component-content__item-11">
                        <span class="component-content__title">Передний переключатель</span>
                        <p>
                            <strong>{!!$data["bike"]["front_switch"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["front_switch"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["rear_switch"])
                    <div class="component-content__item component-content__item-12">
                        <span class="component-content__title">Задний переключатель</span>
                        <p>
                            <strong>{!!$data["bike"]["rear_switch"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rear_switch"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif

                @if($data["bike"]["switch_rollers"])
                    <div class="component-content__item component-content__item-12">
                        <span class="component-content__title">Ролики переключателя</span>
                        <p>
                            <strong>{!!$data["bike"]["switch_rollers"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["switch_rollers"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'control'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-3" checked>
            <label for="checkbox-3"><p>Управление</p></label>
            <div class="component-content">
                @if($data["bike"]["aerobar"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Аэробар</span>
                        <p>
                            <strong>{!!$data["bike"]["aerobar"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["aerobar"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["steering_wheel"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Руль</span>
                        <p>
                            <strong>{!!$data["bike"]["steering_wheel"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["steering_wheel"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["grips"])
                    <div class="component-content__item component-content__item-15">
                        <span class="component-content__title">Грипсы</span>
                        <p>
                            <strong>{!!$data["bike"]["grips"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["grips"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["handlebar_winding"])
                    <div class="component-content__item component-content__item-16">
                        <span class="component-content__title">Обмотка руля</span>
                        <p>
                            <strong>{!!$data["bike"]["handlebar_winding"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["handlebar_winding"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["steering_column"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Рулевая колонка</span>
                        <p>
                            <strong>{!!$data["bike"]["steering_column"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["steering_column"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["stem"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Вынос руля</span>
                        <p>
                            <strong>{!!$data["bike"]["stem"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["stem"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'absorber_fork'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-4" checked>
            <label for="checkbox-4"><p>Подвеска / Вилка</p></label>
            <div class="component-content">
                @if($data["bike"]["rigid_fork"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Ригидная вилка</span>
                        <p>
                            <strong>{!!$data["bike"]["rigid_fork"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rigid_fork"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["shock_absorber_26"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Передний амортизатор 26"</span>
                        <p>
                            <strong>{!!$data["bike"]["shock_absorber_26"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["shock_absorber_26"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["shock_absorber_27_5"])
                    <div class="component-content__item component-content__item-15">
                        <span class="component-content__title">Передний амортизатор 27.5"</span>
                        <p>
                            <strong>{!!$data["bike"]["shock_absorber_27_5"]['brand_list']??""!!}</strong>
                            <span>{!!$data["bike"]["shock_absorber_27_5"]['model_list']??""!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["shock_absorber_29"])
                    <div class="component-content__item component-content__item-16">
                        <span class="component-content__title">Передний амортизатор 29"</span>
                        <p>
                            <strong>{!!$data["bike"]["shock_absorber_29"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["shock_absorber_29"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["shock_absorber_700с"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Передний амортизатор 700C</span>
                        <p>
                            <strong>{!!$data["bike"]["shock_absorber_700с"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["shock_absorber_700с"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["spring"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Пружина</span>
                        <p>
                            <strong>{!!$data["bike"]["spring"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["spring"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["rear_shock_absorber"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Задний амортизатор</span>
                        <p>
                            <strong>{!!$data["bike"]["rear_shock_absorber"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rear_shock_absorber"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'breakers'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-5" checked>
            <label for="checkbox-5"><p>Тормоза</p></label>
            <div class="component-content">
                @if($data["bike"]["rim_brakes"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Ободные тормоза</span>
                        <p>
                            <strong>{!!$data["bike"]["rim_brakes"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rim_brakes"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["disc_brake"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Дисковый тормоз</span>
                        <p>
                            <strong>{!!$data["bike"]["disc_brake"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["disc_brake"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["brake_levers"])
                    <div class="component-content__item component-content__item-15">
                        <span class="component-content__title">Тормозные ручки</span>
                        <p>
                            <strong>{!!$data["bike"]["brake_levers"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["brake_levers"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'saddle_seatpost'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-6" checked>
            <label for="checkbox-6"><p>Седло / подседельный штырь</p></label>
            <div class="component-content">
                @if($data["bike"]["saddle"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Седло</span>
                        <p>
                            <strong>{!!$data["bike"]["saddle"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["saddle"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["seatpost"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Подседельный штырь</span>
                        <p>
                            <strong>{!!$data["bike"]["seatpost"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["seatpost"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["cushioned_seatpost"])
                    <div class="component-content__item component-content__item-15">
                        <span class="component-content__title">Амортизированный подседельный штырь</span>
                        <p>
                            <strong>{!!$data["bike"]["cushioned_seatpost"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["cushioned_seatpost"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["telescopic_seatpost"])
                    <div class="component-content__item component-content__item-16">
                        <span class="component-content__title">Телескопический подседельный штырь</span>
                        <p>
                            <strong>{!!$data["bike"]["telescopic_seatpost"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["telescopic_seatpost"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'wheels_tires'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-7" checked>
            <label for="checkbox-7"><p>Колеса</p></label>
            <div class="component-content">
                @if($data["bike"]["wheels_26"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Колеса 26"</span>
                        <p>
                            <strong>{!!$data["bike"]["wheels_26"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["wheels_26"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["wheels_27"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Колеса 27.5"</span>
                        <p>
                            <strong>{!!$data["bike"]["wheels_27"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["wheels_27"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["wheels_29"])
                    <div class="component-content__item component-content__item-15">
                        <span class="component-content__title">Колеса 29"</span>
                        <p>
                            <strong>{!!$data["bike"]["wheels_29"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["wheels_29"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["wheels_700C"])
                    <div class="component-content__item component-content__item-16">
                        <span class="component-content__title">Колеса 700C</span>
                        <p>
                            <strong>{!!$data["bike"]["wheels_700C"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["wheels_700C"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["rims"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Обода</span>
                        <p>
                            <strong>{!!$data["bike"]["rims"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rims"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["tires"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Шины</span>
                        <p>
                            <strong>{!!$data["bike"]["tires"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["tires"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["rims"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Обода</span>
                        <p>
                            <strong>{!!$data["bike"]["rims"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["rims"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["inner_tire"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Камера</span>
                        <p>
                            <strong>{!!$data["bike"]["inner_tire"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["inner_tire"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["sleeve"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Втулка</span>
                        <p>
                            <strong>{!!$data["bike"]["sleeve"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["sleeve"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["drum_bushing"])
                    <div class="component-content__item component-content__item-17">
                        <span class="component-content__title">Барабан втулки</span>
                        <p>
                            <strong>{!!$data["bike"]["drum_bushing"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["drum_bushing"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif

        @if(!$view->isEmpty($data["bike"],'power'))
        <div class="page-bike__component-wrapper">
            <input type="checkbox" name="component" id="checkbox-8" checked>
            <label for="checkbox-8"><p>Измеритель мощности</p></label>
            <div class="component-content">
                @if($data["bike"]["power_connecting_rods"])
                    <div class="component-content__item component-content__item-13">
                        <span class="component-content__title">Измеритель мощности (шатуны)</span>
                        <p>
                            <strong>{!!$data["bike"]["power_connecting_rods"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["power_connecting_rods"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
                @if($data["bike"]["power_pedals"])
                    <div class="component-content__item component-content__item-14">
                        <span class="component-content__title">Измеритель мощности (педали)</span>
                        <p>
                            <strong>{!!$data["bike"]["power_pedals"]['brand_list']!!}</strong>
                            <span>{!!$data["bike"]["power_pedals"]['model_list']!!}</span>
                        </p>
                    </div>
                @endif
            </div>
        </div>
        @endif
        <button class="page-bike__component-btn btn-black">Наверх</button>
    </div>

    @if(Auth::check() && $data["users_id"] == auth()->user()->id && !$view->isPro($data["bike"]))
        <div class="page-bike__pro hint-block-active">
            <div class="pro__content">
                <img src="{{asset("layouts/build/img/icons/close_x1.svg")}}" alt="Закрыть"
                     class="pro__close hint-block__btn-close">
                <div class="pro__title">Супер! Этот велосипед уже в базе VELOVEX</div>
                <div class="pro__desk">
                    <p>А если ты PRO и хочешь, чтобы рядом с твоим байком стоял особый PRO-значок - <br> жми кнопку
                        «Продолжить» <br> и добавляй компоненты.</p>
                    <img src="{{asset("layouts/build/img/icons/velocode_badge_pro.svg")}}" alt="Иконка 'Про'"
                         class="pro__desk-img">
                </div>
                <a href="{{asset("bikes/".$data["bike"]["id"]."/edit/1#components")}}" class="btn-link btn-black">Продолжить</a>
                <button class="btn-later hint-block__btn-close">Добавлю позже</button>
            </div>
        </div>
    @endif

    @if(Auth::check() && $data["users_id"] != auth()->user()->id)
        <div class="page-bike__user user">
            <div class="user__title">Владелец велосипеда</div>
            <div class="user__block">
                <div class="user__img">
                    <img src="{{$getImage->userImg($data["users_id"])}}"/>
                </div>
                <div class="user__info">
                    <span class="user__name">{{$user["name"]??''}}</span>
                    <span
                        class="user__date">Участник VELOVEX с {{date("Y",strtotime($user["created_at"]))}}</span>
                </div>
            </div>
            <a href="{{asset("/users/{$user["id"]}")}}" class="btn-link btn-transparent user__link">Посмотреть
                профиль</a>
            @if(count($user["bikes"]) > 1)
                <div class="user__list-title">
                    Другие велосипеды владельца:
                </div>
            @endif
            <ul class="user__list">
                @php($item=0)
                @if($user["bikes"]!=null)
                    @foreach ($user["bikes"] as $bike)
                        @if($data["bike"]["id"] != $bike["bikes_id"])
                            @php($item++)
                            <li>
                                <a href="{{asset("/bikes/".$bike["bike"]["id"])}}">{{$bike["bike"]["brand"]['brand_list']??''}} {{$bike["bike"]["model"]}}</a>
                            </li>
                            @break($item==2)
                        @endif
                    @endforeach

                    <?php
                    $count = count( $user["bikes"] ) - 2;
                    ?>
                    @if($count>0)
                        <li><a href="#!">Еще {{$count}}</a></li>
                    @endif
                @endif
            </ul>
        </div>
    @endif
</div>
