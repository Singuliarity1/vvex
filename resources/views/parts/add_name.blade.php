<div class="enter_user_name">
    <div class="dark_background"></div>
    <div class="form_user_name">
        <form method="POST">
            @csrf

            {{method_field("PUT")}}
            <div class="page-settings__contacts-info">
            <h3>Создаем аккаунт</h3>
            <p>Вы получите персональную страницу
                с уникальной ссылкой.</p>
                <div>
                    <label>
                        <span>Ваше имя</span>
                        <input type="text" name="name" value="" required>
                    </label>
                </div>
            </div>
            <button type="submit" class="user_name__btn">
                Сохранить
            </button>
        </form>
    </div>
</div>
