
    <form class="bike-card bike-card__white ajax_delete_bike dsa" method="POST">
        @csrf

        {{method_field("DELETE")}}
        <input type="hidden" class="bookmark_id" name="id" value="{{$bike["bike"]["bike_bookmark"]["id"] ?? ""}}">
        <div class="bike-card bike-card__white">
        <div class="bike-card__img">
            <a href="{{asset("/bikes/{$bike["bike"]["id"]}")}}">
                <img src="{{count($bike["bike"]["bike_files"])>0?$bike["bike"]["bike_files"][0]["file"]["path"]:""}}">
            </a>
        </div>
        <div class="bike-card__info">
            <div class="bike-card__info-top">
                <a href="{{asset("/bikes/{$bike["bike"]["id"]}")}}"
                   class="bike-card__info-title"><span>{{($bike["bike"]["brand"]?$bike["bike"]["brand"]['brand_list']:'')." ".$bike["bike"]["model"]." ".$bike["bike"]["year"]}}</span></a>
                <span class="bike-card__info-img">
                                                    <img
                                                        src="{{asset("layouts/build/img/pictures/velocode_badge-sm.svg")}}"
                                                        alt="">
                                                  </span>
            </div>
            <div class="bike-card__info-content">
                <span>Модельный год</span><span>{{$bike["bike"]["year"]}}</span>
                <span>Размер рамы</span><span>{{$bike["bike"]["size"]}}</span>
                <span>Материал рамы</span><span>{{$bike["bike"]["material"]["brand_list"]}}</span>
                @if($bike["privacy"]!=1 && $bike["bike"]["location"]) <span>Местонахождение</span><span>{{$bike["bike"]["location"]}}</span> @endif
            </div>
            <div class="bike-card__btns">
                <span class="bike-card__info-number">{{$bike["bike"]["serial"]}}</span>
                <a href="#!" class="bike-card__link bike-card__link-delete">Удалить</a>
            </div>
        </div>
        </div>
    </form>
