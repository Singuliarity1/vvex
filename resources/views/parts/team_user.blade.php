@inject('user_info', 'App\Services\UserService')
<div class='page-start-goal__team-item'>
    <div class="team-member">
        <div class="team-member__img">
            <img src="{{$user_info->userImg($user["id"])}}" alt="{{$user["name"]}}">
        </div>
        <div class="team-member__name">
            {{$user["name"]}}
        </div>
        @php($bikes=$user_info->getAllUsersBikesAndCategories($user))
        <div class="team-member__info">
            <p>
                <span>Катается:</span> {{$bikes[0]}}
            </p>
            <p>
                <span>Велосипед:</span> {{$bikes[1]}}
            </p>
        </div>
        <a href="{{asset("users/".$user["id"])}}" class="team-member__btn btn-link btn-transparent">
            Посмотреть профиль
        </a>
    </div>
</div>
