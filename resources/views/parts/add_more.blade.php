
@if(isset($data['count']) && $data['count']>10)
    <div class="result-search__info">
        Показано <span class="limit">10</span> из <span class="total">{{$data['count']}}</span>
    </div>
    <div class="result-search__btn btn-black">Показать еще</div>
@endif
