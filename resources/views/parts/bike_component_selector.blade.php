
@php
    $arr = array(
        "items"=> [],
    )
@endphp

@foreach($data as $key => $value)
    @php

        $arr["items"][$value['slug']]=isset($arr["items"][$value['slug']])??[];
        $count=count($value['items']);
        foreach($value['items'] as $item){
        	$count+=count($item['models']);
        }

        $arr["items"][$value['slug']]=$value["title"];

    @endphp
@endforeach

@foreach($component["items"] as $key => $value)
    @php
        $countBrand=count($value['models']);

        $value['brand_list'] = ($value['brand_list']??'');


        $component["items"][$key]=$value;

    @endphp
@endforeach

<div class="edit-form__field edit-form__field-select @if(isset($bike[$component['slug']])&&$bike[$component['slug']]) component-active @endif">

    <select class="page__edit-select required-form__field select__current components" name="{{$component["slug"].'_parent'}}" data-select="first">
        @include("parts.options_bike",["items"=>$arr["items"], "component"=>$component, "value"=>isset($bike[$component['slug']])&&$bike[$component['slug']]?$component["slug"]:""])
    </select>
    <select class="page__edit-select required-form__field select__current components" name="{{$component["slug"]}}" data-select="second" data-select-id="{{str_replace(' ', '', $component["slug"])}}" disabled>
        @include("parts.options_bike",["items"=>$component["items"], "component"=>$component,"value"=>$bike[$component['slug']]??""])
    </select>
    <div class="edit-form-delete">
        <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle class="delete-hover-stroke" cx="21" cy="21" r="20.5" stroke="#B6BCC3"/>
            <path class="delete-hover" d="M17.2446 26.7199C17.4443 26.7199 17.6361 26.641 17.7784 26.5002C17.9206 26.3594 18.0019 26.1681 18.0047 25.9676V19.8396C18.0047 19.6372 17.9246 19.4432 17.7821 19.3001C17.6395 19.157 17.4462 19.0767 17.2446 19.0767C17.0429 19.0767 16.8496 19.157 16.707 19.3001C16.5645 19.4432 16.4844 19.6372 16.4844 19.8396V25.9676C16.4872 26.1681 16.5685 26.3594 16.7107 26.5002C16.853 26.641 17.0448 26.7199 17.2446 26.7199Z" fill="#B6BCC3"/>
            <path class="delete-hover" d="M20.8998 26.7199C21.0996 26.7199 21.2914 26.641 21.4336 26.5002C21.5759 26.3594 21.6572 26.1681 21.66 25.9676V19.8396C21.66 19.6372 21.5799 19.4432 21.4374 19.3001C21.2948 19.157 21.1014 19.0767 20.8998 19.0767C20.6982 19.0767 20.5049 19.157 20.3623 19.3001C20.2197 19.4432 20.1396 19.6372 20.1396 19.8396V25.9676C20.1424 26.1681 20.2238 26.3594 20.366 26.5002C20.5083 26.641 20.7 26.7199 20.8998 26.7199Z" fill="#B6BCC3"/>
            <path class="delete-hover" d="M24.5561 26.7199C24.7559 26.7199 24.9476 26.641 25.0899 26.5002C25.2321 26.3594 25.3135 26.1681 25.3163 25.9676V19.8396C25.3163 19.6372 25.2362 19.4432 25.0936 19.3001C24.951 19.157 24.7577 19.0767 24.5561 19.0767C24.3545 19.0767 24.1611 19.157 24.0186 19.3001C23.876 19.4432 23.7959 19.6372 23.7959 19.8396V25.9676C23.7987 26.1681 23.88 26.3594 24.0223 26.5002C24.1645 26.641 24.3563 26.7199 24.5561 26.7199Z" fill="#B6BCC3"/>
            <path class="delete-hover" d="M30.0398 13.6939H25.1747L24.5489 11.5897C24.4102 11.1324 24.1296 10.7316 23.748 10.4456C23.3664 10.1596 22.9037 10.0035 22.4274 10H19.3761C18.8999 10.0035 18.4372 10.1596 18.0555 10.4456C17.6739 10.7316 17.3933 11.1324 17.2547 11.5897L16.6289 13.6939H11.7602C11.5586 13.6939 11.3652 13.7742 11.2227 13.9173C11.0801 14.0604 11 14.2544 11 14.4568C11 14.6591 11.0801 14.8532 11.2227 14.9962C11.3652 15.1393 11.5586 15.2197 11.7602 15.2197H12.6653V29.9065C12.6691 30.4624 12.8915 30.9943 13.2842 31.3864C13.6769 31.7785 14.208 31.9991 14.762 32H27.0627C27.6185 31.9991 28.1513 31.7771 28.5443 31.3827C28.9373 30.9882 29.1585 30.4536 29.1594 29.8958V15.1948H30.0398C30.2414 15.1948 30.4348 15.1145 30.5773 14.9714C30.7199 14.8283 30.8 14.6343 30.8 14.4319C30.7996 14.333 30.7794 14.2352 30.7408 14.1442C30.7021 14.0532 30.6457 13.9709 30.5749 13.9022C30.504 13.8334 30.4202 13.7795 30.3283 13.7438C30.2364 13.708 30.1383 13.691 30.0398 13.6939ZM18.7255 12.0261C18.7688 11.8853 18.8546 11.7614 18.9712 11.6717C19.0878 11.5821 19.2293 11.5311 19.3761 11.5258H22.4486C22.5989 11.5245 22.7453 11.5733 22.8649 11.6645C22.9846 11.7557 23.0707 11.8841 23.1098 12.0297L23.5977 13.6939H18.1987L18.7255 12.0261ZM27.6391 30.0626H27.586C27.5464 30.1766 27.4731 30.2758 27.3758 30.3468C27.2784 30.4178 27.1618 30.4573 27.0415 30.46H14.762C14.6079 30.4572 14.4609 30.3941 14.3526 30.284C14.2442 30.1739 14.183 30.0257 14.1821 29.871V15.1948H27.6391V30.0626Z" fill="#B6BCC3"/>
        </svg>
    </div>
</div>
