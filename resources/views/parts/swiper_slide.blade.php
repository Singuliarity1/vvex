@inject('image', 'App\Services\ImageService')
<div class="swiper-slide">
    <div class="hall-slider__item">
        <div class="hall-slider__item-wrapper {{isset($like_bike["bike"]["bike_like"]["like"])&&
                          $like_bike["bike"]["bike_like"]!=[]?"isLiked":""}}">
            <div class="hall-slider__item-img">
                <a href="{{asset("bikes/".$like_bike["bike"]["id"])}}">
                    <img
                        src="{{count($like_bike["bike"]["bike_files"])>0?$image->getImageSize($like_bike["bike"]["bike_files"][0]["file"],"big_slider"):""}}"
                        alt="{{($like_bike["bike"]["brand"]?$like_bike["bike"]["brand"]['brand_list']:'')." ".$like_bike["bike"]["model"]." ".$like_bike["bike"]["year"]}}"
                    >
                </a>
            </div>
            <div class="hall-slider__item-name">
                <a href="{{asset("bikes/".$like_bike["bike"]["id"])}}">
                    {{($like_bike["bike"]["brand"]?$like_bike["bike"]["brand"]['brand_list']:'')." ".$like_bike["bike"]["model"]." ".$like_bike["bike"]["year"]}}
                </a>
            </div>
            <form method="POST" class="hall-slider__btns">
                @csrf
                <input type="hidden" name="bikes_id" value="{{$like_bike["bike"]["id"]}}">
                <button class="hall-slider__like likes_post
                        {{isset($like_bike["bike"]["bike_like"]["like"])&&
                          $like_bike["bike"]["bike_like"]["like"]==1?"isLiked":""}}"
                        data-like="1">{{$like_bike["bike"]["bike_like"]!=[]?$like_bike["bike"]["bike_like"]["like"]:""}}</button>
                <div class="">или</div>
                <button class="hall-slider__fire likes_post
                        {{isset($like_bike["bike"]["bike_like"]["like"])&&
                          $like_bike["bike"]["bike_like"]["like"]==2?"isLiked":""}}"
                        data-like="2">
                    {{$like_bike["bike"]["bike_like"]!=[]?$like_bike["bike"]["bike_like"]["fire"]:""}}

                </button>
            </form>
        </div>
    <!-- <span class="hall-slider__item-date">
                  Добавлен {{date("d-m-Y",strtotime($like_bike["bike"]["created_at"]))}}
        </span> -->
    </div>
</div>
