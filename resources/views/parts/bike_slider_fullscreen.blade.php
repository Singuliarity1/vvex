<div class="page-bike__content-fullscreen">
    <div class="swiper-container bike-fullscreen">
      <button class="btn-mobile-fullscreen"></button>
      <!-- Additional required wrapper -->
      <div class="swiper-wrapper">
        <!-- Slides -->
      {!! $view->showSlide($data["bike"]["bike_files"],"bike-swiper-slide")!!}
      </div>
      <div class="swiper-pagination bike-fullscreen__swiper-pagination"></div>
      <!-- If we need navigation buttons -->
      <div class="slider-btn-prev bike-fullscreen-prev"></div>
      <div class="slider-btn-next bike-fullscreen-next"></div>
      <button class="btn-fullscreen-close"></button>
    </div>
</div>
