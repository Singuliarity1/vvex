<option></option>

@foreach($items as $key_item=>$item_one)
    @if(is_array($item_one))

        <option
            model="{{$key_item}}"
            slug="{{$key_item}}"
            @if((isset($item_one['id']) && isset($value['id']) && $item_one['id']==$value['id'])) selected @endif
            value="{{$item_one['id']??0}}"
            data-parent="{{$key_item}}"
            data-select-id="{{$key_item}}">
            {{$item_one['brand_list']}}
        </option>
        @foreach($item_one['models'] as $key_current_item=>$current_item)

            <option
                model="{{$key_item}}"
                slug="{{$key_current_item}}"
                @if($current_item['id']==($value['id']??$value)) selected @endif
                value="{{$current_item['id']}}"
                data-parent="{{$key_item}}"
                data-child="true"
                data-select-id="{{$key_item}}">
                {{$current_item['model_list']}}
            </option>
        @endforeach
    @else

        <option model="{{$key_item}}"
                slug="{{$key_item}}"
                @if($key_item==$value || $item_one==$value) selected @endif
                value="{{$item_one}}"
                data-select-id="{{str_replace(' ', '', $key_item)}}">{{$item_one}}</option>
    @endif
@endforeach
